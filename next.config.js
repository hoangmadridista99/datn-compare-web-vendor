/* eslint-disable no-undef */
const { i18n } = require('./next-i18next.config');

/** @type {import('next').NextConfig} */
const nextConfig = {
  i18n,
  images: {
    remotePatterns: [
      {
        hostname: 'dev.api.insurance.just.engineer',
      },
      {
        hostname: 'localhost',
      },
    ],
  },
};

module.exports = nextConfig;
