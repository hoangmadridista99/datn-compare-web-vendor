import { useState, useEffect, useCallback } from 'react';

import {
  getInsuranceDetail,
  getObjectives,
  updateInsurance,
} from '@services/insurances';
import {
  FORM_HEALTH_INSURANCE_DEFAULT,
  INSURANCE_CATEGORY,
  ROUTE,
} from '@constants';
import { handleError } from '@helpers/handleError';
import { Form, notification } from 'antd';
import { convertUrl } from '@helpers/convertUrl';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';

const { useForm } = Form;

const ERROR_CODE_NOT_FOUND = 'SS24101';

export const useUpdateInsuranceLogic = id => {
  const [isLoading, setIsLoading] = useState(false);
  const [isModalAlertUpdateSuccess, setIsModalAlertUpdateSuccess] =
    useState(false);

  const [insuranceDetails, setInsuranceDetails] = useState(null);
  const [objectives, setObjectives] = useState([]);

  const [form] = useForm();
  const router = useRouter();
  const { t } = useTranslation(['errors']);

  const handleResponseDataLifeInsurance = useCallback(body => {
    const {
      objective_of_insurance,
      benefits_illustration_table,
      documentation_url,
      ...data
    } = body;
    const objectiveOfInsurance = objective_of_insurance.map(data => data.id);

    let benefitsIllustrationTable = null;
    let documentationUrl = null;

    if (benefits_illustration_table) {
      const { name } = convertUrl(benefits_illustration_table);

      benefitsIllustrationTable = [
        {
          uid: '1',
          name: name,
          status: 'done',
          url: benefits_illustration_table,
        },
      ];
    }

    if (documentation_url) {
      const { name } = convertUrl(documentation_url);

      documentationUrl = [
        {
          uid: '2',
          name: name,
          status: 'done',
          url: documentation_url,
        },
      ];
    }

    return {
      ...data,
      objective_of_insurance: objectiveOfInsurance,
      benefits_illustration_table: benefitsIllustrationTable,
      documentation_url: documentationUrl,
    };
  }, []);
  const handleResponseDataHealthInsurance = useCallback(body => {
    const { dental, obstetric } = body;
    let additional_benefit = [];
    let hospitalsPending = [];

    if (dental) {
      additional_benefit = [...additional_benefit, 'dental'];
    }
    if (obstetric) {
      additional_benefit = [...additional_benefit, 'obstetric'];
    }
    if (body?.hospitals_pending) {
      hospitalsPending = body.hospitals_pending.map(item => ({
        ...item,
        isPending: true,
      }));
    }
    const hospitals = [...(body?.hospitals ?? []), ...hospitalsPending];

    return {
      ...body,
      hospitals,
      additional_benefit,
      dental: dental ?? FORM_HEALTH_INSURANCE_DEFAULT.dental,
      obstetric: obstetric ?? FORM_HEALTH_INSURANCE_DEFAULT.obstetric,
    };
  }, []);

  const hanldeGetObjectives = useCallback(
    async signal => {
      try {
        const response = await getObjectives(signal);
        setObjectives(response);
      } catch (error) {
        handleError(t, error);
      }
    },
    [t]
  );
  const handleGetInsuranceDetails = useCallback(
    async signal => {
      try {
        setIsLoading(true);
        const response = await getInsuranceDetail(id, signal);

        switch (response?.insurance_category?.label) {
          case INSURANCE_CATEGORY.LIFE:
            await hanldeGetObjectives(signal);
            setInsuranceDetails(handleResponseDataLifeInsurance(response));
            return;
          case INSURANCE_CATEGORY.HEALTH:
            setInsuranceDetails(handleResponseDataHealthInsurance(response));
            return;
          default:
            handleError(t, ERROR_CODE_NOT_FOUND);
            return;
        }
      } catch (error) {
        handleError(t, error);
      } finally {
        setIsLoading(false);
      }
    },
    [
      handleResponseDataHealthInsurance,
      handleResponseDataLifeInsurance,
      hanldeGetObjectives,
      id,
      t,
    ]
  );

  useEffect(() => {
    const controller = new AbortController();

    handleGetInsuranceDetails(controller.signal);

    return () => controller.abort();
  }, [handleGetInsuranceDetails]);

  const handleCloseModalUpdateSuccess = () => {
    setIsModalAlertUpdateSuccess(false);
    router.push(ROUTE.INSURANCES);
  };

  const handleGetUrlFileUploadInForm = useCallback(data => {
    if (Array.isArray(data)) return data[0]?.url ?? null;

    if (data?.fileList?.length > 0) {
      return data?.fileList[0]?.xhr?.responseURL ?? null;
    }

    return null;
  }, []);

  const handleBodyUpdateFormLifeInsurance = useCallback(
    data => {
      const {
        objective_of_insurance: objective,
        benefits_illustration_table: benefitsIllustrationTable,
        documentation_url: documentationUrl,
        ...dataValues
      } = data;

      const objective_of_insurance = [...(objective ?? [])].map(id => {
        const findObjective = [...objectives].find(item => item.id === id);

        return findObjective;
      });

      const documentation_url = handleGetUrlFileUploadInForm(documentationUrl);
      const benefits_illustration_table = handleGetUrlFileUploadInForm(
        benefitsIllustrationTable
      );

      const body = {
        ...dataValues,
        objective_of_insurance,
        benefits_illustration_table,
        documentation_url,
      };

      return body;
    },
    [objectives, handleGetUrlFileUploadInForm]
  );
  const hanldeBodyUpdateFormHealthInsurance = useCallback(obj => {
    const { name, description_insurance, additional_benefit } = obj;
    const hospitals = [...(obj.hospitals ?? [])].map(item => {
      if (typeof item?.id === 'number' || item?.isPending) {
        const { address, district, name, province } = item;
        return { address, district, name, province };
      }
      return item;
    });
    return {
      ...obj,
      name,
      description_insurance,
      additional_benefit,
      hospitals,
    };
  }, []);

  const handleFinishForm = async data => {
    try {
      if (insuranceDetails) {
        setIsLoading(true);
        let body = { ...insuranceDetails, ...data };
        switch (insuranceDetails?.insurance_category?.label) {
          case INSURANCE_CATEGORY.LIFE:
            body = handleBodyUpdateFormLifeInsurance(body);
            break;
          case INSURANCE_CATEGORY.HEALTH:
            body = hanldeBodyUpdateFormHealthInsurance(body);
            break;
          default:
            return;
        }

        await updateInsurance(id, body);
        setIsModalAlertUpdateSuccess(true);
      }
    } catch (error) {
      handleError(t, error);
    } finally {
      setIsLoading(false);
    }
  };

  const handleClickSave = async () => {
    try {
      await form.validateFields();
      if (
        insuranceDetails &&
        insuranceDetails?.insurance_category?.label === INSURANCE_CATEGORY.LIFE
      ) {
        const key_benefits = form.getFieldValue('key_benefits');
        if (!key_benefits) {
          notification.error({ message: t('insurances:errors:key_benefits') });
          return;
        }
      }
      form.submit();
    } catch (error) {
      notification.error({
        message: t('insurances:errors:default'),
      });
    }
  };

  return {
    isLoading,
    isModalAlertUpdateSuccess,
    insuranceDetails,
    form,
    objectives,
    handleFinishForm,
    handleClickSave,
    handleCloseModalUpdateSuccess,
  };
};
