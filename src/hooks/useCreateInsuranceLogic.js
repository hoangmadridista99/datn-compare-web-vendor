import { useState, useCallback, useEffect, useMemo } from 'react';
import { Form } from 'antd';

import { handleError } from '@helpers/handleError';
import {
  getObjectives,
  getCategories,
  createInsurance,
} from '@services/insurances';
import {
  FORM_HEALTH_INSURANCE_DEFAULT,
  FORM_LIFE_INSURANCE_DEFAULT,
  INSURANCE_CATEGORY,
} from '@constants';
import { useTranslation } from 'next-i18next';

export const useCreateInsuranceLogic = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [isModalCreateSuccess, setIsModalCreateSuccess] = useState(false);

  const [insuranceCategory, setInsuranceCategory] = useState(null);
  const [objectives, setObjectives] = useState(null);
  const [insuranceCategories, setInsuranceCategories] = useState(null);

  const [form] = Form.useForm();

  const { t } = useTranslation(['errors']);

  const handleGetCategories = useCallback(
    async signal => {
      try {
        const result = await getCategories(signal);
        setInsuranceCategories(result);
      } catch (error) {
        handleError(t, error);
        setInsuranceCategories([]);
      }
    },
    [t]
  );
  const handleGetObjectives = useCallback(
    async signal => {
      try {
        if (
          insuranceCategory &&
          insuranceCategory === INSURANCE_CATEGORY.LIFE &&
          !objectives
        ) {
          const result = await getObjectives(signal);
          setObjectives(result);
        }
      } catch (error) {
        handleError(t, error);
        setObjectives([]);
      }
    },
    [insuranceCategory, objectives, t]
  );

  useEffect(() => {
    const controller = new AbortController();

    handleGetCategories(controller.signal).catch(error =>
      handleError(t, error)
    );

    return () => controller.abort();
  }, [handleGetCategories, t]);
  useEffect(() => {
    const controller = new AbortController();

    handleGetObjectives(controller.signal);

    return () => controller.abort();
  }, [handleGetObjectives]);

  const handleDataFormLifeInsurance = useCallback(
    data => {
      const {
        objective_of_insurance: objective,
        benefits_illustration_table: document_benefits,
        documentation_url: document,
        ...dataValues
      } = data;

      const objective_of_insurance = [...(objective ?? [])].map(id => {
        const findObjective = [...objectives].find(item => item.id === id);

        return findObjective;
      });

      let documentation_url = null;
      let benefits_illustration_table = null;

      if (document_benefits) {
        benefits_illustration_table = Array.isArray(document_benefits)
          ? document_benefits[0].url
          : document_benefits.file.xhr.responseURL;
      }

      if (document) {
        documentation_url = Array.isArray(document)
          ? document[0].url
          : document.file.xhr.responseURL;
      }

      const body = {
        ...dataValues,
        objective_of_insurance,
        benefits_illustration_table,
        documentation_url,
      };

      return body;
    },
    [objectives]
  );
  const handleFinishForm = useCallback(
    async data => {
      try {
        if (insuranceCategory) {
          const insurance_category = [...insuranceCategories].find(
            item => item.label === insuranceCategory
          );
          if (insurance_category) {
            setIsLoading(true);
            let body = { insurance_category };
            switch (insuranceCategory) {
              case INSURANCE_CATEGORY.HEALTH:
                body = { ...body, ...FORM_HEALTH_INSURANCE_DEFAULT, ...data };
                break;
              case INSURANCE_CATEGORY.LIFE:
                body = {
                  ...body,
                  ...FORM_LIFE_INSURANCE_DEFAULT,
                  ...handleDataFormLifeInsurance(data),
                };
                break;
              default:
                return;
            }

            await createInsurance(body);

            form.resetFields();
            setInsuranceCategory(null);
            setIsModalCreateSuccess(true);
          }
        }
      } catch (error) {
        handleError(t, error);
      } finally {
        setIsLoading(false);
      }
    },
    [
      form,
      handleDataFormLifeInsurance,
      insuranceCategories,
      insuranceCategory,
      t,
    ]
  );

  const handleSelectInsuranceCategory = value => setInsuranceCategory(value);
  const handleCloseModalCreateSuccess = () => setIsModalCreateSuccess(false);

  const result = useMemo(
    () => ({
      isLoading,
      isModalCreateSuccess,
      objectives,
      insuranceCategory,
      form,
      handleSelectInsuranceCategory,
      handleCloseModalCreateSuccess,
      handleFinishForm,
    }),
    [
      isLoading,
      form,
      insuranceCategory,
      isModalCreateSuccess,
      objectives,
      handleFinishForm,
    ]
  );

  return result;
};
