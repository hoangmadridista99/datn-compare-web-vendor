import { useState, useCallback } from 'react';
import { useTranslation } from 'react-i18next';

const ACCEPTED_LIST_ALPHABET_WITH_COMMAND = ['a', 'v', 'c', 'x'];
const ACCEPTED_LIST_KEY = [
  'Backspace',
  'ArrowLeft',
  'ArrowRight',
  'Control',
  'Meta',
];

export const useInputNumber = () => {
  const [errors, setErrors] = useState({});

  const { t } = useTranslation(['common']);

  const hasAccepted = useCallback((key, ctrlKey, metaKey) => {
    const isNumber = !isNaN(parseInt(key));
    if (isNumber) return true;
    const isListKeyAccepted = [
      ...ACCEPTED_LIST_KEY,
      ...ACCEPTED_LIST_ALPHABET_WITH_COMMAND,
    ].includes(key);

    if (isListKeyAccepted) {
      if (!ACCEPTED_LIST_KEY.includes(key)) {
        const isCommandAccepted =
          (ctrlKey || metaKey) &&
          ACCEPTED_LIST_ALPHABET_WITH_COMMAND.includes(key);

        if (!isCommandAccepted) return false;
      }

      return true;
    }

    return false;
  }, []);

  const onKeyDown = (event, keyError) => {
    const { ctrlKey, metaKey, key } = event;

    if (key === 'Unidentified') {
      event.preventDefault();
      setErrors(preState => ({ ...preState, [keyError]: true }));
      return;
    }

    const isAccepted = hasAccepted(key, ctrlKey, metaKey);

    if (!isAccepted) {
      event.preventDefault();
      setErrors(preState => ({ ...preState, [keyError]: true }));
      return;
    }

    setErrors(preState => ({ ...preState, [keyError]: false }));
  };

  return {
    errors,
    message: t('common:input_number'),
    onKeyDown,
  };
};
