import { useState, useEffect, useCallback, useMemo } from 'react';

import { handleError } from '@helpers/handleError';
import { getInsurances } from '@services/insurances';
import { FILTER_INSURANCE_DEFAULT } from '@constants';
import { useTranslation } from 'next-i18next';

export const useInsurancesLogic = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [isShowMoreFilter, setIsShowMoreFilter] = useState(false);

  const [previewInsurance, setPreviewInsurance] = useState(null);

  const [filter, setFilter] = useState(FILTER_INSURANCE_DEFAULT);
  const [insuranceNameTemp, setInsuranceNameTemp] = useState(null);
  const [monthlyFeeTemp, setMonthlyFeeTemp] = useState(null);

  const [insurances, setInsurances] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);

  const { t } = useTranslation(['errors']);

  const handleGetInsurances = useCallback(
    async signal => {
      try {
        setIsLoading(true);
        const result = await getInsurances(currentPage, filter, signal);

        setInsurances(result.data);
        setTotalPages(result.meta.totalPages);
      } catch (error) {
        handleError(t, error);
        setInsurances([]);
      } finally {
        setIsLoading(false);
      }
    },
    [currentPage, filter, t]
  );

  useEffect(() => {
    const controller = new AbortController();

    handleGetInsurances(controller.signal);

    return () => controller.abort();
  }, [handleGetInsurances]);
  useEffect(() => {
    let id;

    if (insuranceNameTemp !== null) {
      id = setTimeout(
        () =>
          setFilter(preFilter => ({
            ...preFilter,
            insurance_name: insuranceNameTemp,
          })),
        500
      );
    }

    return () => clearTimeout(id);
  }, [insuranceNameTemp]);
  useEffect(() => {
    let id;

    if (monthlyFeeTemp !== null) {
      id = setTimeout(
        () =>
          setFilter(preFilter => ({
            ...preFilter,
            monthly_fee: monthlyFeeTemp ? monthlyFeeTemp : null,
          })),
        500
      );
    }

    return () => clearTimeout(id);
  }, [monthlyFeeTemp]);

  const handleOpenModalPreview = item => setPreviewInsurance(item);
  const handleCloseModalPreview = () => setPreviewInsurance(null);

  const handleClickPage = useCallback(async value => setCurrentPage(value), []);
  const handleClickToggleFilter = () =>
    setIsShowMoreFilter(preIsShowMoreFilter => !preIsShowMoreFilter);

  const handleClickReset = () => {
    setFilter(FILTER_INSURANCE_DEFAULT);
    setInsuranceNameTemp(undefined);
    setMonthlyFeeTemp(undefined);
  };

  const handleChangeInsuranceName = e => setInsuranceNameTemp(e.target.value);
  const handleChangeMonthlyFee = e => {
    const value = e.target.value;

    const formatValue = value.replace(/\./g, '');

    if (!formatValue || !isNaN(parseFloat(formatValue))) {
      setMonthlyFeeTemp(formatValue);
    }
  };

  const handleSelectStatus = value =>
    setFilter(preFilter => ({ ...preFilter, status: value }));
  const handleSelectInsuranceCategory = value =>
    setFilter(preFilter => ({ ...preFilter, insurance_category_label: value }));

  const isDisableResetButton = useMemo(
    () => Object.values(filter).every(item => !item),
    [filter]
  );

  return {
    isLoading,
    isShowMoreFilter,
    isDisableResetButton,
    filter,
    previewInsurance,
    insuranceNameTemp,
    monthlyFeeTemp,
    insurances,
    currentPage,
    totalPages,
    handleClickPage,
    handleClickToggleFilter,
    handleClickReset,
    handleChangeInsuranceName,
    handleChangeMonthlyFee,
    handleSelectStatus,
    handleSelectInsuranceCategory,
    handleOpenModalPreview,
    handleCloseModalPreview,
    handleGetInsurances,
  };
};
