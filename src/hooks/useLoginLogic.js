import { useState, useCallback } from 'react';
import { useForm } from 'react-hook-form';
import { signIn } from 'next-auth/react';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';

import { REGEX_EMAIL, REGEX_PHONE, ROUTE } from '@constants';
import { sentOtp } from '@services/auth';
import { handleError } from '@helpers/handleError';
import { notification } from 'antd';

const ACCEPTED_LIST_ALPHABET_WITH_COMMAND = ['a', 'v', 'c', 'x'];
const ACCEPTED_LIST_KEY = [
  'Backspace',
  'ArrowLeft',
  'ArrowRight',
  'Control',
  'Meta',
];

export const DESTINATION_TYPE = {
  EMAIL: 'email',
  PHONE: 'phone',
};

const OPERATOR_NOT_ACCEPTED = [',', '.'];

const KEY_ENTER = 'Enter';

export const useLoginLogic = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [isFormOtp, setIsFormOtp] = useState(false);
  const [otpIsString, setOtpIsString] = useState(false);

  const [destination, setDestination] = useState(null);
  const [destinationType, setDestinationType] = useState('email');
  const [code, setCode] = useState('');

  const { t } = useTranslation(['login', 'errors']);
  const {
    getValues,
    register,
    formState: { errors },
    handleSubmit,
    resetField,
  } = useForm();
  const router = useRouter();

  const handleConvertDestination = value =>
    value.slice(0, value.length - 3) + '***';
  const handleBodyAndDestination = useCallback(
    destination => {
      if (
        destinationType !== DESTINATION_TYPE.EMAIL &&
        destinationType !== DESTINATION_TYPE.PHONE
      )
        return null;

      if (destinationType === DESTINATION_TYPE.EMAIL) {
        const [username, emailDomain] = destination.split('@');
        const body = { destination_type: destinationType, email: destination };
        const updateDestination = [
          handleConvertDestination(username),
          emailDomain,
        ].join('@');

        return { body, updateDestination };
      }

      const body = { destination_type: destinationType, phone: destination };
      const updateDestination = handleConvertDestination(destination);

      return { body, updateDestination };
    },
    [destinationType]
  );
  const handleSendOtp = useCallback(
    async destination => {
      try {
        const result = handleBodyAndDestination(destination);
        if (!result) {
          notification.error({ message: 'Không hợp lệ' });
          return;
        }
        setIsLoading(true);
        await sentOtp(result.body);

        setDestination(result.updateDestination);
        setIsFormOtp(true);
        notification.success({ message: t('login:success.otp') });
      } catch (error) {
        handleError(t, error);
      } finally {
        setIsLoading(false);
      }
    },
    [handleBodyAndDestination, t]
  );
  const handleLogin = useCallback(
    async body => {
      try {
        setIsLoading(true);
        const response = await signIn('credentials', {
          ...body,
          redirect: false,
        });

        if (!response.ok) {
          const [, errorCode] = response.error.split(': ');

          return handleError(t, errorCode);
        }

        notification.success({ message: t('login:success.login') });

        router.push(ROUTE.INSURANCES);
      } catch (error) {
        handleError(t, error);
      } finally {
        setIsLoading(false);
      }
    },
    [router, t]
  );

  const handleValidateDestination = value => {
    if (destinationType === 'email') return REGEX_EMAIL.test(value) || 'email';

    if (destinationType === 'phone') return REGEX_PHONE.test(value) || 'phone';
  };
  const handleSubmitForm = () => {
    if (isFormOtp) {
      const body = {
        ...getValues(),
        destination_type: destinationType,
        otp_code: code,
      };
      handleLogin(body);
    } else {
      const body = getValues();
      handleSendOtp(body?.destination);
    }
  };

  const handleClickDestinationType = value => {
    resetField('destination');
    setDestinationType(value);
  };

  const handleKeyDownOtp = event => {
    const key = event.key;
    const value = event.currentTarget.value;
    const isAcceptKey = [
      ...ACCEPTED_LIST_KEY,
      ...ACCEPTED_LIST_ALPHABET_WITH_COMMAND,
    ].includes(key);

    if (key === KEY_ENTER && value.length === 6) return handleSubmitForm();

    if (OPERATOR_NOT_ACCEPTED.includes(key)) return event.preventDefault();
    if (value.length > 5 && !isAcceptKey) return event.preventDefault();

    const { ctrlKey, metaKey } = event;
    const isCommand =
      (ctrlKey || metaKey) && ACCEPTED_LIST_ALPHABET_WITH_COMMAND.includes(key);

    if (
      isNaN(parseInt(key)) &&
      !isCommand &&
      !ACCEPTED_LIST_KEY.includes(key)
    ) {
      setOtpIsString(true);
    } else {
      setOtpIsString(false);
    }
  };
  const handleChangeOtp = e => setCode(e.target.value);

  return {
    isLoading,
    isFormOtp,
    otpIsString,
    destination,
    destinationType,
    errors,
    code,
    register,
    t,
    handleSubmitForm: handleSubmit(handleSubmitForm),
    handleValidateDestination,
    handleClickDestinationType,
    handleKeyDownOtp,
    handleChangeOtp,
    resetField,
  };
};
