import React from 'react';
import Head from 'next/head';

import SidebarComponent from '@components/Sidebar';
import HeaderComponent from '@components/Header';

const Layout = ({ children, title, button, head }) => {
  return (
    <>
      <Head>
        <title>{head ? `${head} - Sosanh24` : 'Sosanh24'}</title>
      </Head>
      <div className='min-w-screen min-h-screen overflow-x-hidden bg-cultured'>
        <SidebarComponent />
        <HeaderComponent />
        <main className='min-h-screen w-full pl-64'>
          <div className='px-6'>
            {title && (
              <div className='mb-6 flex w-full items-center justify-between rounded-xl bg-white p-4 shadow'>
                {typeof title === 'string' ? (
                  <p className='text-xl font-bold text-nickel'>{title}</p>
                ) : (
                  title
                )}
                {button}
              </div>
            )}
            {children}
          </div>
        </main>
      </div>
    </>
  );
};

export default Layout;
