import ClipboardIconSvg from '@components/Icons/clipboard';
import DocumentIconSvg from '@components/Icons/document';
import EditIconSvg from '@components/Icons/edit';

// Constants
export const REGEX_EMAIL = /^[\w\-.]+@([\w-]+\.)+[\w-]{2,4}$/;
export const REGEX_PHONE = /(84|0[3|5|7|8|9])+([0-9]{8})\b/;

export const ENDPOINTS = {
  AUTH: {
    SENT_OTP: '/v1/api/vendor/otps/login/send-otp',
    PROFILE: '/v1/api/vendor/users/profile',
    CHANGE_PASSWORD: '/v1/api/vendor/auth/change-password',
  },
  BLOGS_USER: '/v1/api/vendor/blogs/user',
  BLOG_CATEGORIES: '/v1/api/client/blog-categories',
  INSURANCE_OBJECTIVES: '/v1/api/client/insurance-objectives',
  INSURANCE_CATEGORIES: '/v1/api/client/insurance-categories',
  INSURANCES: insuranceId =>
    `/v1/api/vendor/insurances${insuranceId ? `/${insuranceId}` : ''}`,
  INSURANCE_HOSPITAL: '/v1/api/vendor/hospitals',
  UPLOAD: {
    BANNER: '/v1/api/vendor/uploads/blogs/banner',
    PDF: '/v1/api/vendor/uploads/insurance/pdf',
  },
  BLOGS: blogId => `/v1/api/vendor/blogs${blogId ? `/${blogId}` : ''}`,
  BLOG_COMMENTS: id => `/v1/api/vendor/blog-comments/blogs/${id}`,
};
export const ROUTE = {
  INSURANCES: '/vendor/insurances',
  CREATE_INSURANCE: '/vendor/insurances/create',
  UPDATE_INSURANCE: '/vendor/insurances/update',
  BLOGS: '/vendor/blogs',
  SETTINGS: '/vendor/settings',
};
export const ROUTERS = [
  {
    label: 'insurances',
    path: ROUTE.INSURANCES,
    icon: isActive => (
      <ClipboardIconSvg className={isActive ? 'fill-azure' : 'fill-nickel'} />
    ),
  },
  {
    label: 'create-insurance',
    path: ROUTE.CREATE_INSURANCE,
    icon: isActive => (
      <EditIconSvg className={isActive ? 'stroke-azure' : 'stroke-nickel'} />
    ),
  },
  {
    label: 'blogs',
    path: ROUTE.BLOGS,
    icon: isActive => (
      <DocumentIconSvg
        className={isActive ? 'stroke-azure' : 'stroke-nickel'}
      />
    ),
  },
];

export const SETTING_TAB_KEY = {
  PROFILE: 'profile',
  PASSWORD: 'password',
};

// Insurances
export const FIELDS_LIFE_INSURANCE = {
  details: ['name', 'description_insurance', 'objective_of_insurance'],
  terms: [
    'age_eligibility',
    'deadline_for_deal',
    'insurance_minimum_fee',
    'insured_person',
    'profession',
    'deadline_for_payment',
    'age_of_contract_termination',
    'termination_conditions',
    'total_sum_insured',
    'monthly_fee',
  ],
  benefits: ['key_benefits'],
  additional_benefits: [
    'death_or_disability',
    'serious_illnesses',
    'health_care',
    'investment_benefit',
    'increasing_value_bonus',
    'for_child',
    'flexible_and_diverse',
    'termination_benefits',
    'expiration_benefits',
    'fee_exemption',
  ],
  customer_orientation: [
    'acceptance_rate',
    'completion_time_deal',
    'end_of_process',
    'withdrawal_time',
    'reception_and_processing_time',
  ],
  document: ['documentation_url', 'benefits_illustration_table'],
};
export const FIELDS_HEALTH_INSURANCE = {
  details: ['name', 'description_insurance'],
  customer_orientation: [
    'reception_and_processing_time',
    'insurance_scope',
    'waiting_period',
    'compensation_process',
  ],
  terms: [
    'age_eligibility',
    'deadline_for_deal',
    'insurance_minimum_fee',
    'insured_person',
    'profession',
    'deadline_for_payment',
    'age_of_contract_termination',
    'total_sum_insured',
    'monthly_fee',
    'customer_segment',
  ],
  inpatient: [
    'room_type',
    'for_cancer',
    'for_illnesses',
    'for_accidents',
    'for_surgical',
    'for_hospitalization',
    'for_intensive_care',
    'for_administrative',
    'for_organ_transplant',
  ],
  obstetric: [
    'give_birth_normally',
    'caesarean_section',
    'obstetric_complication',
    'give_birth_abnormality',
    'after_give_birth_fee',
    'before_discharged',
    'postpartum_childcare_cost',
  ],
  dental: [
    'examination_and_diagnosis',
    'gingivitis',
    'xray_and_diagnostic_imaging',
    'filling_teeth_basic',
    'root_canal_treatment',
    'dental_pathology',
    'dental_calculus',
  ],
  outpatient: [
    'examination_and_treatment',
    'testing_and_diagnosis',
    'home_care',
    'due_to_accident',
    'due_to_illness',
    'due_to_cancer',
    'restore_functionality',
  ],
};

export const FIELDS_HEALTH_INSURANCE_DEFAULT = {
  customer_orientation: [],
  terms: [],
  inpatient: [],
  obstetric: [],
  dental: [],
  outpatient: [],
};

export const FORM_LIFE_INSURANCE_DEFAULT = {
  benefits_illustration_table: null,
  documentation_url: null,
  additional_benefits: {
    death_or_disability: {
      text: null,
      level: 'none',
    },
    serious_illnesses: {
      text: null,
      level: 'none',
    },
    health_care: {
      text: null,
      level: 'none',
    },
    investment_benefit: {
      text: null,
      level: 'none',
    },
    increasing_value_bonus: {
      text: null,
      level: 'none',
    },
    for_child: {
      text: null,
      level: 'none',
    },
    flexible_and_diverse: {
      text: null,
      level: 'none',
    },
    termination_benefits: {
      text: null,
      level: 'none',
    },
    expiration_benefits: {
      text: null,
      level: 'none',
    },
    fee_exemption: {
      text: null,
      level: 'none',
    },
  },
  terms: {
    age_eligibility: {
      to: null,
      from: null,
      level: 'none',
      description: null,
    },
    deadline_for_deal: {
      to: null,
      from: null,
      type: 'year',
      level: 'none',
      value: null,
      description: null,
    },
    insurance_minimum_fee: {
      level: 'none',
      value: null,
      description: null,
    },
    deadline_for_payment: {
      level: 'none',
      value: null,
      description: null,
    },
    insured_person: {
      level: 'none',
      value: null,
      description: null,
    },
    profession: {
      level: 'none',
      text: null,
    },
    age_of_contract_termination: {
      to: null,
      from: null,
      type: null,
      level: 'none',
      description: null,
    },
    termination_conditions: {
      text: null,
      level: 'none',
    },
    total_sum_insured: {
      to: null,
      from: null,
      level: 'none',
      description: null,
    },
    monthly_fee: {
      to: null,
      from: null,
      level: 'none',
      description: null,
    },
  },
  customer_orientation: {
    acceptance_rate: {
      text: null,
      level: 'none',
    },
    completion_time_deal: {
      text: null,
      level: 'none',
    },
    end_of_process: {
      text: null,
      level: 'none',
    },
    withdrawal_time: {
      text: null,
      level: 'none',
    },
    reception_and_processing_time: {
      text: null,
      level: 'none',
    },
  },
};
export const FORM_HEALTH_INSURANCE_DEFAULT = {
  customer_orientation: {
    reception_and_processing_time: {
      level: 'none',
      text: null,
    },
    insurance_scope: {
      level: 'none',
      values: null,
    },
    waiting_period: {
      level: 'none',
      from: null,
      to: null,
      description: null,
    },
    compensation_process: {
      level: 'none',
      description: null,
    },
  },
  terms: {
    age_eligibility: {
      level: 'none',
      from: null,
      to: null,
      description: null,
    },
    deadline_for_deal: {
      level: 'none',
      type: null,
      description: null,
      value: null,
      from: null,
      to: null,
    },
    insurance_minimum_fee: {
      level: 'none',
      value: null,
      description: null,
    },
    insured_person: {
      level: 'none',
      value: null,
      description: null,
    },
    profession: {
      level: 'none',
      text: null,
    },
    deadline_for_payment: {
      level: 'none',
      description: null,
      value: null,
    },
    age_of_contract_termination: {
      level: 'none',
      type: null,
      description: null,
      from: null,
      to: null,
    },
    total_sum_insured: {
      level: 'none',
      description: null,
      from: null,
      to: null,
    },
    monthly_fee: {
      level: 'none',
      description: null,
      from: null,
      to: null,
    },
    customer_segment: {
      level: 'none',
      text: null,
    },
  },
  inpatient: {
    room_type: {
      level: 'none',
      values: null,
      description: null,
    },
    for_cancer: {
      level: 'none',
      value: null,
      description: null,
    },
    for_illnesses: {
      level: 'none',
      value: null,
      description: null,
    },
    for_accidents: {
      level: 'none',
      value: null,
      description: null,
    },
    for_surgical: {
      level: 'none',
      value: null,
      description: null,
    },
    for_hospitalization: {
      level: 'none',
      value: null,
      type: null,
    },
    for_intensive_care: {
      level: 'none',
      value: null,
      description: null,
    },
    for_administrative: {
      level: 'none',
      value: null,
      description: null,
    },
    for_organ_transplant: {
      level: 'none',
      value: null,
      description: null,
    },
  },
  obstetric: {
    give_birth_normally: {
      level: 'none',
      value: null,
      description: null,
    },
    caesarean_section: {
      level: 'none',
      value: null,
      description: null,
    },
    obstetric_complication: {
      level: 'none',
      value: null,
      description: null,
    },
    give_birth_abnormality: {
      level: 'none',
      value: null,
      description: null,
    },
    after_give_birth_fee: {
      level: 'none',
      value: null,
      description: null,
    },
    before_discharged: {
      level: 'none',
      value: null,
      description: null,
    },
    postpartum_childcare_cost: {
      level: 'none',
      value: null,
      description: null,
    },
  },
  dental: {
    examination_and_diagnosis: {
      level: 'none',
      value: null,
      description: null,
    },
    gingivitis: {
      level: 'none',
      value: null,
      description: null,
    },
    xray_and_diagnostic_imaging: {
      level: 'none',
      value: null,
      description: null,
    },
    filling_teeth_basic: {
      level: 'none',
      value: null,
      description: null,
    },
    root_canal_treatment: {
      level: 'none',
      value: null,
      description: null,
    },
    dental_pathology: {
      level: 'none',
      value: null,
      description: null,
    },
    dental_calculus: {
      level: 'none',
      value: null,
      description: null,
    },
  },
  outpatient: {
    examination_and_treatment: {
      level: 'none',
      value: null,
      description: null,
    },
    testing_and_diagnosis: {
      level: 'none',
      value: null,
      description: null,
    },
    home_care: {
      level: 'none',
      value: null,
      description: null,
    },
    due_to_accident: {
      level: 'none',
      value: null,
      description: null,
    },
    due_to_illness: {
      level: 'none',
      value: null,
      description: null,
    },
    due_to_cancer: {
      level: 'none',
      value: null,
      description: null,
    },
    restore_functionality: {
      level: 'none',
      value: null,
      description: null,
    },
  },
  additional_benefit: [],
};
export const FILTER_INSURANCE_DEFAULT = {
  insurance_name: null,
  monthly_fee: null,
  status: null,
  insurance_category_label: null,
};

export const LIST_ICON_OF_FORM_LEVEL = [
  {
    value: 'high',
    icon: 'check-success.svg',
    alt: 'Level Hight',
  },
  {
    value: 'medium',
    icon: 'check-warning.svg',
    alt: 'Level Medium',
  },
  {
    value: 'not-support',
    icon: 'close.svg',
    alt: 'Level Not Support',
  },
  {
    value: 'none',
    icon: 'warning.svg',
    alt: 'Level None',
  },
];

export const LIST_YEAR_OR_OLD = ['year', 'old'];
export const LIST_PERSON = ['one-self', 'other-person'];
export const LIST_PROFESSION = [
  'EMPLOYER',
  'EMPLOYEE',
  'STUDENT',
  'OTHER_PROFESSION',
];
export const LIST_PAY = ['monthly', 'yearly', 'flexible', 'once'];
export const LIST_CUSTOMERS = [
  'INDIVIDUAL',
  'FAMILY',
  'ORGANIZATION',
  'BUSINESS',
];
export const LIST_ROOM_TYPES = [
  'one-bed',
  'two-beds',
  'multi-beds',
  'vip-room',
  'special-room',
];
export const LIST_HOSPITALIZATION = ['by-day', 'by-year'];
export const LIST_INSURANCE_SCOPE = [
  'death',
  'inpatient',
  'outpatient',
  'dental',
  'obstetric',
];
export const LIST_STATUS = ['approved', 'pending', 'rejected'];

export const INSURANCE_CATEGORY = {
  LIFE: 'life',
  HEALTH: 'health',
};
export const HEALTH_INSURANCE_ADDITIONAL_BENEFITS = {
  DENTAL: 'dental',
  OBSTETRIC: 'obstetric',
};
export const STATUS = {
  APPROVED: 'approved',
  PENDING: 'pending',
  REJECTED: 'rejected',
};
export const OPTION_YEAR_OR_OLD = {
  YEAR: 'year',
  OLD: 'old',
};

export const MODEL_ALERT_EDIT_TYPE = {
  INSURANCE: 'insurance',
  BLOG: 'blog',
};

// ENV
export const BaseUrl = process.env.NEXT_PUBLIC_BASE_URL;
