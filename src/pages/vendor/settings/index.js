import React, { useMemo } from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Layout from '@layout';
import { Tabs } from 'antd';
import SettingProfile from '@components/Settings/Profile';
import ChangePassword from '@components/Settings/ChangePassword';
import { useTranslation } from 'next-i18next';
import { SETTING_TAB_KEY } from '@constants';
import { useRouter } from 'next/router';

const Settings = () => {
  const { t } = useTranslation(['settings']);
  const router = useRouter();

  const tabs = [
    {
      key: SETTING_TAB_KEY.PROFILE,
      label: t('settings:profile:title'),
      children: <SettingProfile />,
    },
    {
      key: SETTING_TAB_KEY.PASSWORD,
      label: t('settings:changePassword:title'),
      children: <ChangePassword />,
    },
  ];

  const defaultActiveKey = useMemo(() => {
    const { tab } = router.query;
    switch (tab) {
      case SETTING_TAB_KEY.PROFILE:
        return SETTING_TAB_KEY.PROFILE;
      case SETTING_TAB_KEY.PASSWORD:
        return SETTING_TAB_KEY.PASSWORD;
      default:
        return SETTING_TAB_KEY.PROFILE;
    }
  }, [router]);

  return (
    <>
      <Layout head='Settings'>
        <h4 className='mb-2 text-3.5xl font-bold leading-normal text-arsenic'>
          {t('settings:title')}
        </h4>
        <p className='mb-10 text-sm leading-5 text-nickel'>
          {t('settings:description')}
        </p>
        <Tabs
          defaultActiveKey={defaultActiveKey}
          items={tabs}
          animated
          type='card'
          className='tabs-setting [&>div]:mb-0'
        />
      </Layout>
    </>
  );
};

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'settings',
        'errors',
      ])),
    },
  };
};

export default Settings;
