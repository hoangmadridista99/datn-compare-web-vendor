import React from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { Modal, Select, Spin, Result } from 'antd';
import { BeatLoader } from 'react-spinners';

import FormLifeInsurance from '@components/FormInsurances/Life';
import FormHealthInsurance from '@components/FormInsurances/Health';
import { INSURANCE_CATEGORY } from '@constants';
import Layout from '@layout';
import Image from 'next/image';
import { useUpdateInsuranceLogic } from '@hooks/useUpdateInsuranceLogic';

const UpdateInsurance = ({ id }) => {
  const {
    isLoading,
    isModalAlertUpdateSuccess,
    form,
    insuranceDetails,
    objectives,
    handleClickSave,
    handleCloseModalUpdateSuccess,
    handleFinishForm,
  } = useUpdateInsuranceLogic(id);
  const { t } = useTranslation(['common']);

  const FormInsurance = () => {
    switch (insuranceDetails?.insurance_category?.label) {
      case INSURANCE_CATEGORY.LIFE:
        return (
          <FormLifeInsurance
            objectives={objectives}
            form={form}
            onFinishForm={handleFinishForm}
            initialValues={insuranceDetails}
            isDisabled={!insuranceDetails || isLoading}
          />
        );
      case INSURANCE_CATEGORY.HEALTH:
        return (
          <FormHealthInsurance
            form={form}
            onFinishForm={handleFinishForm}
            isDisabled={!insuranceDetails || isLoading}
            initialValues={insuranceDetails}
          />
        );
      default:
        return null;
    }
  };

  const ContentCreateInsurance = () =>
    insuranceDetails ? (
      <>
        <h2 className='leading-5xl mb-4 text-3.5xl font-bold text-arsenic'>
          {t('insurances:category')}
        </h2>
        <div className='mb-10 flex items-center justify-between'>
          <Select
            className='w-1/5'
            options={[
              {
                value: INSURANCE_CATEGORY.LIFE,
                label: t('insurances:form:options:life'),
              },
              {
                value: INSURANCE_CATEGORY.HEALTH,
                label: t('insurances:form:options:health'),
              },
            ]}
            placeholder={t('insurances:form:placeholder:choose_insurance')}
            value={insuranceDetails?.insurance_category.label}
            disabled
          />
          <button
            className='rounded-lg bg-ultramarine-blue px-10 py-3 font-bold text-cultured'
            onClick={handleClickSave}
            disabled={isLoading}
          >
            {isLoading ? (
              <BeatLoader color='#F8F8F9' margin={0} size={10} loading />
            ) : (
              t('insurances:form:button:update')
            )}
          </button>
        </div>
        <FormInsurance />
      </>
    ) : null;

  return (
    <>
      <Layout
        head={t('common:title.update-insurance')}
        title={t('common:layout.update-insurance')}
      >
        {insuranceDetails && <ContentCreateInsurance />}
        {!insuranceDetails && isLoading && (
          <div className='flex justify-center'>
            <Spin size='large' />
          </div>
        )}
      </Layout>
      <Modal
        open={isModalAlertUpdateSuccess}
        onCancel={handleCloseModalUpdateSuccess}
        centered
        closable={false}
        footer={null}
      >
        <Result
          status='success'
          title={t('insurances:success:update:title')}
          subTitle={t('insurances:success:update:subTitle')}
          icon={
            <div className='flex w-full justify-center'>
              <Image src='/svg/check.svg' width={98} height={98} alt='Check' />
            </div>
          }
          extra={
            <div className='flex w-full justify-center'>
              <button
                className='ml-2 w-1/2 rounded-lg bg-ultramarine-blue py-2 font-bold text-cultured'
                type='button'
                onClick={handleCloseModalUpdateSuccess}
                key='form-insurance-button'
              >
                {t('insurances:success:button:go-insurances')}
              </button>
            </div>
          }
        />
      </Modal>
    </>
  );
};

export const getServerSideProps = async ({ locale, query }) => {
  const { id } = query;
  if (!id || typeof id !== 'string')
    return {
      notFound: true,
    };
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'insurances',
        'common',
        'errors',
      ])),
      id,
    },
  };
};

export default UpdateInsurance;
