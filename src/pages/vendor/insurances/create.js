import React from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { Modal, Select, notification, Result } from 'antd';
import { BeatLoader } from 'react-spinners';

import FormLifeInsurance from '@components/FormInsurances/Life';
import FormHealthInsurance from '@components/FormInsurances/Health';
import { INSURANCE_CATEGORY } from '@constants';
import Layout from '@layout';
import { useCreateInsuranceLogic } from '@hooks/useCreateInsuranceLogic';
import Link from 'next/link';
import Image from 'next/image';

const CreateInsurance = () => {
  const {
    form,
    handleCloseModalCreateSuccess,
    handleFinishForm,
    handleSelectInsuranceCategory,
    insuranceCategory,
    isLoading,
    isModalCreateSuccess,
    objectives,
  } = useCreateInsuranceLogic();
  const { t } = useTranslation(['common']);

  const handleClickSave = async () => {
    try {
      await form.validateFields();
      if (insuranceCategory === INSURANCE_CATEGORY.LIFE) {
        const key_benefits = form.getFieldValue('key_benefits');
        if (!key_benefits) {
          notification.error({ message: t('insurances:errors:key_benefits') });
          return;
        }
      }
      form.submit();
    } catch (error) {
      notification.error({
        message: t('insurances:errors:default'),
      });
    }
  };

  const FormInsurance = () => {
    switch (insuranceCategory) {
      case INSURANCE_CATEGORY.LIFE:
        return (
          <FormLifeInsurance
            objectives={objectives}
            form={form}
            onFinishForm={handleFinishForm}
            isDisabled={isLoading}
          />
        );
      case INSURANCE_CATEGORY.HEALTH:
        return (
          <FormHealthInsurance
            form={form}
            onFinishForm={handleFinishForm}
            isDisabled={isLoading}
          />
        );
      default:
        return null;
    }
  };

  const ContentCreateInsurance = () => (
    <>
      <h2 className='leading-5xl mb-4 text-3.5xl font-bold text-arsenic'>
        {t('insurances:category')}
      </h2>
      <div className='mb-10 flex items-center justify-between'>
        <Select
          className='w-1/5'
          options={[
            {
              value: INSURANCE_CATEGORY.LIFE,
              label: t('insurances:form:options:life'),
            },
            {
              value: INSURANCE_CATEGORY.HEALTH,
              label: t('insurances:form:options:health'),
            },
          ]}
          placeholder={t('insurances:form:placeholder:choose_insurance')}
          value={insuranceCategory}
          onChange={handleSelectInsuranceCategory}
        />
        <button
          className='rounded-lg bg-ultramarine-blue px-10 py-3 font-bold text-cultured'
          onClick={handleClickSave}
          disabled={isLoading}
        >
          {isLoading ? (
            <BeatLoader color='#F8F8F9' margin={0} size={10} loading />
          ) : (
            t('insurances:form:button:create')
          )}
        </button>
      </div>
      <FormInsurance />
    </>
  );

  return (
    <>
      <Layout head={t('common:title.create-insurance')}>
        <ContentCreateInsurance />
      </Layout>
      <Modal
        open={isModalCreateSuccess}
        onCancel={handleCloseModalCreateSuccess}
        centered
        closable={false}
        footer={null}
        width='60%'
      >
        <Result
          status='success'
          title={t('insurances:success:create:title')}
          subTitle={t('insurances:success:create:subTitle')}
          className='result-insurances'
          icon={
            <div className='flex w-full justify-center'>
              <Image src='/svg/check.svg' width={98} height={98} alt='Check' />
            </div>
          }
          extra={
            <div className='flex w-full items-center justify-center gap-4'>
              <Link
                className='w-1/3 rounded-lg border border-nickel py-2 font-bold text-nickel'
                key='form-insurance-link'
                href='/vendor/insurances'
              >
                {t('insurances:success:button:go-insurances')}
              </Link>
              <button
                className='w-1/3 rounded-lg border border-ultramarine-blue bg-ultramarine-blue py-2 font-bold text-cultured'
                type='button'
                onClick={handleCloseModalCreateSuccess}
                key='form-insurance-button'
              >
                {t('insurances:success:button:create-insurance')}
              </button>
            </div>
          }
        />
      </Modal>
    </>
  );
};

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'insurances',
        'common',
        'errors',
      ])),
    },
  };
};

export default CreateInsurance;
