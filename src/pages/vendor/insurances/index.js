/* eslint-disable jsx-a11y/role-supports-aria-props */
import React from 'react';
import { Input, Spin, Select, Pagination, Empty } from 'antd';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Image from 'next/image';

import { INSURANCE_CATEGORY, LIST_STATUS } from '@constants';
import Layout from '@layout';
import CardInsurance from '@components/Card/Insurance';
import { useInsurancesLogic } from '@hooks/useInsurancesLogic';
import { formatCurrency } from '@helpers/formatCurrency';
import PreviewModalInsurance from '@components/PreviewModal/Insurance';
import { useInputNumber } from '@hooks/useInputNumber';

const Insurances = () => {
  const {
    isDisableResetButton,
    isLoading,
    currentPage,
    filter,
    insuranceNameTemp,
    insurances,
    isShowMoreFilter,
    monthlyFeeTemp,
    previewInsurance,
    totalPages,
    handleChangeInsuranceName,
    handleChangeMonthlyFee,
    handleClickPage,
    handleClickReset,
    handleClickToggleFilter,
    handleCloseModalPreview,
    handleOpenModalPreview,
    handleSelectInsuranceCategory,
    handleSelectStatus,
    handleGetInsurances,
  } = useInsurancesLogic();
  const { t } = useTranslation(['common', 'insurances']);
  const { onKeyDown } = useInputNumber();

  const handleKeyDownMonthlyFee = event => onKeyDown(event, 'monthlyFee');
  const handleOptionsStatus = () =>
    LIST_STATUS.map(item => ({
      value: item,
      label: t(`common:status:${item}`),
    }));

  return (
    <>
      <Layout head={t('common:title.insurances')}>
        <div className='rounded-lg bg-white px-4 py-6 shadow'>
          <div className='flex items-center justify-between'>
            <Input
              size='large'
              placeholder={t('insurances:filter.name')}
              prefix={
                <Image
                  src='/svg/search.svg'
                  width={24}
                  height={24}
                  alt='Search'
                />
              }
              className='w-200 text-base'
              onChange={handleChangeInsuranceName}
              value={insuranceNameTemp}
            />
            <button
              type='button'
              className='flex items-center justify-center rounded-lg border border-nickel px-4 py-2 text-base font-normal text-nickel aria-checked:border-ultramarine-blue aria-checked:text-ultramarine-blue'
              aria-checked={isShowMoreFilter}
              onClick={handleClickToggleFilter}
            >
              <Image
                src={
                  isShowMoreFilter ? '/svg/filter.svg' : '/svg/no-filter.svg'
                }
                width={18}
                height={18}
                alt='Filter'
                className='mr-2'
              />
              {t('insurances:filter.button')}
            </button>
          </div>
          <div
            className='hidden overflow-hidden aria-checked:block'
            aria-checked={isShowMoreFilter}
          >
            <div className='mt-6 flex items-center justify-between'>
              <Select
                options={[
                  {
                    value: INSURANCE_CATEGORY.LIFE,
                    label: t('insurances:form:options:life'),
                  },
                  {
                    value: INSURANCE_CATEGORY.HEALTH,
                    label: t('insurances:form:options:health'),
                  },
                ]}
                allowClear
                size='large'
                className='w-full text-nickel placeholder:text-nickel'
                onChange={handleSelectInsuranceCategory}
                value={filter.insurance_category_label}
                placeholder={t('insurances:form:placeholder:choose_insurance')}
              />
              <Input
                placeholder={t('insurances:filter.price')}
                allowClear={false}
                className='mx-6 w-full text-nickel placeholder:text-nickel'
                size='large'
                onChange={handleChangeMonthlyFee}
                value={
                  monthlyFeeTemp ? formatCurrency(monthlyFeeTemp) : undefined
                }
                onKeyDown={handleKeyDownMonthlyFee}
              />
              <Select
                options={handleOptionsStatus()}
                size='large'
                className='w-full text-nickel placeholder:text-nickel'
                onChange={handleSelectStatus}
                value={filter.status}
                allowClear
                placeholder={t('insurances:form:placeholder:choose_status')}
              />
              <button
                type='button'
                className='ml-4 rounded-lg bg-ultramarine-blue px-6 px-6 py-2 py-2 text-base text-cultured aria-disabled:bg-platinum aria-disabled:text-silver-metallic'
                disabled={isDisableResetButton}
                aria-disabled={isDisableResetButton}
                onClick={handleClickReset}
              >
                Reset
              </button>
            </div>
          </div>
          <div className='mt-8 flex items-center px-4'>
            <p className='w-3/12 text-base font-bold text-silver-metallic'>
              {t('insurances:header.name')}
            </p>
            <p className='w-3/12 text-base font-bold text-silver-metallic'>
              {t('insurances:header.expense')}
            </p>
            <p className='w-2/12 text-base font-bold text-silver-metallic'>
              {t('insurances:header.created')}
            </p>
            <p className='w-2/12 text-base font-bold text-silver-metallic'>
              {t('insurances:header.status')}
            </p>
            <p className='w-2/12 text-base font-bold text-silver-metallic' />
          </div>
          {!insurances && isLoading && (
            <div className='mt-10 flex justify-center'>
              <Spin size='large' />
            </div>
          )}
          {insurances && (
            <>
              <div className='mt-4'>
                {insurances.length > 0 &&
                  insurances.map(item => (
                    <CardInsurance
                      key={item.id}
                      data={item}
                      handleOpenModalPreview={handleOpenModalPreview}
                      isLoading={isLoading}
                      handleGetInsurances={handleGetInsurances}
                    />
                  ))}
                {insurances.length === 0 && !isLoading && (
                  <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                )}
                {insurances.length === 0 && isLoading && (
                  <div className='mt-5 flex justify-center'>
                    <Spin size='large' />
                  </div>
                )}
              </div>
              <Pagination
                current={currentPage}
                total={totalPages}
                pageSize={1}
                showSizeChanger={false}
                hideOnSinglePage
                className='mt-4 text-right'
                onChange={handleClickPage}
              />
            </>
          )}
        </div>
      </Layout>
      {insurances && (
        <PreviewModalInsurance
          previewInsurance={previewInsurance}
          onClose={handleCloseModalPreview}
        />
      )}
    </>
  );
};

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'insurances',
        'common',
        'modal',
        'errors',
      ])),
    },
  };
};

export default Insurances;
