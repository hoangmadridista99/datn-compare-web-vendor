/* eslint-disable jsx-a11y/role-supports-aria-props */
import React, { useState, useEffect, useCallback, useMemo } from 'react';
import { Spin, Input, DatePicker, Select, Pagination, Empty } from 'antd';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Image from 'next/image';

import Layout from '@layout';
import { getCategories, getBlogs } from '@services/blogs';
import { handleError } from '@helpers/handleError';
import CardBlog from '@components/Card/Blog';
import { useTranslation } from 'next-i18next';
import { LIST_STATUS } from '@constants';
import ModalUpdateBlog from '@components/Blog/ModalUpdate';
import ModalCreateBlog from '@components/Blog/ModalCreate';
import ModalAlertCreateBlogSuccess from '@components/Blog/ModalAlertCreateSuccess';
import PreviewModalBlog from '@components/PreviewModal/Blog';

const FILTER_BLOG_DEFAULT = {
  title: null,
  created_at: null,
  blog_category_id: null,
  status: null,
};

const Blogs = ({ categories, locale }) => {
  const [isLoading, setIsLoading] = useState(false);

  const [isOpenModalCreate, setIsOpenModalCreate] = useState(false);
  const [isOpenModalAlertCreateSuccess, setIsOpenModalAlertCreateSuccess] =
    useState(false);

  const [blogs, setBlogs] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);

  const [previewBlogId, setPreviewBlogId] = useState(null);
  const [updateBlogId, setUpdateBlogId] = useState(null);

  const [filter, setFilter] = useState(() => FILTER_BLOG_DEFAULT);
  const [titleTemp, setTitleTemp] = useState(null);

  const { t } = useTranslation(['common', 'blogs', 'errors']);

  const handleGetBlogs = useCallback(
    async signal => {
      try {
        setIsLoading(true);
        const response = await getBlogs(currentPage, filter, signal);
        setBlogs(response?.data ?? []);
        setTotalPages(response?.meta?.totalPages ?? 0);
      } catch (error) {
        handleError(t, error);
      } finally {
        setIsLoading(false);
      }
    },
    [currentPage, filter, t]
  );

  useEffect(() => {
    const controller = new AbortController();

    handleGetBlogs(controller.signal);

    return () => controller.abort();
  }, [handleGetBlogs]);
  useEffect(() => {
    let id;

    if (titleTemp !== null) {
      id = setTimeout(
        () => setFilter(preFilter => ({ ...preFilter, title: titleTemp })),
        500
      );
    }

    return () => clearTimeout(id);
  }, [titleTemp]);

  const handleOpenModalUpdateBlog = id => setUpdateBlogId(id);
  const handleCloseModalUpdateBlog = () => setUpdateBlogId(null);
  const handleOpenModalPreviewBlog = id => setPreviewBlogId(id);
  const handleCloseModalPreviewBlog = () => setPreviewBlogId(null);

  const handleToggleModalCreate = isOpen => setIsOpenModalCreate(isOpen);
  const handleCloseModalAlertCreateBlogSuccess = () =>
    setIsOpenModalAlertCreateSuccess(false);
  const handleContinueCreateInModalAlertCreateBlogSuccess = () => {
    setIsOpenModalAlertCreateSuccess(false);
    setIsOpenModalCreate(true);
  };

  const handleCreateSuccess = () => {
    handleClickReset();
    setIsOpenModalCreate(false);
    setIsOpenModalAlertCreateSuccess(true);
  };
  const handleUpdateSuccess = item => {
    const updateBlogs = blogs.map(blog => {
      if (blog.id === item.id) return { ...blog, ...item, status: 'pending' };

      return blog;
    });

    setBlogs(updateBlogs);
  };

  const handleClickReset = () => {
    if (JSON.stringify(filter) === JSON.stringify(FILTER_BLOG_DEFAULT)) {
      handleGetBlogs(1, {});
      return;
    }

    setTitleTemp(undefined);
    setFilter(FILTER_BLOG_DEFAULT);
  };
  const handleClickPage = async page => {
    handleGetBlogs(page, filter).then(() => setCurrentPage(page));
  };

  const hanldeChangeTitle = e => setTitleTemp(e.target.value);

  const handleSelectFilter = (key, value) =>
    setFilter(preFilter => ({ ...preFilter, [key]: value }));

  const handleOptions = () =>
    LIST_STATUS.map(item => ({
      value: item,
      label: t(`common:status:${item}`),
    }));

  // Validation
  const isDisableReset = useMemo(
    () => Object.values(filter).every(item => !item),
    [filter]
  );

  return (
    <>
      <Layout head='Blogs'>
        {blogs && (
          <div className='rounded-lg bg-white px-4 py-6 shadow'>
            <div className='flex items-center justify-between'>
              <Input
                size='large'
                placeholder={t('blogs:filter:placeholder:name')}
                prefix={
                  <Image
                    src='/svg/search.svg'
                    width={24}
                    height={24}
                    alt='Search'
                  />
                }
                className='w-200 text-base'
                onChange={hanldeChangeTitle}
                value={titleTemp}
                disabled={isLoading}
              />
              <button
                className='rounded-lg bg-ultramarine-blue px-6 py-2 text-cultured'
                onClick={() => handleToggleModalCreate(true)}
              >
                {t('blogs:button:create')}
              </button>
            </div>
            <div className='block'>
              <div className='mt-6 flex items-center justify-between'>
                <DatePicker
                  placeholder={t('blogs:filter:placeholder:createdAt')}
                  suffixIcon={
                    <Image
                      src='/svg/calendar.svg'
                      width={18}
                      height={18}
                      alt='Calendar'
                    />
                  }
                  allowClear
                  className='text_placeholder w-full text-nickel placeholder:text-nickel'
                  size='large'
                  disabled={isLoading}
                  onChange={value =>
                    handleSelectFilter(
                      'created_at',
                      value ? value.toISOString() : null
                    )
                  }
                />
                <Select
                  options={categories}
                  size='large'
                  disabled={isLoading}
                  className='text_placeholder mx-4 w-full text-nickel placeholder:text-nickel'
                  onChange={value =>
                    handleSelectFilter('blog_category_id', value)
                  }
                  value={filter.blog_category_id}
                  allowClear
                  placeholder={t('blogs:filter.options.choose_category')}
                />
                <Select
                  placeholder={t('blogs:filter.options.choose_status')}
                  options={handleOptions()}
                  size='large'
                  disabled={isLoading}
                  className='text_placeholder w-full text-nickel placeholder:text-nickel'
                  onChange={value => handleSelectFilter('status', value)}
                  value={filter.status}
                  allowClear
                />
                <button
                  type='button'
                  className='ml-4 rounded-lg bg-ultramarine-blue px-6 px-6 py-2 py-2 text-base text-cultured aria-disabled:bg-platinum aria-disabled:text-silver-metallic'
                  disabled={isLoading || isDisableReset}
                  aria-disabled={isLoading || isDisableReset}
                  onClick={handleClickReset}
                >
                  Reset
                </button>
              </div>
            </div>
            <div className='mt-8 flex items-center px-4'>
              <p className='w-2/12 pr-6 text-base font-bold text-silver-metallic'>
                {t('blogs:header:banner')}
              </p>
              <p className='w-4/12 text-base font-bold text-silver-metallic'>
                {t('blogs:header:title')}
              </p>
              <p className='w-2/12 text-base font-bold text-silver-metallic'>
                {t('blogs:header:createdAt')}
              </p>
              <p className='w-2/12 text-base font-bold text-silver-metallic'>
                {t('blogs:header:status')}
              </p>
              <p className='w-2/12 text-base font-bold text-silver-metallic' />
            </div>
            <div className='mt-4'>
              {blogs.length > 0 &&
                blogs.map(item => (
                  <CardBlog
                    key={item.id}
                    handleOpenModalPreviewBlog={handleOpenModalPreviewBlog}
                    handleOpenModalUpdateBlog={handleOpenModalUpdateBlog}
                    handleGetBlogs={handleGetBlogs}
                    isLoading={isLoading}
                    data={item}
                  />
                ))}
              {blogs.length === 0 && !isLoading && (
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
              )}
              {blogs.length === 0 && isLoading && (
                <div className='mt-5 flex justify-center'>
                  <Spin size='large' />
                </div>
              )}
            </div>
            <Pagination
              current={currentPage}
              total={totalPages}
              pageSize={1}
              showSizeChanger={false}
              hideOnSinglePage
              className='mt-4 text-right'
              onChange={handleClickPage}
            />
          </div>
        )}
        {!blogs && (
          <div className='flex justify-center'>
            <Spin size='large' />
          </div>
        )}
      </Layout>
      {blogs && (
        <>
          <ModalUpdateBlog
            blogCategories={categories}
            blogId={updateBlogId}
            onClose={handleCloseModalUpdateBlog}
            onUpdateSuccess={handleUpdateSuccess}
          />
          <ModalCreateBlog
            isOpen={isOpenModalCreate}
            onClose={() => handleToggleModalCreate(false)}
            onCreateSuccess={handleCreateSuccess}
            blogCategories={categories}
          />
          <ModalAlertCreateBlogSuccess
            isOpen={isOpenModalAlertCreateSuccess}
            onClose={handleCloseModalAlertCreateBlogSuccess}
            onContinueCreate={handleContinueCreateInModalAlertCreateBlogSuccess}
          />
          <PreviewModalBlog
            blogId={previewBlogId}
            onClose={handleCloseModalPreviewBlog}
            locale={locale}
          />
        </>
      )}
    </>
  );
};

export const getServerSideProps = async ({ locale }) => {
  const responseCategories = await getCategories();
  const categories = responseCategories.map(item => ({
    value: item.id,
    label: locale === 'vi' ? item.vn_label : item.en_label,
  }));

  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'blogs',
        'modal',
        'errors',
      ])),
      categories,
      locale,
    },
  };
};

export default Blogs;
