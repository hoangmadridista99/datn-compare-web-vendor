import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';

import { BaseUrl } from '@constants';

export const AuthOptions = {
  providers: [
    CredentialsProvider({
      type: 'credentials',
      credentials: {},
      async authorize(credentials) {
        try {
          const { destination, password, destination_type, otp_code } =
            credentials;
          const url = `${BaseUrl}/v1/api/vendor/auth/login`;
          const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify({
              destination,
              password,
              destination_type,
              otp_code,
            }),
            headers: { 'Content-Type': 'application/json' },
          });
          const result = await response.json();

          if (response.status !== 201) throw new Error(result.error_code);

          if (response.ok) {
            const { first_name, last_name, accessToken, ...data } = result;

            return {
              accessToken,
              user: {
                ...data,
                first_name,
                last_name,
              },
            };
          }

          return null;
        } catch (error) {
          throw new Error(error);
        }
      },
    }),
  ],

  callbacks: {
    async jwt({ token, trigger, user: data, session }) {
      if (data) {
        const { accessToken, user } = data;
        const { id, role, ...dataUser } = user;
        token = {
          accessToken,
          id,
          role,
          user: dataUser,
        };
      }

      if (trigger === 'update') {
        token.user = session;
      }
      return token;
    },

    async session({ session, token }) {
      // Send properties to the client, like an access_token from a provider.
      session.accessToken = token.accessToken;
      session.role = token.role;
      session.user = token.user;

      return session;
    },

    async signIn() {
      return true;
    },
  },

  session: {
    strategy: 'jwt',
    maxAge: 30 * 24 * 60 * 60 * 365,
    updateAge: 24 * 60 * 60,
  },

  pages: {
    signIn: '/auth/login',
  },

  debug: true,
};

export default NextAuth(AuthOptions);
