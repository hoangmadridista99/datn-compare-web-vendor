import React, { useEffect, useState } from 'react';
import { appWithTranslation } from 'next-i18next';
import { SessionProvider } from 'next-auth/react';
import { useRouter } from 'next/router';

import '../styles/style.css';

import Loading from '@components/Loading';

const MyApp = ({ Component, pageProps: { session, ...pageProps } }) => {
  const [isLoading, setIsLoading] = useState(false);

  const router = useRouter();

  useEffect(() => {
    const handleStart = () => setIsLoading(true);

    router.events.on('routeChangeStart', handleStart);

    return () => router.events.off('routeChangeStart', handleStart);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    const handleComplete = () => setIsLoading(false);

    router.events.on('routeChangeComplete', handleComplete);

    return () => router.events.off('routeChangeComplete', handleComplete);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <SessionProvider session={session}>
      {isLoading && <Loading />}
      <Component {...pageProps} />
    </SessionProvider>
  );
};

export default appWithTranslation(MyApp);
