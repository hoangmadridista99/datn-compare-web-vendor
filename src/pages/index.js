import React from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Image from 'next/image';
import { getServerSession } from 'next-auth/next';
import { AuthOptions } from './api/auth/[...nextauth]';

export default function Home() {



  return (
    <>
      <Head>Vendor - Sosanh24</Head>
      
      <header className='flex items-center justify-between p-3 md:p-6'>
        <Link href='/'>
          <Image src='/img/logo.png' alt='logo' width={125} height={32} />
        </Link>
        <Link
          href='/auth/login'
          className='rounded-lg bg-ultramarine-blue px-3 py-1 text-cultured'
        >
          Đăng nhập
        </Link>
      </header>
    </>
  );
}

export const getServerSideProps = async ({ req, res }) => {
  const session = await getServerSession(req, res, AuthOptions);

  if (session && session.user && session.role)
    return {
      redirect: {
        destination: '/vendor/insurances',
        permanent: true,
      },
    };

  return {
    props: {},
  };
};
