/* eslint-disable indent */
/* eslint-disable jsx-a11y/role-supports-aria-props */
import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { BeatLoader } from 'react-spinners';
import Head from 'next/head';
import classNames from 'class-names';
import { getServerSession } from 'next-auth/next';
import { AuthOptions } from '@pages/api/auth/[...nextauth]';

import { DESTINATION_TYPE, useLoginLogic } from '@hooks/useLoginLogic';
import { useInputNumber } from '@hooks/useInputNumber';

const AuthLogin = () => {
  const {
    code,
    destination,
    destinationType,
    errors,
    isFormOtp,
    isLoading,
    otpIsString,
    t,
    register,
    handleChangeOtp,
    handleClickDestinationType,
    handleKeyDownOtp,
    handleSubmitForm,
    handleValidateDestination,
    resetField,
  } =             useLoginLogic();
  const { errors: errorsHook, message, onKeyDown } = useInputNumber();
  const { onChange: onChangeDestination, ...registerDestination } = register(
    'destination',
    {
      validate: handleValidateDestination,
    }
  );

  const          handleChangeDestination = event => {
    if (
      destinationType === DESTINATION_TYPE.PHONE &&
      !!errorsHook['destination']
    ) {
      resetField('destination');
      return;
    }

    return onChangeDestination(event);
  };

  return (
    <>
      <Head>
        <title>Login Vendor - Sosanh24</title>
      </Head>
      <div className='flex h-screen w-screen overflow-hidden lg:from-ultramarine-blue lg:to-egyptian-blue lg:bg-gradient-[247deg]'>
        <div className='flex h-full w-full flex-col items-center justify-center bg-cultured px-3 lg:z-2 lg:w-1/2 lg:justify-start lg:rounded-tr-3xl lg:py-10 xl:px-15 2xl:px-27'>
          <Link href='/' className='mb-10 lg:self-start lg:pl-5'>
            <Image src='/img/logo.png' alt='logo' width={125} height={32} />
          </Link>
          <div className='relative w-full rounded-lg bg-white p-5 shadow sm:w-4/5 md:w-2/3 lg:w-full lg:translate-y-1/4 lg:bg-transparent lg:shadow-none'>
            <div
              aria-hidden={isFormOtp}
              className='opacity-1 scale-1 blur-none transition duration-300 aria-hidden:absolute aria-hidden:-z-1 aria-hidden:w-full aria-hidden:scale-95 aria-hidden:opacity-0 aria-hidden:blur-lg'
            >
              <h1 className='leading-5xl mb-4 text-center text-3.5xl font-bold text-arsenic lg:mb-10 lg:text-start'>
                {t('login:title')}
              </h1>
              <div className='mb-2 flex items-center justify-evenly sm:mb-4 lg:mb-6 lg:justify-start'>
                <button
                  type='button'
                  className='rounded-lg bg-cultured px-3 py-2 font-semibold text-spanish-gray transition duration-500 hover:bg-platinum hover:text-arsenic aria-selected:bg-platinum aria-selected:text-arsenic md:px-5 md:py-3'
                  aria-selected={destinationType === 'email'}
                  onClick={() => handleClickDestinationType('email')}
                >
                  {t('login:type.email')}
                </button>
                <button
                  type='button'
                  className='rounded-lg bg-cultured px-3 py-2 font-semibold text-spanish-gray transition duration-500 hover:bg-platinum hover:text-arsenic aria-selected:bg-platinum aria-selected:text-arsenic md:px-5 md:py-3'
                  aria-selected={destinationType === 'phone'}
                  onClick={() => handleClickDestinationType('phone')}
                >
                  {t('login:type.phone')}
                </button>
              </div>
              <form
                onSubmit={handleSubmitForm}
                className='space-y-4 md:space-y-6'
              >
                <div className='mb-6'>
                  <label
                    htmlFor='destination'
                    className='mb-2 block font-bold text-arsenic'
                  >
                    {t(`login:destination.${destinationType}`)}
                  </label>
                  <input
                    type='text'
                    name='destination'
                    id='destination'
                    className='w-full rounded-md border border-chinese-silver p-3 placeholder:text-chinese-silver'
                    placeholder={t(`login:placeholder.${destinationType}`)}
                    required
                    {...registerDestination}
                    onChange={handleChangeDestination}
                    disabled={isLoading}
                    onKeyDown={
                      destinationType === DESTINATION_TYPE.PHONE
                        ? event => {
                            return onKeyDown(event, 'destination');
                          }
                        : undefined
                    }
                  />
                  {errors.destination && (
                    <p className='mt-1.5 text-sm font-semibold text-red-600'>
                      {t(
                        `login:error.destination.${errors.destination.message}`
                      )}
                    </p>
                  )}
                  {destinationType === DESTINATION_TYPE.PHONE &&
                    errorsHook['destination'] && (
                      <p className='mt-1.5 text-sm font-semibold text-red-600'>
                        {message}
                      </p>
                    )}
                </div>
                <div className='mb-6'>
                  <label
                    htmlFor='password'
                    className='mb-2 block font-bold text-arsenic'
                  >
                    {t('login:password')}
                  </label>
                  <input
                    type='password'
                    name='password'
                    id='password'
                    placeholder={t('login:placeholder.password')}
                    className='w-full rounded-md border border-chinese-silver p-3 placeholder:text-chinese-silver'
                    required
                    {...register('password', {
                      required: true,
                      minLength: 8,
                      maxLength: 30,
                    })}
                    disabled={isLoading}
                  />
                  {errors.password && (
                    <p className='mt-1.5 text-sm font-semibold text-red-600'>
                      {t(
                        `login:error.password.${
                          errors.password.type === 'minLength' ? 'min' : 'max'
                        }`
                      )}
                    </p>
                  )}
                </div>
                <button
                  type='submit'
                  className={classNames(
                    'w-full rounded-lg bg-ultramarine-blue px-5 py-2.5 text-center text-sm font-medium text-white hover:bg-interdimensional-blue focus:outline-none focus:ring-4',
                    {
                      'animate-pulse': isLoading,
                    }
                  )}
                  disabled={isLoading}
                >
                  <BeatLoader
                    color='#fff'
                    margin={0}
                    size={10}
                    loading={isLoading}
                  />
                  {!isLoading && t('button')}
                </button>
              </form>
              <p className='mt-6 text-center text-sm font-light text-gray-500'>
                <span className='text-nickel'>{`${t(
                  'login:signup.title'
                )} `}</span>
                <a
                  href='#'
                  className='dark:text-primary-500 font-medium text-ultramarine-blue underline'
                >
                  {t('login:signup.button')}
                </a>
              </p>
            </div>
            <div
              aria-hidden={!isFormOtp}
              className='opacity-1 scale-1 duration-2000 mt-20 blur-none transition aria-hidden:absolute aria-hidden:-z-1 aria-hidden:mr-6 aria-hidden:w-1/2 aria-hidden:scale-95 aria-hidden:opacity-0 aria-hidden:blur-lg'
            >
              <h1 className='leading-5xl mb-2 text-center text-3.5xl font-bold text-arsenic lg:text-start'>
                {t('login:otp.title')}
              </h1>
              <p className='mb-5 text-center text-nickel lg:mb-10'>
                {`${t('login:otp.description')} ${t(
                  `otp.${destinationType}`
                )} ${destination}`}
              </p>
              <form onSubmit={handleSubmitForm}>
                <div className='mb-6'>
                  <label
                    htmlFor='otp_code'
                    className='mb-2 block font-bold text-arsenic'
                  >
                    {t('login:otp.label')}
                  </label>
                  <input
                    type='number'
                    name='otp_code'
                    id='otp_code'
                    className='input-number-remove-arrow w-full rounded-md border border-chinese-silver p-3 placeholder:text-chinese-silver'
                    placeholder={t('login:placeholder.otp')}
                    onKeyDown={handleKeyDownOtp}
                    value={code}
                    onChange={handleChangeOtp}
                    disabled={isLoading}
                  />
                  {otpIsString && (
                    <p className='mt-1.5 text-sm font-semibold text-red-600'>
                      {t('login:otp.error')}
                    </p>
                  )}
                </div>
                <button
                  type='submit'
                  className='m-0 w-full rounded-lg bg-ultramarine-blue px-5 py-2.5 text-center text-sm font-medium text-white hover:bg-interdimensional-blue focus:outline-none focus:ring-4 aria-disabled:cursor-not-allowed aria-disabled:bg-platinum aria-disabled:text-silver-metallic'
                  disabled={isLoading || code.length !== 6}
                  aria-disabled={code.length !== 6}
                >
                  <BeatLoader
                    color='#fff'
                    margin={0}
                    size={10}
                    loading={isLoading}
                  />
                  {!isLoading && t('otp.button')}
                </button>
              </form>
              <p className='mt-6 text-center text-sm font-light text-gray-500'>
                <span className='text-nickel'>{`${t(
                  'login:signup.title'
                )} `}</span>
                <a
                  href='#'
                  className='dark:text-primary-500 font-medium text-ultramarine-blue underline'
                >
                  {t('login:signup.button')}
                </a>
              </p>
            </div>
          </div>
        </div>
        <div className='relative hidden h-screen flex-col justify-end lg:flex lg:w-1/2'>
          <div className='absolute -right-[45px] top-[55px] h-[91px] w-[91px]'>
            <Image src='/svg/polygon.svg' fill alt='Polygon' priority />
          </div>
          <div className='absolute -right-[82px] top-[146px] h-[221px] w-[221px]'>
            <Image src='/svg/polygon.svg' fill alt='Polygon' priority />
          </div>
          <div className='absolute -left-[82px] top-[444px] h-[243px] w-[243px]'>
            <Image src='/svg/polygon.svg' fill alt='Polygon' priority />
          </div>
          <div className='absolute -left-[167px] top-200 h-[413px] w-[413px]'>
            <Image src='/svg/polygon-border.svg' fill alt='Polygon' priority />
          </div>
          <h3 className='mb-3 px-14 text-2xl font-bold text-cultured'>
            {t('login:description:first')}
          </h3>
          <p className='mb-22 px-14 text-base text-cultured'>
            {t('login:description:seconds')}
          </p>
          <div className='relative h-3/6 w-full xl:h-3/5'>
            <Image
              src='/img/bg-login.png'
              className='object-cover 2xl:object-contain'
              fill
              priority
              alt='Background Login'
            />
          </div>
        </div>
      </div>
    </>
  );
};

export const getServerSideProps = async ({ locale, req, res }) => {
  const session = await getServerSession(req, res, AuthOptions);

  if (session && session.user && session.role)
    return {
      redirect: {
        destination: '/vendor/insurances',
        permanent: true,
      },
    };

  return {
    props: {
      ...(await serverSideTranslations(locale, ['login', 'errors', 'common'])),
    },
  };
};

export default AuthLogin;
