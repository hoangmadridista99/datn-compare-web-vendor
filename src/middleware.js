import { withAuth } from 'next-auth/middleware';
import { NextResponse } from 'next/server';

export const config = { matcher: ['/vendor/:path*'] };

export default withAuth(
  function middleware(req) {
    const { token } = req.nextauth;

    if (!token) {
      const url = req.nextUrl.clone();
      url.pathname = '/auth/login';

      NextResponse.redirect(url);

      return;
    }

    return NextResponse.next();
  },
  {
    callbacks: {
      authorized: ({ token }) =>
        token?.role === 'vendor' || token?.role === 'admin',
    },
  }
);
