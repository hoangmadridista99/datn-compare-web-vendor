import { ENDPOINTS } from '@constants';
import getAxios from '@utils/axios';

const axios = getAxios();

export const getObjectives = async signal => {
  const response = await axios.get(ENDPOINTS.INSURANCE_OBJECTIVES, { signal });
  return response.data;
};

export const getCategories = async signal => {
  const response = await axios.get(ENDPOINTS.INSURANCE_CATEGORIES, { signal });
  return response.data;
};

export const getInsurances = async (page, filter, signal) => {
  const response = await axios.get(ENDPOINTS.INSURANCES(), {
    signal,
    params: {
      ...filter,
      page,
      limit: 10,
    },
  });
  return response.data;
};

export const getInsuranceDetail = async (id, signal) => {
  const response = await axios.get(ENDPOINTS.INSURANCES(id), { signal });
  return response.data;
};

export const createInsurance = async (body, signal) => {
  return await axios.post(ENDPOINTS.INSURANCES(), body, { signal });
};

export const updateInsurance = async (id, body, signal) => {
  return await axios.patch(ENDPOINTS.INSURANCES(id), body, { signal });
};

export const removeInsurance = async (id, signal) => {
  return await axios.delete(ENDPOINTS.INSURANCES(id), { signal });
};

export const getHospitals = async (name, signal) => {
  const response = await axios.get(ENDPOINTS.INSURANCE_HOSPITAL, {
    signal,
    params: { name },
  });
  return response.data;
};
