import { ENDPOINTS } from '@constants';
import getAxios from '@utils/axios';
const axios = getAxios();

export const sentOtp = async (body, signal) => {
  return await axios.post(ENDPOINTS.AUTH.SENT_OTP, body, { signal });
};

export const updateUser = async (body, signal) => {
  const response = await axios.patch(ENDPOINTS.AUTH.PROFILE, body, { signal });
  return response.data;
};

export const changePassword = async body => {
  return await axios.post(ENDPOINTS.AUTH.CHANGE_PASSWORD, body);
};
