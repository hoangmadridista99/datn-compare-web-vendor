import { ENDPOINTS } from '@constants';
import getAxios from '@utils/axios';
const axios = getAxios();

export const uploadBanner = async (body, signal) => {
  const response = await axios.post(ENDPOINTS.UPLOAD.BANNER, body, { signal });
  return response.data;
};

export const uploadPdf = async (body, signal) => {
  const response = await axios.post(ENDPOINTS.UPLOAD.PDF, body, { signal });
  return response.data;
};
