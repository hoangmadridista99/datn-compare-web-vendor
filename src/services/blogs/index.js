import { ENDPOINTS } from '@constants';
import getAxios from '@utils/axios';

const axios = getAxios();

export const getCategories = async () => {
  try {
    const response = await axios.get(ENDPOINTS.BLOG_CATEGORIES);
    return response.data;
  } catch (_) {
    return [];
  }
};

export const getComments = async (id, page, signal) => {
  const response = await axios.get(ENDPOINTS.BLOG_COMMENTS(id), {
    signal,
    params: { page, limit: 10 },
  });
  return response.data;
};

export const getBlogs = async (page, filter, signal) => {
  const response = await axios.get(ENDPOINTS.BLOGS_USER, {
    signal,
    params: { ...filter, page, limit: 10 },
  });

  return response.data;
};

export const getBlogDetails = async (blogId, signal) => {
  const response = await axios.get(ENDPOINTS.BLOGS(blogId), { signal });
  return response.data;
};

export const createBlog = async (body, signal) => {
  return await axios.post(ENDPOINTS.BLOGS(), body, { signal });
};

export const updateBlog = async (blogId, body, signal) => {
  return await axios.patch(ENDPOINTS.BLOGS(blogId), body, { signal });
};

export const removeBlog = async (blogId, signal) => {
  return await axios.delete(ENDPOINTS.BLOGS(blogId), { signal });
};
