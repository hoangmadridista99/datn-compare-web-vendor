import React, { useState, useEffect } from 'react';
import Image from 'next/image';
import { useSession } from 'next-auth/react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';
import classNames from 'class-names';
import LogoutIconSvg from '@components/Icons/logout';
import { signOut } from 'next-auth/react';
import { ROUTE, SETTING_TAB_KEY } from '@constants';
import LockIconSvg from '@components/Icons/lock';
import ProfileIconSvg from '@components/Icons/profile';

const Header = () => {
  const [isShowLanguages, setIsShowLanguages] = useState(false);
  const [isShowProfile, setIsShowProfile] = useState(false);
  const [locale, setLocale] = useState('vi');

  const session = useSession();
  const router = useRouter();
  const { t } = useTranslation(['common']);

  useEffect(() => {
    if (window) {
      const language = window.navigator.language;

      if (language.includes('en')) {
        setLocale('en');
      }
    }
  }, []);
  useEffect(() => {
    const handleDisableShowLanguages = () => {
      setIsShowLanguages(false);
      setIsShowProfile(false);
    };

    router.events.on('routeChangeStart', handleDisableShowLanguages);

    setLocale(router.locale ?? 'vi');

    return () =>
      router.events.off('routeChangeStart', handleDisableShowLanguages);
  }, [router]);

  const hanldeToggleLanguages = () => {
    setIsShowLanguages(!isShowLanguages);
    if (isShowProfile) {
      setIsShowProfile(false);
    }
  };
  const handleToggleProfile = () => {
    setIsShowProfile(!isShowProfile);
    if (isShowLanguages) {
      setIsShowLanguages(false);
    }
  };
  const handleClickBackground = () => {
    setIsShowLanguages(false);
    setIsShowProfile(false);
  };
  const handleClickLogout = () => signOut();

  return (
    <>
      <div
        onClick={handleClickBackground}
        className='fixed z-40 h-screen w-screen aria-hidden:-z-1 aria-hidden:hidden'
        aria-hidden={!isShowLanguages && !isShowProfile}
      />
      <header className='flex h-14 items-center justify-end px-6 py-4'>
        <div className='relative'>
          <button
            className={classNames('relative flex items-center', {
              'z-50': isShowProfile || isShowLanguages,
            })}
            type='button'
            onClick={hanldeToggleLanguages}
          >
            <Image src='/svg/world.svg' width={24} height={24} alt='World' />
            <span className='mx-2 text-nickel'>{t(`common:${locale}`)}</span>
            <Image
              src='/svg/arrow-down.svg'
              width={24}
              height={24}
              alt='Arrow Down'
            />
          </button>
          <div
            className='aria-hidden:opacity-200 absolute top-7/5 -z-1 w-full scale-95 rounded-lg bg-white py-3 text-center text-nickel opacity-0 shadow blur-lg transition duration-500 aria-hidden:z-50 aria-hidden:opacity-100 aria-hidden:blur-none'
            aria-hidden={isShowLanguages}
          >
            <Link
              className='duration-400 block w-full px-3 py-1 transition hover:bg-azure hover:text-cultured'
              href={router.asPath}
              locale='vi'
            >
              {t('common:vi')}
            </Link>
            <Link
              className='duration-400 block w-full px-3 py-1 transition hover:bg-azure hover:text-cultured'
              href={router.asPath}
              locale='en'
            >
              {t('common:en')}
            </Link>
          </div>
        </div>
        <div className='mx-4 h-full w-px bg-platinum' />
        <div className='relative overflow-visible'>
          <button
            className={classNames('relative flex items-center', {
              'z-50': isShowProfile || isShowLanguages,
            })}
            type='button'
            onClick={handleToggleProfile}
          >
            <div className='relative h-6 w-6 rounded-full'>
              <Image
                src={
                  session?.data?.user?.avatar_profile_url ??
                  '/svg/avatar-default.svg'
                }
                fill
                className='rounded-full'
                alt='Avatar'
              />
            </div>
            <p className='ml-1 text-sm text-arsenic'>
              {session?.data?.user?.first_name && session?.data?.user?.last_name
                ? `${session?.data?.user?.first_name} ${session?.data?.user?.last_name}`
                : 'Vendor'}
            </p>
            <Image
              src='/svg/arrow-down.svg'
              width={24}
              height={24}
              alt='Arrow Down'
            />
          </button>
          <div
            className='aria-hidden:opacity-200 absolute right-0 top-7/5 -z-1 w-auto scale-95 rounded-lg bg-white p-4 opacity-0 shadow blur-lg transition duration-500 aria-hidden:z-50 aria-hidden:opacity-100 aria-hidden:blur-none'
            aria-hidden={isShowProfile}
          >
            <div className='mb-3 flex items-center border-b border-cultured pb-3 last:mb-0 last:pb-0'>
              <div className='relative mr-2 h-10 w-10 rounded-full'>
                <Image
                  src={
                    session?.data?.user?.avatar_profile_url ??
                    '/svg/avatar-default.svg'
                  }
                  fill
                  className='rounded-full'
                  alt='Avatar'
                />
              </div>
              <div className='flex flex-col justify-between'>
                <p className='whitespace-nowrap text-sm font-medium text-arsenic'>
                  {session?.data?.user?.first_name &&
                  session?.data?.user?.last_name
                    ? `${session?.data?.user?.first_name} ${session?.data?.user?.last_name}`
                    : 'Vendor'}
                </p>
                <p className='text-xs text-nickel'>Vendor</p>
              </div>
            </div>
            <Link
              href={{
                pathname: ROUTE.SETTINGS,
                query: {
                  tab: SETTING_TAB_KEY.PROFILE,
                },
              }}
              className='group inline-block rounded-md px-2 py-1 text-arsenic transition duration-200 hover:bg-azure hover:text-cultured'
            >
              <div className='flex items-center'>
                <ProfileIconSvg className='stroke-arsenic group-hover:stroke-cultured' />
                <span className='ml-2 whitespace-nowrap'>
                  {t('common:sidebar:settings:profile')}
                </span>
              </div>
            </Link>
            <Link
              href={{
                pathname: ROUTE.SETTINGS,
                query: {
                  tab: SETTING_TAB_KEY.PASSWORD,
                },
              }}
              className='group inline-block rounded-md px-2 py-1 text-arsenic transition duration-200 hover:bg-azure hover:text-cultured'
            >
              <div className='flex items-center'>
                <LockIconSvg className='stroke-arsenic group-hover:stroke-cultured' />
                <span className='ml-2 whitespace-nowrap'>
                  {t('common:sidebar:settings:password')}
                </span>
              </div>
            </Link>
            <div className='my-3 border-0 border-b border-solid border-cultured' />
            <button
              className='group flex w-full cursor-pointer items-center rounded-md border-none bg-transparent px-2 py-1 text-arsenic transition duration-200 hover:bg-azure hover:text-cultured'
              type='button'
              onClick={handleClickLogout}
            >
              <LogoutIconSvg className='stroke-arsenic group-hover:stroke-cultured' />
              <span className='ml-2 whitespace-nowrap'>
                {t('common:sidebar:logout')}
              </span>
            </button>
          </div>
        </div>
      </header>
    </>
  );
};

export default Header;
