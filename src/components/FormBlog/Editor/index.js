import React, { useState } from 'react';
import { Editor } from '@tinymce/tinymce-react';

import { handleError } from '@helpers/handleError';
import { uploadBanner } from '@services/upload';
import LoadingEditor from '../Loading';
import { useTranslation } from 'next-i18next';

const EditorComponent = ({ isLoading, onChange, value }) => {
  const [isLoaded, setIsLoaded] = useState(false);

  const { t } = useTranslation(['blogs']);

  const handleUploadImage = blobInfo =>
    new Promise(resolve => {
      const body = new FormData();
      body.append('file', blobInfo.blob(), blobInfo.filename());

      const reader = new FileReader();

      uploadBanner(body)
        .then(data => resolve(data.image_url))
        .catch(error => handleError(error));

      reader.readAsDataURL(blobInfo.blob());
    });
  const handleLoadContent = () => setIsLoaded(true);

  if (isLoading) return <LoadingEditor />;

  return (
    <>
      {!isLoaded && <LoadingEditor />}
      <Editor
        apiKey='5fbchhduklga0b7i65mfqrdhtatmm6qrgwt3rvbybo0rt3vz'
        onEditorChange={value => onChange(encodeURI(value))}
        init={{
          height: 500,
          menubar: false,
          plugins: ['fullscreen', 'link', 'image'],
          toolbar:
            'fullscreen | undo redo | blocks | fontsize | bold italic underline backcolor | link image | alignleft aligncenter alignright alignjustify ',
          content_style: 'body { font-size:14px }',
          file_picker_types: 'image',
          automatic_uploads: true,
          images_upload_handler: handleUploadImage,
          placeholder: t('blogs:filter:placeholder:content'),
        }}
        onLoadContent={handleLoadContent}
        value={value}
      />
    </>
  );
};

export default EditorComponent;
