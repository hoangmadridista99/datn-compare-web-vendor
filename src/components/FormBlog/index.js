import React, { useState, useEffect, useRef, useCallback } from 'react';
import Image from 'next/image';
import { notification, Spin, Input, Form, Select } from 'antd';
import { useTranslation } from 'next-i18next';

import Editor from './Editor';
import { uploadBanner } from '@services/upload';
import { handleError } from '@helpers/handleError';

const { TextArea } = Input;

const FormBlog = props => {
  const { blogCategories, blogDetails, form, onFinishForm, isFormUpdate } =
    props;

  const [file, setFile] = useState();
  const [content, setContent] = useState();
  const [banner, setBanner] = useState();

  const inputRef = useRef(null);
  const { t } = useTranslation(['blogs', 'errors']);

  const handleUploadBanner = useCallback(async (data, signal) => {
    const body = new FormData();
    body.append('file', data);

    const result = await uploadBanner(body, signal);

    setBanner(result.image_url);
  }, []);

  useEffect(() => {
    if (blogDetails) {
      setContent(blogDetails?.content);
      setBanner(blogDetails?.banner_image_url);
    }
  }, [blogDetails]);
  useEffect(() => {
    const controller = new AbortController();

    if (file) {
      handleUploadBanner(file, controller.signal)
        .catch(error => handleError(t, error))
        .finally(() => setFile(undefined));
    }

    return () => controller.abort();
  }, [file, handleUploadBanner, t]);

  const handleSubmitForm = formData => {
    if (!blogDetails) {
      if (!banner) {
        notification.warning({ message: t('blogs:message:banner') });

        return;
      }

      if (!content) {
        notification.warning({ message: t('blogs:message:content') });

        return;
      }
    }

    const body = { ...formData, content, banner_image_url: banner };
    onFinishForm(body);
  };

  const hanldeClickUpload = () => {
    if (inputRef && inputRef.current) {
      inputRef.current.click();
    }
  };

  const handleChangeUpload = e => {
    const value = e.target.files[0];

    setFile(value);
  };
  const handleChangeContent = value => setContent(value);

  const handleClickRemoveBanner = () => setBanner(undefined);

  return (
    <>
      <Form
        className='w-full flex-col justify-between rounded-2xl bg-white p-6 shadow-blog'
        form={form}
        onFinish={handleSubmitForm}
      >
        <div className='mb-6 last:mb-0'>
          <label
            className='mb-2 block text-sm font-bold text-gray-700'
            htmlFor='banner_image_url'
          >
            {t('blogs:form:banner')}
          </label>
          {!banner && (
            <>
              <input
                className='hidden'
                id='banner_image_url'
                type='file'
                ref={inputRef}
                onChange={handleChangeUpload}
                accept='image/gif,image/jpeg,image/jpg,image/png,image/svg'
                style={{ display: 'none' }}
              />
              <div className='flex h-9.25 w-full flex-col items-center justify-center rounded-lg border border-dashed shadow-form'>
                <div className='mb-4 flex items-center justify-center'>
                  <Image
                    src='/svg/image-default.svg'
                    width={48}
                    height={48}
                    alt='Form Blog Image Default'
                  />
                  <div className='ml-4 flex h-full flex-col justify-between'>
                    <p className='text-sm font-normal text-arsenic'>
                      {t('blogs:form:file1')}
                    </p>
                    <span className='text-xs font-normal text-nickel'>
                      {t('blogs:form:file2')}
                    </span>
                  </div>
                </div>
                <button
                  id='banner'
                  className='rounded-md border border-ultramarine-blue px-6 py-1 text-sm font-bold text-ultramarine-blue'
                  onClick={hanldeClickUpload}
                  disabled={!!file}
                  type='button'
                >
                  {file ? <Spin /> : t('blogs:button:upload')}
                </button>
              </div>
            </>
          )}
          {banner && (
            <div className='relative h-200 w-full'>
              <Image src={banner} fill alt='Banner' className='object-cover' />
              <button
                className='absolute right-4 top-4 z-10 flex h-8 w-8 items-center justify-center rounded-full bg-black-0.5'
                onClick={handleClickRemoveBanner}
              >
                <Image
                  src='/svg/close-blog.svg'
                  width={10}
                  height={10}
                  alt='Remove'
                />
              </button>
            </div>
          )}
        </div>
        <div className='mb-6 last:mb-0'>
          <label
            className='mb-4 block text-base font-bold text-arsenic'
            htmlFor='blog_category_id'
          >
            {t('blogs:form:category')}
          </label>
          <Form.Item
            name='blog_category_id'
            rules={[{ required: true, message: t('blogs:message:category') }]}
          >
            <Select
              size='large'
              options={blogCategories}
              placeholder={t('blogs:filter:options:choose')}
              loading={isFormUpdate && !blogDetails}
              disabled={isFormUpdate && !blogDetails}
            />
          </Form.Item>
        </div>
        <div className='mb-6 last:mb-0'>
          <label
            className='mb-4 block text-base font-bold text-arsenic'
            htmlFor='title'
          >
            {t('blogs:form:title')}
          </label>
          <Form.Item
            name='title'
            rules={[{ required: true, message: t('blogs:message:title') }]}
          >
            <Input
              className='w-full rounded-md border border-chinese-silver p-3 placeholder:text-chinese-silver focus:outline-none'
              id='title'
              type='text'
              placeholder={t('blogs:filter:options:title')}
              disabled={isFormUpdate && !blogDetails}
            />
          </Form.Item>
        </div>
        <div className='mb-6 last:mb-0'>
          <label
            className='mb-4 block text-base font-bold text-arsenic'
            htmlFor='description'
          >
            {t('blogs:form:description')}
          </label>
          <Form.Item
            className='mb-6'
            name='description'
            rules={[
              { required: true, message: t('blogs:message:description') },
            ]}
          >
            <TextArea
              className='w-full rounded-md border border-chinese-silver p-3 placeholder:text-chinese-silver focus:outline-none'
              id='description'
              type='text'
              placeholder={t('blogs:filter:options:description')}
              disabled={isFormUpdate && !blogDetails}
            />
          </Form.Item>
        </div>
        <div className='mb-6 last:mb-0'>
          <label
            className='mb-4 block text-base font-bold text-arsenic'
            htmlFor='content'
          >
            {t('blogs:form:content')}
          </label>
          <Editor
            value={content ? decodeURI(content) : undefined}
            onChange={handleChangeContent}
            isLoading={isFormUpdate && !blogDetails}
          />
        </div>
      </Form>
    </>
  );
};

export default FormBlog;
