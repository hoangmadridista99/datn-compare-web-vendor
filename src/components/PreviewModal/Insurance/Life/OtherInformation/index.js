import React from 'react';
import PreviewModalTermInsurance from '../../Terms';
import { useTranslation } from 'next-i18next';
import { FIELDS_LIFE_INSURANCE } from '@constants';
import Image from 'next/image';
import { convertUrl } from '@helpers/convertUrl';
import Link from 'next/link';

const OtherInformation = ({ insuranceDetails, getIconUrl }) => {
  const { t } = useTranslation(['insurances']);

  const renderTextValue = (
    fromValue = null,
    toValue = null,
    determinedValue = null
  ) => {
    if (!!fromValue && !!toValue) {
      return (
        <span>{`${t('insurances:field:from')} ${fromValue} ${t(
          'insurances:field:to'
        )} ${toValue}`}</span>
      );
    }

    if (fromValue) {
      return <span>{`${t('insurances:field:from')} ${fromValue}`}</span>;
    }

    if (toValue) {
      return <span>{`${t('insurances:form:to')} ${toValue}`}</span>;
    }

    if (determinedValue) {
      return (
        <span>{`${t('insurances:field:within')} ${determinedValue}`}</span>
      );
    }
  };

  const renderUnitValue = unit => {
    return t(`insurances:field:${unit}`);
  };

  const renderField = (
    fromValue,
    toValue,
    determinedValue,
    type,
    description,
    isFormatType = true
  ) => {
    if (!fromValue && !toValue && !determinedValue && !description)
      return t('insurances:field:not_support');

    return (
      <>
        {(determinedValue || fromValue || toValue) && (
          <>
            {determinedValue && (
              <p>
                {renderTextValue(null, null, determinedValue)}
                <span>&nbsp;</span>
                {renderUnitValue(type)}
              </p>
            )}

            <p>
              {renderTextValue(fromValue, toValue)}
              <span>&nbsp;</span>
              {isFormatType ? renderUnitValue(type) : <>{type}</>}
            </p>
          </>
        )}

        {description && <p>{description}</p>}
      </>
    );
  };

  const benefitsIllustrationTable =
    insuranceDetails?.benefits_illustration_table
      ? convertUrl(insuranceDetails.benefits_illustration_table)
      : null;
  const documentationUrl = insuranceDetails?.documentation_url
    ? convertUrl(insuranceDetails.documentation_url)
    : null;

  return (
    <div className='no-scrollbar h-[47vh] overflow-hidden overflow-y-scroll'>
      {/* Terms */}
      <PreviewModalTermInsurance
        insuranceDetails={insuranceDetails}
        getIconUrl={getIconUrl}
        renderField={renderField}
      />

      {/* Additional Benefit */}
      <div className='mb-6 font-normal text-nickel'>
        <p className='mb-4 text-xl font-medium text-arsenic'>
          {t('insurances:form:additional_benefits:title')}
        </p>
        {FIELDS_LIFE_INSURANCE.additional_benefits.map(key => (
          <div key={key} className='mb-6 flex justify-between'>
            <p className='w-2/5 pr-2'>
              {t(`insurances:form:additional_benefits:${key}`)}
            </p>

            <div className='flex flex-1'>
              <div className='mt-1'>
                {getIconUrl(insuranceDetails?.additional_benefits[key].level)}
              </div>

              <p className='ml-4 flex-1'>
                {insuranceDetails?.additional_benefits[key].text ??
                  t('insurances:field:not_support')}
              </p>
            </div>
          </div>
        ))}
      </div>

      {/* Customer Orientation */}
      <div className='mb-6 font-normal text-nickel'>
        <p className='mb-4 text-xl font-medium text-arsenic'>
          {t('insurances:form:additional_benefits:title')}
        </p>
        {FIELDS_LIFE_INSURANCE.customer_orientation.map(key => (
          <div key={key} className='mb-6 flex justify-between'>
            <p className='w-2/5 pr-2'>
              {t(`insurances:form:customer_orientation:${key}`)}
            </p>

            <div className='flex flex-1'>
              <div className='mt-1'>
                {getIconUrl(insuranceDetails?.customer_orientation[key].level)}
              </div>

              <p className='ml-4 flex-1'>
                {insuranceDetails?.customer_orientation[key].text ??
                  t('insurances:field:not_support')}
              </p>
            </div>
          </div>
        ))}
      </div>

      {/* Document */}
      {(benefitsIllustrationTable || documentationUrl) && (
        <div className='mb-6 font-normal text-nickel'>
          <p className='mb-4 text-xl font-medium text-arsenic'>
            {t('insurances:form:document:title')}
          </p>
          {benefitsIllustrationTable && (
            <Link
              href={benefitsIllustrationTable.link}
              className='mb-4 flex items-center last:mb-0'
              target='_blank'
            >
              <Image
                src='/svg/document.svg'
                width={24}
                height={24}
                alt='Document'
              />
              <p className='ml-2'>
                {decodeURI(benefitsIllustrationTable.name)}
              </p>
            </Link>
          )}
          {documentationUrl && (
            <Link
              href={documentationUrl.link}
              className='mb-4 flex items-center last:mb-0'
              target='_blank'
            >
              <Image
                src='/svg/document.svg'
                width={24}
                height={24}
                alt='Document'
              />
              <p className='ml-2'>{decodeURI(documentationUrl.name)}</p>
            </Link>
          )}
        </div>
      )}
    </div>
  );
};

export default OtherInformation;
