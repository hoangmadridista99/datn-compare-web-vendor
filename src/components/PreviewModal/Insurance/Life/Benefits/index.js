import React from 'react';
import { useTranslation } from 'next-i18next';
import { Skeleton } from 'antd';

const Benefits = ({ insuranceDetails }) => {
  const { t } = useTranslation(['insurances']);

  return (
    <div className='no-scrollbar h-[47vh] overflow-hidden overflow-y-scroll'>
      <div className='mb-6 text-nickel'>
        <p className='mb-4 text-xl font-medium text-arsenic'>{`${t(
          'insurances:field:information'
        )} ${
          insuranceDetails?.name
            ? `${t('insurances:field:about')} ${insuranceDetails?.name}`
            : ''
        }`}</p>
        <Skeleton active paragraph={{ rows: 2 }} loading={!insuranceDetails}>
          <p className='text-base font-normal'>
            {insuranceDetails?.description_insurance}
          </p>
        </Skeleton>
      </div>
      <div className='mb-6 font-normal'>
        <p className='mb-4 text-xl font-medium text-arsenic'>
          {t('insurances:form:details:title')}
        </p>

        <Skeleton active paragraph={{ rows: 2 }} loading={!insuranceDetails}>
          <div className='mb-3 flex items-center text-nickel'>
            <p className='w-2/5'>{t('insurances:form:details:company_id')}</p>
            <p className='flex-1'>{insuranceDetails?.company.long_name}</p>
          </div>

          <div className='mb-3 flex items-center text-nickel'>
            <p className='w-2/5'>
              {t('insurances:form:details:insurance_type')}
            </p>
            <p className='flex-1'>
              {t(
                `insurances:form:options:${insuranceDetails?.insurance_category?.label}`
              )}
            </p>
          </div>

          <div className='mb-3 flex items-center text-nickel'>
            <p className='w-2/5'>{t('insurances:field:insurance_name')}</p>
            <p className='flex-1'>{insuranceDetails?.name}</p>
          </div>
        </Skeleton>
      </div>

      {/* key_benefits */}
      <div className='mb-6 font-normal text-nickel'>
        <p className='mb-4 text-xl font-medium text-arsenic'>
          {t('insurances:form:key_benefits')}
        </p>

        <p className='mb-6'>{insuranceDetails?.key_benefits}</p>
      </div>
    </div>
  );
};

export default Benefits;
