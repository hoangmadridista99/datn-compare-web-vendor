/* eslint-disable indent */
import React from 'react';
import { Card, Tabs } from 'antd';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';
import Benefits from './Benefits';
import OtherInformation from './OtherInformation';

const ModalBodyLifeInsurance = ({ insuranceDetails }) => {
  const { t } = useTranslation(['insurances']);

  const getIconUrl = level => {
    switch (level) {
      case 'high':
        return (
          <Image
            width={18}
            height={18}
            src='/svg/check-success.svg'
            alt='Check Success'
          />
        );
      case 'medium':
        return (
          <Image
            width={18}
            height={18}
            src='/svg/check-warning.svg'
            alt='Check Warning'
          />
        );
      case 'not-support':
        return (
          <Image width={18} height={18} src='/svg/close.svg' alt='close' />
        );
      case 'none':
        return (
          <Image width={18} height={18} src='/svg/warning.svg' alt='warning' />
        );
      default:
        return null;
    }
  };

  const tabList = [
    {
      key: 'benefits',
      label: t('insurances:form:key_benefits'),
      children: (
        <Benefits insuranceDetails={insuranceDetails} getIconUrl={getIconUrl} />
      ),
    },
    {
      key: 'other',
      label: t('insurances:field:other_information'),
      disabled: !insuranceDetails,
      children: (
        <OtherInformation
          insuranceDetails={insuranceDetails}
          getIconUrl={getIconUrl}
        />
      ),
    },
  ];

  return (
    <div className='h-[60vh]'>
      <Card bordered={false} className='border-none shadow-none'>
        <Tabs
          centered
          defaultActiveKey='benefits'
          items={tabList}
          className='text-base text-spanish-gray'
        />
      </Card>
    </div>
  );
};

export default ModalBodyLifeInsurance;
