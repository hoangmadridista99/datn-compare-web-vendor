import { formatCurrency } from '@helpers/formatCurrency';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { FIELDS_HEALTH_INSURANCE } from '@constants';

const AdditionalBenefit = ({ insuranceDetails, getIconUrl }) => {
  const { t } = useTranslation(['insurances']);

  const renderField = (determinedValue, description) => {
    return (
      <>
        {determinedValue && (
          <p>
            {formatCurrency(determinedValue)}
            <span>&nbsp;</span>
            VNĐ
          </p>
        )}

        {description && <p>{description}</p>}
      </>
    );
  };

  return (
    <div className='no-scrollbar h-[47vh] overflow-hidden overflow-y-scroll'>
      {!insuranceDetails?.dental && !insuranceDetails?.obstetric && (
        <div className='flex justify-center font-bold'>
          {t('insurances:field:not_support')}
        </div>
      )}

      {/* dental */}
      {insuranceDetails?.dental && (
        <div className='mb-6 font-normal text-nickel'>
          <p className='mb-4 text-xl font-medium text-arsenic'>
            {t('insurances:form:dental:title')}
          </p>
          {FIELDS_HEALTH_INSURANCE.dental.map(key => (
            <div key={key} className='mb-6 flex justify-between'>
              <p className='w-2/5'>{t(`insurances:form:dental:${key}`)}</p>

              <div className='flex flex-1'>
                <div className='mt-1'>
                  {getIconUrl(insuranceDetails?.dental[key]?.level)}
                </div>

                <div className='ml-4 flex-1'>
                  {renderField(
                    insuranceDetails?.dental[key]?.value,
                    insuranceDetails?.dental[key]?.description
                  )}
                </div>
              </div>
            </div>
          ))}
        </div>
      )}

      {/* obstetric */}
      {insuranceDetails?.obstetric && (
        <div className='mb-6 font-normal text-nickel'>
          <p className='mb-4 text-xl font-medium text-arsenic'>
            {t('insurances:form:obstetric:title')}
          </p>
          {FIELDS_HEALTH_INSURANCE.obstetric.map(key => (
            <div key={key} className='mb-6 flex justify-between'>
              <p className='w-2/5'>{t(`insurances:form:obstetric:${key}`)}</p>

              <div className='flex flex-1'>
                <div className='mt-1'>
                  {getIconUrl(insuranceDetails?.obstetric[key]?.level)}
                </div>

                <div className='ml-4 flex-1'>
                  {renderField(
                    insuranceDetails?.obstetric[key]?.value,
                    insuranceDetails?.obstetric[key]?.description
                  )}
                </div>
              </div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default AdditionalBenefit;
