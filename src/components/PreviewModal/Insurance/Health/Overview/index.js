/* eslint-disable indent */
import React from 'react';
import { useTranslation } from 'next-i18next';
import { Skeleton } from 'antd';
import PreviewModalTermInsurance from '../../Terms';

const Overview = ({ insuranceDetails, getIconUrl }) => {
  const { t } = useTranslation(['insurances']);

  const renderTextValue = (
    fromValue = null,
    toValue = null,
    determinedValue = null
  ) => {
    if (!!fromValue && !!toValue) {
      return (
        <span>{`${t('insurances:field:from')} ${fromValue} ${t(
          'insurances:field:to'
        )} ${toValue}`}</span>
      );
    }

    if (fromValue) {
      return <span>{`${t('insurances:form:from')} ${fromValue}`}</span>;
    }

    if (toValue) {
      return <span>{`${t('insurances:form:to')} ${toValue}`}</span>;
    }

    if (determinedValue) {
      return (
        <span>{`${t('insurances:field:within')} ${determinedValue}`}</span>
      );
    }
  };

  const renderUnitValue = unit => {
    return t(`insurances:field:${unit}`);
  };

  const renderField = (
    fromValue,
    toValue,
    determinedValue,
    type,
    description,
    isFormatType = true
  ) => {
    if (!fromValue && !toValue && !determinedValue && !description)
      return t('insurances:field:not_support');

    return (
      <>
        {(determinedValue || fromValue || toValue) && (
          <>
            {determinedValue && (
              <p>
                {renderTextValue(null, null, determinedValue)}
                <span>&nbsp;</span>
                {renderUnitValue(type)}
              </p>
            )}

            <p>
              {renderTextValue(fromValue, toValue)}
              <span>&nbsp;</span>
              {isFormatType ? renderUnitValue(type) : <>{type}</>}
            </p>
          </>
        )}

        {description && <p>{description}</p>}
      </>
    );
  };

  return (
    <div className='no-scrollbar h-[47vh] overflow-hidden overflow-y-scroll'>
      <div className='mb-6 text-nickel'>
        <p className='mb-4 text-xl font-medium text-arsenic'>{`${t(
          'insurances:field:information'
        )} ${
          insuranceDetails?.name
            ? `${t('insurances:field:about')} ${insuranceDetails?.name}`
            : ''
        }`}</p>
        <Skeleton active paragraph={{ rows: 2 }} loading={!insuranceDetails}>
          <p className='text-base font-normal'>
            {insuranceDetails?.description_insurance}
          </p>
        </Skeleton>
      </div>
      <div className='mb-6 font-normal'>
        <p className='mb-4 text-xl font-medium text-arsenic'>
          {t('insurances:form:details:title')}
        </p>

        <Skeleton active paragraph={{ rows: 2 }} loading={!insuranceDetails}>
          <div className='mb-3 flex items-center text-nickel'>
            <p className='w-2/5'>{t('insurances:form:details:company_id')}</p>
            <p className='flex-1'>{insuranceDetails?.company.long_name}</p>
          </div>

          <div className='mb-3 flex items-center text-nickel'>
            <p className='w-2/5'>
              {t('insurances:form:details:insurance_type')}
            </p>
            <p className='flex-1'>
              {t(
                `insurances:form:options:${insuranceDetails?.insurance_category?.label}`
              )}
            </p>
          </div>

          <div className='mb-3 flex items-center text-nickel'>
            <p className='w-2/5'>{t('insurances:field:insurance_name')}</p>
            <p className='flex-1'>{insuranceDetails?.name}</p>
          </div>
        </Skeleton>
      </div>

      {/* Terms */}
      <PreviewModalTermInsurance
        insuranceDetails={insuranceDetails}
        getIconUrl={getIconUrl}
        renderField={renderField}
      />

      {/* health_customer_orientation */}
      <div className='mb-6 font-normal text-nickel'>
        <p className='mb-4 text-xl font-medium text-arsenic'>
          {t('insurances:form:customer_orientation:title')}
        </p>

        {/* insurance_scope */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>
            {t('insurances:form:customer_orientation:insurance_scope')}
          </p>

          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(
                insuranceDetails?.customer_orientation.insurance_scope?.level
              )}
            </div>

            <div className='ml-4 flex-1'>
              {insuranceDetails?.customer_orientation.insurance_scope
                ?.values ? (
                <p>
                  {insuranceDetails?.customer_orientation.insurance_scope.values
                    .map(item => t(`insurances:form:options:${item}`))
                    .join(', ')}
                </p>
              ) : (
                t('insurances:field:not_support')
              )}
            </div>
          </div>
        </div>

        {/* waiting_period */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>
            {t('insurances:form:customer_orientation:waiting_period')}
          </p>

          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(
                insuranceDetails?.customer_orientation?.waiting_period?.level
              )}
            </div>

            <div className='ml-4 flex-1'>
              {!insuranceDetails?.customer_orientation?.waiting_period?.from &&
              !insuranceDetails?.customer_orientation?.waiting_period?.to &&
              !insuranceDetails?.customer_orientation?.waiting_period
                ?.description
                ? t('insurances:field:not_support')
                : renderField(
                    insuranceDetails?.customer_orientation?.waiting_period
                      ?.from,
                    insuranceDetails?.customer_orientation?.waiting_period?.to,
                    null,
                    'day',
                    insuranceDetails?.customer_orientation?.waiting_period
                      ?.description,
                    true
                  )}
            </div>
          </div>
        </div>

        {/* compensation_process */}
        <div className='mb-6 flex justify-between last:mb-0'>
          <p className='w-2/5'>
            {t('insurances:form:customer_orientation:compensation_process')}
          </p>

          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(
                insuranceDetails?.customer_orientation?.compensation_process
                  ?.level
              )}
            </div>

            <div className='ml-4 flex-1'>
              {insuranceDetails?.customer_orientation?.compensation_process
                ?.description ? (
                <p>
                  {
                    insuranceDetails?.customer_orientation.compensation_process
                      .description
                  }
                </p>
              ) : (
                t('insurances:field:not_support')
              )}
            </div>
          </div>
        </div>

        {/* reception_and_processing_time */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>
            {t(
              'insurances:form:customer_orientation:reception_and_processing_time'
            )}
          </p>

          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(
                insuranceDetails?.customer_orientation
                  ?.reception_and_processing_time?.level
              )}
            </div>

            <div className='ml-4 flex-1'>
              {insuranceDetails?.customer_orientation
                ?.reception_and_processing_time?.text ? (
                <p>
                  {
                    insuranceDetails?.customer_orientation
                      ?.reception_and_processing_time?.text
                  }
                </p>
              ) : (
                t('insurances:field:not_support')
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Overview;
