import { Table } from 'antd';
import React from 'react';
import { useTranslation } from 'next-i18next';

const HospitalLinked = ({ insuranceDetails }) => {
  const { t } = useTranslation(['insurances']);

  const columns = [
    {
      title: t('insurances:form:linkedList:hospital_name'),
      dataIndex: 'name',
    },
    {
      title: t('insurances:form:linkedList:hospital_address'),
      dataIndex: 'address',
    },
    {
      title: t('insurances:form:linkedList:hospital_province'),
      dataIndex: 'province',
    },
    {
      title: t('insurances:form:linkedList:hospital_district'),
      dataIndex: 'district',
    },
  ];
  const dataSource = [
    ...(insuranceDetails?.hospitals ?? []),
    ...(insuranceDetails?.hospitals_pending ?? []),
  ];
  return (
    <div className='no-scrollbar h-[47vh] overflow-hidden overflow-y-scroll'>
      <div className='mb-6 font-normal text-nickel'>
        <Table
          rowKey={'id'}
          size='middle'
          bordered
          dataSource={dataSource}
          columns={columns}
          pagination={{
            pageSize: 5,
            hideOnSinglePage: true,
          }}
        />
      </div>
    </div>
  );
};

export default HospitalLinked;
