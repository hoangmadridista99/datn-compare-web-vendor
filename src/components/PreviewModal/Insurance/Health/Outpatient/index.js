import { formatCurrency } from '@helpers/formatCurrency';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { FIELDS_HEALTH_INSURANCE } from '@constants';

const Outpatient = ({ insuranceDetails, getIconUrl }) => {
  const { t } = useTranslation(['insurances']);

  const renderField = (determinedValue, description) => {
    if (!determinedValue && !description)
      return t('insurances:field:not_support');

    return (
      <>
        {determinedValue && (
          <p>
            {formatCurrency(determinedValue)}
            <span>&nbsp;</span>
            VNĐ
          </p>
        )}

        {description && <p>{description}</p>}
      </>
    );
  };

  return (
    <div className='no-scrollbar h-[47vh] overflow-hidden overflow-y-scroll'>
      <div className='mb-6 font-normal text-nickel'>
        {FIELDS_HEALTH_INSURANCE.outpatient.map(key => (
          <div key={key} className='mb-6 flex justify-between'>
            <p className='w-2/5 pr-4'>
              {t(`insurances:form:outpatient:${key}`)}
            </p>

            <div className='flex flex-1'>
              <div className='mt-1'>
                {getIconUrl(insuranceDetails?.outpatient[key].level)}
              </div>

              <div className='ml-4'>
                {renderField(
                  insuranceDetails?.outpatient[key].value,
                  insuranceDetails?.outpatient[key]?.description
                )}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Outpatient;
