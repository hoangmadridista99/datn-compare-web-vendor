/* eslint-disable indent */
import React from 'react';
import { Card, Tabs } from 'antd';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';
import Overview from './Overview';
import Inpatient from './Inpatient';
import Outpatient from './Outpatient';
import AdditionalBenefit from './AdditionalBenefit';
import HospitalLinked from './HospitalsLinked';

const ModalBodyHealthInsurance = ({ insuranceDetails }) => {
  const { t } = useTranslation(['insurances']);

  const getIconUrl = level => {
    switch (level) {
      case 'high':
        return (
          <Image
            width={18}
            height={18}
            src='/svg/check-success.svg'
            alt='Check Success'
          />
        );
      case 'medium':
        return (
          <Image
            width={18}
            height={18}
            src='/svg/check-warning.svg'
            alt='Check Warning'
          />
        );
      case 'not-support':
        return (
          <Image width={18} height={18} src='/svg/close.svg' alt='close' />
        );
      case 'none':
        return (
          <Image width={18} height={18} src='/svg/warning.svg' alt='warning' />
        );
      default:
        return null;
    }
  };

  const tabList = [
    {
      key: 1,
      label: t('insurances:form:overview'),
      children: (
        <Overview insuranceDetails={insuranceDetails} getIconUrl={getIconUrl} />
      ),
    },
    {
      key: 2,
      label: t('insurances:form:inpatient:title'),
      disabled: !insuranceDetails,
      children: (
        <Inpatient
          insuranceDetails={insuranceDetails}
          getIconUrl={getIconUrl}
        />
      ),
    },
    {
      key: 3,
      label: t('insurances:form:outpatient:title'),
      disabled: !insuranceDetails,
      children: (
        <Outpatient
          insuranceDetails={insuranceDetails}
          getIconUrl={getIconUrl}
        />
      ),
    },
    {
      key: 4,
      label: t('insurances:form:additional_benefits:title'),
      disabled: !insuranceDetails,
      children: (
        <AdditionalBenefit
          insuranceDetails={insuranceDetails}
          getIconUrl={getIconUrl}
        />
      ),
    },
    {
      key: 5,
      label: t('insurances:form:linkedList:title'),
      disabled: !insuranceDetails,
      children: <HospitalLinked insuranceDetails={insuranceDetails} />,
    },
  ];

  return (
    <div className='h-[60vh]'>
      <Card bordered={false} className='border-none shadow-none'>
        <Tabs
          centered
          defaultActiveKey={1}
          items={tabList}
          className='text-base text-spanish-gray'
        />
      </Card>
    </div>
  );
};

export default ModalBodyHealthInsurance;
