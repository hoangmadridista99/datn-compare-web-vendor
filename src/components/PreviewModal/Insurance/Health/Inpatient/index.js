import { formatCurrency } from '@helpers/formatCurrency';
import React from 'react';
import { useTranslation } from 'next-i18next';

const Inpatient = ({ insuranceDetails, getIconUrl }) => {
  const { t } = useTranslation(['insurances']);

  const renderUnitValue = unit => (
    <>VNĐ/&nbsp;{t(`insurances:field:${unit}`)}</>
  );

  const renderField = (determinedValue, description, type = null) => {
    if (!determinedValue && !description)
      return t('insurances:field:not_support');

    return (
      <>
        {determinedValue && (
          <p>
            {formatCurrency(determinedValue)}
            <span>&nbsp;</span>
            {type ? renderUnitValue(type) : 'VNĐ'}
          </p>
        )}

        {description && <p>{description}</p>}
      </>
    );
  };

  return (
    <div className='no-scrollbar h-[47vh] overflow-hidden overflow-y-scroll'>
      <div className='mb-6 font-normal text-nickel'>
        {/* room_type */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>{t('insurances:form:inpatient:room_type')}</p>

          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(insuranceDetails?.inpatient.room_type?.level)}
            </div>

            <div className='ml-4 flex-1'>
              {insuranceDetails?.inpatient.room_type.values ? (
                <p>
                  {insuranceDetails?.inpatient.room_type.values
                    .map(item => t(`insurances:form:options:${item}`))
                    .join(', ')}
                </p>
              ) : (
                t('insurances:field:not_support')
              )}

              {insuranceDetails?.inpatient.room_type.description && (
                <p>{insuranceDetails?.inpatient.room_type.description}</p>
              )}
            </div>
          </div>
        </div>

        {/* for_cancer */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>{t('insurances:form:inpatient:for_cancer')}</p>

          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(insuranceDetails?.inpatient.for_cancer.level)}
            </div>

            <div className='ml-4 flex-1'>
              {renderField(
                insuranceDetails?.inpatient?.for_cancer?.value,
                insuranceDetails?.inpatient?.for_cancer?.description
              )}
            </div>
          </div>
        </div>

        {/* for_illnesses */}
        <div className='mb-6 flex justify-between last:mb-0'>
          <p className='w-2/5'>
            {t('insurances:form:inpatient:for_illnesses')}
          </p>

          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(insuranceDetails?.inpatient.for_illnesses.level)}
            </div>

            <div className='ml-4 flex-1'>
              {renderField(
                insuranceDetails?.inpatient?.for_illnesses?.value,
                insuranceDetails?.inpatient?.for_illnesses?.description
              )}
            </div>
          </div>
        </div>

        {/* for_accidents */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>
            {t('insurances:form:inpatient:for_accidents')}
          </p>

          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(insuranceDetails?.inpatient.for_accidents.level)}
            </div>

            <div className='ml-4 flex-1'>
              {renderField(
                insuranceDetails?.inpatient?.for_accidents?.value,
                insuranceDetails?.inpatient?.for_accidents?.description
              )}
            </div>
          </div>
        </div>

        {/* for_surgical */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>{t('insurances:form:inpatient:for_surgical')}</p>
          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(insuranceDetails?.inpatient.for_surgical.level)}
            </div>

            <div className='ml-4 flex-1'>
              {renderField(
                insuranceDetails?.inpatient?.for_surgical?.value,
                insuranceDetails?.inpatient?.for_surgical?.description
              )}
            </div>
          </div>
        </div>

        {/* for_hospitalization */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5 '>
            {t('insurances:form:inpatient:for_hospitalization')}
          </p>

          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(
                insuranceDetails?.inpatient.for_hospitalization.level
              )}
            </div>

            <div className='ml-4 flex'>
              {renderField(
                insuranceDetails?.inpatient?.for_hospitalization?.value,
                insuranceDetails?.inpatient?.for_hospitalization?.description,
                insuranceDetails?.inpatient?.for_hospitalization?.type ===
                  'by-day'
                  ? 'day'
                  : 'year'
              )}
            </div>
          </div>
        </div>

        {/* for_intensive_care */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5 '>
            {t('insurances:form:inpatient:for_intensive_care')}
          </p>
          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(
                insuranceDetails?.inpatient?.for_intensive_care?.level
              )}
            </div>

            <div className='ml-4 flex-1'>
              {renderField(
                insuranceDetails?.inpatient?.for_intensive_care?.value,
                insuranceDetails?.inpatient?.for_intensive_care?.description
              )}
            </div>
          </div>
        </div>

        {/* for_administrative */}
        {insuranceDetails?.inpatient?.for_administrative && (
          <div className='mb-6 flex justify-between'>
            <p className='w-2/5'>
              {t('insurances:form:inpatient:for_administrative')}
            </p>
            <div className='flex flex-1'>
              <div className='mt-1'>
                {getIconUrl(
                  insuranceDetails?.inpatient.for_administrative?.level
                )}
              </div>

              <div className='ml-4 flex-1'>
                {renderField(
                  insuranceDetails?.inpatient?.for_administrative?.value,
                  insuranceDetails?.inpatient?.for_administrative?.description
                )}
              </div>
            </div>
          </div>
        )}

        {/* for_organ_transplant */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>
            {t('insurances:form:inpatient:for_organ_transplant')}
          </p>
          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(
                insuranceDetails?.inpatient.for_organ_transplant.level
              )}
            </div>

            <div className='ml-4 flex-1'>
              {renderField(
                insuranceDetails?.inpatient?.for_organ_transplant?.value,
                insuranceDetails?.inpatient?.for_organ_transplant?.description
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Inpatient;
