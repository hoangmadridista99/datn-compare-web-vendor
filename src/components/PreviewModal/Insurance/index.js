/* eslint-disable indent */
import React, { useState, useEffect, useCallback } from 'react';
import { Modal } from 'antd';
import { formatCurrency } from '@helpers/formatCurrency';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import { INSURANCE_CATEGORY } from '@constants';
import ModalBodyHealthInsurance from './Health';
import ModalBodyLifeInsurance from './Life';
import { getInsuranceDetail } from '@services/insurances';
import ModalTitleHealthInsurance from './Title/Health';
import ModalTitleLifeInsurance from './Title/Life';

const PreviewModalInsurance = ({ previewInsurance, onClose }) => {
  const [insuranceDetails, setInsuranceDetails] = useState(null);

  const { t } = useTranslation(['insurances']);

  const handleGetInsuranceDetails = useCallback(async () => {
    if (previewInsurance?.id) {
      const response = await getInsuranceDetail(previewInsurance?.id);
      setInsuranceDetails(response);
    }
  }, [previewInsurance?.id]);

  useEffect(() => {
    handleGetInsuranceDetails();
  }, [handleGetInsuranceDetails]);

  const formatPrices = (fromValue, toValue) => {
    const value =
      fromValue && toValue
        ? `${formatCurrency(fromValue)} - ${formatCurrency(toValue)}`
        : formatCurrency(fromValue || toValue);
    return (
      <p className='text-2xl font-bold text-ultramarine-blue'>
        {`${value} VNĐ ${t('insurances:card:month')}`}
      </p>
    );
  };

  const getIconUrl = level => {
    switch (level) {
      case 'high':
        return (
          <Image
            width={18}
            height={18}
            src='/svg/check-success.svg'
            alt='Check Success'
          />
        );
      case 'medium':
        return (
          <Image
            width={18}
            height={18}
            src='/svg/check-warning.svg'
            alt='Check Warning'
          />
        );
      case 'not-support':
        return (
          <Image width={18} height={18} src='/svg/close.svg' alt='close' />
        );
      case 'none':
        return (
          <Image width={18} height={18} src='/svg/warning.svg' alt='warning' />
        );
      default:
        return null;
    }
  };

  const ModalTitle = () => {
    switch (previewInsurance?.insurance_category?.label) {
      case INSURANCE_CATEGORY.HEALTH:
        return (
          <ModalTitleHealthInsurance
            insuranceDetails={insuranceDetails}
            getIconUrl={getIconUrl}
            formatPrices={formatPrices}
          />
        );
      case INSURANCE_CATEGORY.LIFE:
        return (
          <ModalTitleLifeInsurance
            insuranceDetails={insuranceDetails}
            getIconUrl={getIconUrl}
            formatPrices={formatPrices}
          />
        );
      default:
        return null;
    }
  };
  const ModalBody = () => {
    switch (previewInsurance?.insurance_category?.label) {
      case INSURANCE_CATEGORY.HEALTH:
        return <ModalBodyHealthInsurance insuranceDetails={insuranceDetails} />;
      case INSURANCE_CATEGORY.LIFE:
        return <ModalBodyLifeInsurance insuranceDetails={insuranceDetails} />;
      default:
        return null;
    }
  };

  const handleClickClose = () => {
    setInsuranceDetails(null);
    onClose();
  };

  return (
    <Modal
      centered
      open={!!previewInsurance}
      footer={null}
      width='70%'
      destroyOnClose
      onCancel={handleClickClose}
      title={<ModalTitle />}
    >
      <ModalBody />
    </Modal>
  );
};

export default PreviewModalInsurance;
