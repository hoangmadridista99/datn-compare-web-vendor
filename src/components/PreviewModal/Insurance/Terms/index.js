/* eslint-disable indent */
import React from 'react';
import { Skeleton } from 'antd';
import { useTranslation } from 'next-i18next';
import { formatCurrency } from '@helpers/formatCurrency';

const PreviewModalTermInsurance = ({
  insuranceDetails,
  getIconUrl,
  renderField,
}) => {
  const { t } = useTranslation(['insurances']);
  return (
    <div className='mb-6 font-normal text-nickel'>
      <p className='mb-4 text-xl font-medium text-arsenic'>
        {t('insurances:form:terms:title')}
      </p>
      <Skeleton active paragraph={{ rows: 2 }} loading={!insuranceDetails}>
        {/* age_eligibility */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>{t('insurances:form:terms:age_eligibility')}</p>

          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(insuranceDetails?.terms.age_eligibility?.level)}
            </div>

            <div className='ml-4'>
              {!insuranceDetails?.terms.age_eligibility.from &&
              !insuranceDetails?.terms.age_eligibility.to &&
              !insuranceDetails?.terms.age_eligibility.description
                ? t('insurances:field:not_support')
                : renderField(
                    insuranceDetails?.terms.age_eligibility.from,
                    insuranceDetails?.terms.age_eligibility.to,
                    null,
                    'old',
                    insuranceDetails?.terms.age_eligibility.description,
                    true
                  )}
            </div>
          </div>
        </div>

        {/* deadline_for_deal */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>
            {t('insurances:form:terms:deadline_for_deal')}
          </p>

          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(insuranceDetails?.terms.deadline_for_deal.level)}
            </div>

            <div className='ml-4'>
              {renderField(
                insuranceDetails?.terms?.deadline_for_deal?.from,
                insuranceDetails?.terms?.deadline_for_deal?.to,
                insuranceDetails?.terms?.deadline_for_deal?.value,
                insuranceDetails?.terms?.deadline_for_deal?.type,
                insuranceDetails?.terms?.deadline_for_deal?.description
              )}
            </div>
          </div>
        </div>

        {/* insurance_minimum_fee */}
        <div className='mb-6 flex items-center justify-between last:mb-0'>
          <p className='w-2/5'>
            {t('insurances:form:terms:insurance_minimum_fee')}
          </p>

          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(insuranceDetails?.terms.insurance_minimum_fee.level)}
            </div>

            <div className='ml-4'>
              {!insuranceDetails?.terms.insurance_minimum_fee.value &&
              !insuranceDetails?.terms.insurance_minimum_fee.description ? (
                t('insurances:field:not_support')
              ) : (
                <>
                  {insuranceDetails?.terms.insurance_minimum_fee.value && (
                    <p>
                      {`${t(
                        'insurances:field:amount_of_money'
                      )}: ${formatCurrency(
                        insuranceDetails?.terms?.insurance_minimum_fee?.value
                      )} VNĐ`}
                    </p>
                  )}
                  {insuranceDetails?.terms.insurance_minimum_fee
                    .description && (
                    <p>
                      {
                        insuranceDetails?.terms.insurance_minimum_fee
                          .description
                      }
                    </p>
                  )}
                </>
              )}
            </div>
          </div>
        </div>

        {/* insured_person */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>{t('insurances:form:terms:insured_person')}</p>

          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(insuranceDetails?.terms.insured_person.level)}
            </div>

            <div className='ml-4'>
              {!insuranceDetails?.terms?.insured_person?.value ||
              !insuranceDetails?.terms?.insured_person?.description ? (
                t('insurances:field:not_support')
              ) : (
                <>
                  {insuranceDetails?.terms.insured_person.value && (
                    <p>
                      {insuranceDetails?.terms.insured_person.value
                        .map(item => t(`insurances:form:options:${item}`))
                        .join(', ')}
                    </p>
                  )}
                  {insuranceDetails?.terms?.insured_person?.description && (
                    <p>
                      {insuranceDetails?.terms?.insured_person?.description}
                    </p>
                  )}
                </>
              )}
            </div>
          </div>
        </div>

        {/* profession */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>{t('insurances:form:terms:profession')}</p>
          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(insuranceDetails?.terms.profession.level)}
            </div>

            <p className='ml-4 flex-1'>
              {!insuranceDetails?.terms.profession.text
                ? t('insurances:field:not_support')
                : insuranceDetails?.terms.profession.text
                    .map(item => t(`insurances:form:options:${item}`))
                    .join(', ')}
            </p>
          </div>
        </div>

        {/* deadline_for_payment */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>
            {t('insurances:form:terms:deadline_for_payment')}
          </p>

          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(insuranceDetails?.terms.deadline_for_payment.level)}
            </div>

            {!insuranceDetails?.terms.deadline_for_payment.value &&
            !insuranceDetails?.terms.deadline_for_payment.description ? (
              <p className='ml-4'>{t('insurances:field:not_support')}</p>
            ) : (
              <div className='ml-4 flex-1'>
                {insuranceDetails?.terms.deadline_for_payment.value && (
                  <p>
                    {insuranceDetails?.terms.deadline_for_payment.value
                      .map(element => t(`insurances:form:options:${element}`))
                      .join(', ')}
                  </p>
                )}
                {insuranceDetails?.terms.deadline_for_payment.description && (
                  <p className='text-nickel'>
                    {insuranceDetails?.terms.deadline_for_payment.description}
                  </p>
                )}
              </div>
            )}
          </div>
        </div>

        {/* age_of_contract_termination */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5 '>
            {t('insurances:form:terms:age_of_contract_termination')}
          </p>
          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(
                insuranceDetails?.terms.age_of_contract_termination.level
              )}
            </div>

            <div className='ml-4 flex-1'>
              {renderField(
                insuranceDetails?.terms.age_of_contract_termination.from,
                insuranceDetails?.terms.age_of_contract_termination.to,
                null,
                insuranceDetails?.terms.age_of_contract_termination.type,
                insuranceDetails?.terms.age_of_contract_termination.description
              )}
            </div>
          </div>
        </div>

        {/* termination_conditions */}
        {insuranceDetails?.terms?.termination_conditions && (
          <div className='mb-6 flex justify-between'>
            <p className='w-2/5 '>
              {t('insurances:form:terms:termination_conditions')}
            </p>
            <div className='flex flex-1'>
              <div className='mt-1'>
                {getIconUrl(
                  insuranceDetails?.terms.termination_conditions.level
                )}
              </div>

              <p className='ml-4 flex-1'>
                {insuranceDetails?.terms.termination_conditions.text ??
                  t('insurances:field:not_support')}
              </p>
            </div>
          </div>
        )}

        {/* customer_segment */}
        {insuranceDetails?.terms?.customer_segment && (
          <div className='mb-6 flex justify-between'>
            <p className='w-2/5'>
              {t('insurances:form:terms:customer_segment')}
            </p>
            <div className='flex flex-1'>
              <div className='mt-1'>
                {getIconUrl(insuranceDetails?.terms.customer_segment?.level)}
              </div>

              <p className='ml-4'>
                {!insuranceDetails?.terms.customer_segment.text
                  ? t('insurances:field:not_support')
                  : insuranceDetails?.terms.customer_segment.text
                      .map(element => t(`insurances:form:options:${element}`))
                      .join(', ')}
              </p>
            </div>
          </div>
        )}

        {/* total_sum_insured */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>
            {t('insurances:form:terms:total_sum_insured')}
          </p>
          <div className='flex flex-1'>
            <div className='mt-1'>
              {getIconUrl(insuranceDetails?.terms.total_sum_insured.level)}
            </div>

            <div className='ml-4 flex-1'>
              {!insuranceDetails?.terms.total_sum_insured.from &&
              !insuranceDetails?.terms.total_sum_insured.to &&
              !insuranceDetails?.terms.total_sum_insured.description
                ? t('insurances:field:not_support')
                : renderField(
                    formatCurrency(
                      insuranceDetails?.terms.total_sum_insured.from
                    ),
                    formatCurrency(
                      insuranceDetails?.terms.total_sum_insured.to
                    ),
                    null,
                    'VNĐ',
                    insuranceDetails?.terms.total_sum_insured.description,
                    false
                  )}
            </div>
          </div>
        </div>

        {/* monthly_fee */}
        <div className='mb-6 flex justify-between'>
          <p className='w-2/5'>{t('insurances:form:terms:monthly_fee')}</p>
          <div className='flex flex-1'>
            {getIconUrl(insuranceDetails?.terms.monthly_fee.level)}

            <div className='ml-4 flex-1'>
              {!insuranceDetails?.terms.monthly_fee.from &&
              !insuranceDetails?.terms.monthly_fee.to &&
              !insuranceDetails?.terms.monthly_fee.description
                ? t('insurances:field:not_support')
                : renderField(
                    formatCurrency(insuranceDetails?.terms.monthly_fee.from),
                    formatCurrency(insuranceDetails?.terms.monthly_fee.to),
                    null,
                    'VNĐ',
                    insuranceDetails?.terms.monthly_fee.description,
                    false
                  )}
            </div>
          </div>
        </div>
      </Skeleton>
    </div>
  );
};

export default PreviewModalTermInsurance;
