/* eslint-disable indent */
import { formatCurrency } from '@helpers/formatCurrency';
import { Skeleton } from 'antd';
import Image from 'next/image';
import React from 'react';
import { useTranslation } from 'next-i18next';

const ModalTitleHealthInsurance = ({
  insuranceDetails,
  getIconUrl,
  formatPrices,
}) => {
  const { t } = useTranslation(['insurances']);

  return (
    <>
      <div className='mb-4 flex items-center justify-between'>
        <Skeleton
          loading={!insuranceDetails}
          avatar={{ shape: 'square', size: 70 }}
          paragraph={{ rows: 1 }}
          active
        >
          <div className='inline-flex w-1/2'>
            <Image
              src={insuranceDetails?.company.logo}
              width={64}
              height={64}
              alt={insuranceDetails?.company.short_name}
              className='object-none'
            />

            <div className='ml-4 flex w-full flex-col justify-between'>
              <h1 className='text-2xl text-arsenic'>
                {insuranceDetails?.name}
              </h1>
              <p className='text-base font-medium text-nickel'>
                {insuranceDetails?.company.long_name}
              </p>
            </div>
          </div>
          {insuranceDetails && (
            <div className='inline-flex whitespace-nowrap text-right'>
              {(insuranceDetails?.terms.monthly_fee.from ||
                insuranceDetails?.terms.monthly_fee.to) &&
                formatPrices(
                  insuranceDetails?.terms.monthly_fee.from,
                  insuranceDetails?.terms.monthly_fee.to
                )}
            </div>
          )}
        </Skeleton>
      </div>

      <Skeleton
        className='mt-4'
        paragraph={{ rows: 2 }}
        active
        loading={!insuranceDetails}
      >
        <div className='text-sm font-normal text-nickel'>
          <p className='mb-3 flex items-center'>
            {getIconUrl(insuranceDetails?.inpatient?.room_type?.level)}
            <span className='ml-2 mr-1'>
              {t('insurances:preview:inpatient:room_type')}
            </span>
            <span>
              {!insuranceDetails?.inpatient?.room_type?.values
                ? t('insurances:field:not_support')
                : insuranceDetails?.inpatient?.room_type?.values
                    .map(item => t(`insurances:form:options:${item}`))
                    .join(', ')}
            </span>
          </p>

          <p className='mb-3 flex items-center'>
            {getIconUrl(
              insuranceDetails?.inpatient?.for_hospitalization?.level
            )}
            <span className='ml-2 mr-1'>
              {t('insurances:preview:inpatient:for_hospitalization')}
            </span>
            <span>
              {!insuranceDetails?.inpatient?.for_hospitalization?.value ||
              !insuranceDetails?.inpatient?.for_hospitalization?.type
                ? t('insurances:field:not_support')
                : `Tối đa ${formatCurrency(
                    insuranceDetails?.inpatient?.for_hospitalization?.value
                  )} VNĐ/ ${t(
                    `insurances:field:${insuranceDetails?.inpatient?.for_hospitalization?.type}`
                  )}`}
            </span>
          </p>

          <p className='mb-3 flex items-center'>
            {getIconUrl(insuranceDetails?.inpatient?.for_cancer?.level)}
            <span className='ml-2 mr-1'>
              {t('insurances:preview:inpatient:for_cancer')}
            </span>
            <span>
              {!insuranceDetails?.inpatient?.for_cancer?.value
                ? t('insurances:field:not_support')
                : `Tối đa ${formatCurrency(
                    insuranceDetails?.inpatient?.for_cancer?.value
                  )} VNĐ/ ${t('insurances:field:therapy')}`}
            </span>
          </p>

          <p className='mb-3 flex items-center'>
            {getIconUrl(insuranceDetails?.dental ? 'high' : 'none')}
            <span className='ml-2 mr-1'>{`${t(
              'insurances:form:dental:title'
            )}: `}</span>
            <span>
              {insuranceDetails?.dental
                ? t('insurances:field:support')
                : t('insurances:field:not_support')}
            </span>
          </p>

          <p className='mb-3 flex items-center'>
            {getIconUrl(insuranceDetails?.obstetric ? 'high' : 'none')}
            <span className='ml-2 mr-1'>{`${t(
              'insurances:form:obstetric:title'
            )}: `}</span>
            <span>
              {insuranceDetails?.obstetric
                ? t('insurances:field:support')
                : t('insurances:field:not_support')}
            </span>
          </p>
        </div>
      </Skeleton>
    </>
  );
};

export default ModalTitleHealthInsurance;
