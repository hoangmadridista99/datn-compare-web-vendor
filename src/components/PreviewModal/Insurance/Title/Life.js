/* eslint-disable indent */
import React from 'react';
import { Skeleton } from 'antd';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';
import { OPTION_YEAR_OR_OLD } from '@constants';

const ModalTitleLifeInsurance = ({
  insuranceDetails,
  getIconUrl,
  formatPrices,
}) => {
  const { t } = useTranslation(['insurances']);

  const getValueDeadlineForDeal = item => {
    if (!item) return t('insurances:field:not_support');
    const { from, to, type, value } = item;
    if (!from && !to && !type && !value)
      return t('insurances:field:not_support');

    switch (type) {
      case OPTION_YEAR_OR_OLD.YEAR:
        if (!value) return t('insurances:field:not_support');
        return `${t('insurances:field:in')} ${value} ${t(
          'insurances:field:year'
        )}`;
      case OPTION_YEAR_OR_OLD.OLD:
        if (!from && !to) return t('insurances:field:not_support');
        return from && to
          ? `${t('insurances:field:from')} ${from} ${t(
              'insurances:field:to'
            )} ${to} ${t('insurances:field:old')}`
          : `${t('insurances:field:from')} ${from || to} ${t(
              'insurances:field:old'
            )}`;
      default:
        return t('insurances:field:not_support');
    }
  };

  return (
    <>
      <div className='mb-4 flex items-center justify-between'>
        <Skeleton
          loading={!insuranceDetails}
          avatar={{ shape: 'square', size: 70 }}
          paragraph={{ rows: 1 }}
          active
        >
          <div className='inline-flex w-1/2'>
            <Image
              src={insuranceDetails?.company.logo}
              width={64}
              height={64}
              alt={insuranceDetails?.company.short_name}
              className='object-none'
            />

            <div className='ml-4 flex w-full flex-col justify-between'>
              <h1 className='text-2xl text-arsenic'>
                {insuranceDetails?.name}
              </h1>
              <p className='text-base font-medium text-nickel'>
                {insuranceDetails?.company.long_name}
              </p>
            </div>
          </div>
          {insuranceDetails && (
            <div className='inline-flex whitespace-nowrap text-right'>
              {(insuranceDetails?.terms.monthly_fee.from ||
                insuranceDetails?.terms.monthly_fee.to) &&
                formatPrices(
                  insuranceDetails?.terms.monthly_fee.from,
                  insuranceDetails?.terms.monthly_fee.to
                )}
            </div>
          )}
        </Skeleton>
      </div>

      <Skeleton
        className='mt-4'
        paragraph={{ rows: 2 }}
        active
        loading={!insuranceDetails}
      >
        <div className='text-sm font-normal text-nickel'>
          <p className='mb-3 flex items-center'>
            {getIconUrl(
              insuranceDetails?.customer_orientation?.acceptance_rate?.level
            )}
            <span className='ml-2 mr-1'>
              {t('insurances:preview:customer_orientation:acceptance_rate')}
            </span>
            <span>
              {insuranceDetails?.customer_orientation?.acceptance_rate?.text ??
                t('insurances:field:not_support')}
            </span>
          </p>

          <p className='mb-3 flex items-center'>
            {getIconUrl(
              insuranceDetails?.customer_orientation
                ?.reception_and_processing_time?.level
            )}
            <span className='ml-2 mr-1'>
              {t(
                'insurances:preview:customer_orientation:reception_and_processing_time'
              )}
            </span>
            <span>
              {insuranceDetails?.customer_orientation
                ?.reception_and_processing_time?.text ??
                t('insurances:field:not_support')}
            </span>
          </p>

          <p className='mb-3 flex items-center'>
            {getIconUrl(insuranceDetails?.terms?.deadline_for_deal?.level)}
            <span className='ml-2 mr-1'>
              {t('insurances:preview:terms:deadline_for_deal')}
            </span>
            <span>
              {getValueDeadlineForDeal(
                insuranceDetails?.terms?.deadline_for_deal
              )}
            </span>
          </p>

          <p className='mb-3 flex items-center'>
            {getIconUrl(insuranceDetails?.terms?.deadline_for_payment?.level)}
            <span className='ml-2 mr-1'>
              {t('insurances:preview:terms:deadline_for_payment')}
            </span>
            <span>
              {!insuranceDetails?.terms?.deadline_for_payment?.value
                ? t('insurances:field:not_support')
                : insuranceDetails?.terms?.deadline_for_payment?.value
                    .map(item => t(`insurances:field:${item}`))
                    .join(', ')}
            </span>
          </p>
        </div>
      </Skeleton>
    </>
  );
};

export default ModalTitleLifeInsurance;
