import React, { useState, useEffect, useCallback } from 'react';
import { Pagination, Modal, Skeleton } from 'antd';

import { getBlogDetails, getComments } from '@services/blogs';
import { handleError } from '@helpers/handleError';
import BlogComment from './Comment';
import Image from 'next/image';
import dayjs from 'dayjs';
import { useTranslation } from 'next-i18next';

const PreviewModalBlog = ({ blogId, onClose, locale }) => {
  const [blogDetails, setBlogDetails] = useState(null);

  const [blogComments, setBlogComments] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);

  const { t } = useTranslation(['errors']);

  const handleGetBlogDetails = useCallback(
    async signal => {
      if (blogId) {
        const response = await getBlogDetails(blogId, signal);
        setBlogDetails(response);
      }
    },
    [blogId]
  );
  const handleGetBlogDetailsAndComments = useCallback(
    async (page, signal) => {
      if (blogId) {
        await handleGetBlogDetails(signal);
        const response = await getComments(blogId, page, signal);
        setBlogComments(response?.data ?? []);
        setTotalPages(response?.meta?.totalPages ?? 0);
      }
    },
    [blogId, handleGetBlogDetails]
  );

  useEffect(() => {
    const controller = new AbortController();

    handleGetBlogDetailsAndComments(1, controller.signal).catch(error =>
      handleError(t, error)
    );

    return () => controller.abort();
  }, [handleGetBlogDetailsAndComments, t]);

  const handleClickPage = async page => {
    try {
      const response = await getComments(blogId, page);
      setBlogComments(response?.data ?? []);
      setCurrentPage(page);
    } catch (error) {
      handleError(t, error);
    }
  };

  return (
    <Modal
      centered
      width='70%'
      destroyOnClose
      open={!!blogId}
      footer={null}
      onCancel={onClose}
    >
      <div className='no-scrollbar max-h-screen-7/10 overflow-hidden overflow-y-scroll px-2 py-3'>
        <Skeleton loading={!blogDetails} paragraph={{ rows: 1 }} active>
          <div className='mb-3 flex items-center text-sm text-nickel'>
            <div className='relative mr-2 h-5 w-5 overflow-hidden rounded-full'>
              <Image
                src={
                  blogDetails?.user?.avatar_profile_url ??
                  '/svg/avatar-default.svg'
                }
                fill
                alt='Avatar'
                className='rounded-full'
              />
            </div>
            <p>{`${blogDetails?.user?.first_name} ${blogDetails?.user?.last_name}`}</p>
            <div className='mx-1 h-3 w-0.25 bg-arsenic' />
            {blogDetails?.created_at && (
              <p>{dayjs(blogDetails.created_at).format('DD MMM YYYY')}</p>
            )}
          </div>
        </Skeleton>
        <Skeleton loading={!blogDetails} paragraph={{ rows: 1 }} active>
          <p className='mb-3 text-3.5xl font-bold leading-12'>
            {blogDetails?.title}
          </p>
        </Skeleton>
        <Skeleton loading={!blogDetails} paragraph={{ rows: 1 }} active>
          <p className='mb-6 inline-block rounded-lg bg-lavender-web p-1 text-xs text-azure'>
            {locale === 'vi'
              ? blogDetails?.blog_category?.vn_label
              : blogDetails?.blog_category?.en_label}
          </p>
        </Skeleton>
        <Skeleton loading={!blogDetails} paragraph={{ rows: 1 }} active>
          <p className='mb-8 text-base text-nickel'>
            {blogDetails?.description}
          </p>
        </Skeleton>
        <Skeleton loading={!blogDetails} paragraph={{ rows: 5 }} active>
          <div
            className='mb-6 [&>*]:text-nickel [&_h2]:mb-8 [&_h2]:font-bold [&_img]:mx-auto [&_p]:mb-8'
            dangerouslySetInnerHTML={{
              __html: decodeURI(blogDetails?.content),
            }}
          />
        </Skeleton>
        {blogComments && blogComments.length > 0 && (
          <div className='rounded-xl bg-pale-silver p-5'>
            {blogComments.map(item => (
              <BlogComment key={item.id} data={item} />
            ))}
            <Pagination
              current={currentPage}
              total={totalPages}
              pageSize={1}
              showSizeChanger={false}
              hideOnSinglePage
              className='mt-4 text-right'
              onChange={handleClickPage}
            />
          </div>
        )}
      </div>
    </Modal>
  );
};

export default PreviewModalBlog;
