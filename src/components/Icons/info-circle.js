import * as React from 'react';

function InfoCircleIconSvg(props) {
  return (
    <svg
      width={24}
      height={24}
      viewBox='0 0 24 24'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      {...props}
    >
      <path
        d='M12 22c5.5 0 10-4.5 10-10S17.5 2 12 2 2 6.5 2 12s4.5 10 10 10z'
        stroke='#6C6F7B'
        strokeWidth={1.5}
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M12.138 8C10.673 8 10 8.934 10 9.564c0 .456.358.666.651.666.586 0 .348-.9 1.455-.9.543 0 .977.258.977.794 0 .63-.608.993-.966 1.32-.315.291-.728.77-.728 1.774 0 .607.152.782.597.782.532 0 .64-.257.64-.479 0-.607.012-.957.609-1.459.293-.245 1.215-1.039 1.215-2.136C14.45 8.83 13.528 8 12.138 8z'
        fill='#6C6F7B'
      />
      <path
        d='M11.995 16h.009'
        stroke='#6C6F7B'
        strokeWidth={2}
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
}

export default InfoCircleIconSvg;
