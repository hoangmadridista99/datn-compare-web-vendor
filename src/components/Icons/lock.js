import React from 'react';

const LockIconSvg = props => (
  <svg
    width={15}
    height={18}
    viewBox='0 0 15 18'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...props}
  >
    <g
      stroke='#404249'
      strokeWidth={1.5}
      strokeLinecap='round'
      strokeLinejoin='round'
    >
      <path d='M11.687 6.873V5.084a3.793 3.793 0 00-3.793-3.792 3.792 3.792 0 00-3.808 3.775v1.807' />
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M11.07 16.708H4.701a3.16 3.16 0 01-3.16-3.16V9.974a3.16 3.16 0 013.16-3.16h6.367a3.16 3.16 0 013.16 3.16v3.574a3.16 3.16 0 01-3.16 3.16z'
      />
      <path d='M7.886 10.835v1.851' />
    </g>
  </svg>
);

export default LockIconSvg;
