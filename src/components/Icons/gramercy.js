import React from 'react';

const GramercyIconSvg = props => (
  <svg
    width={20}
    height={20}
    viewBox='0 0 20 20'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...props}
  >
    <path
      d='M15.8919 4.1249C19.2003 7.43324 19.1419 12.8332 15.7252 16.0749C12.5669 19.0665 7.44192 19.0665 4.27525 16.0749C0.850253 12.8332 0.79191 7.43324 4.10858 4.1249C7.35858 0.866569 12.6419 0.866569 15.8919 4.1249Z'
      strokeWidth='1.5'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
    <path
      d='M13.2002 13.3916C11.4336 15.0583 8.56693 15.0583 6.80859 13.3916'
      strokeWidth='1.5'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </svg>
);

export default GramercyIconSvg;
