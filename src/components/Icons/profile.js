import * as React from 'react';

function ProfileIconSvg(props) {
  return (
    <svg
      width={14}
      height={18}
      viewBox='0 0 14 18'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      {...props}
    >
      <g
        fillRule='evenodd'
        clipRule='evenodd'
        stroke='#404249'
        strokeLinecap='round'
        strokeLinejoin='round'
      >
        <path
          d='M6.987 11.788c-3.223 0-5.975.487-5.975 2.439s2.735 2.456 5.975 2.456c3.223 0 5.975-.488 5.975-2.439 0-1.95-2.734-2.456-5.975-2.456z'
          strokeWidth={1.5}
        />
        <path
          d='M6.987 9.005a3.83 3.83 0 10-3.83-3.83 3.817 3.817 0 003.803 3.83h.027z'
          strokeWidth={1.42857}
        />
      </g>
    </svg>
  );
}

export default ProfileIconSvg;
