import React from 'react';

const CloseIconSvg = () => (
  <svg
    width='18'
    height='18'
    viewBox='0 0 18 18'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
  >
    <g id='Frame 1321314670'>
      <path
        id='Vector'
        d='M1 17L17 1'
        stroke='#C7C9CE'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        id='Vector_2'
        d='M17 17L1 1'
        stroke='#C7C9CE'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </g>
  </svg>
);

export default CloseIconSvg;
