import React from 'react';

const InformationIconSvg = props => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    width={24}
    height={24}
    viewBox='0 0 24 24'
    fill='none'
    {...props}
  >
    <path
      d='M12 2.9a9.1 9.1 0 019.1 9.1 9.1 9.1 0 01-9.1 9.1A9.1 9.1 0 012.9 12 9.1 9.1 0 0112 2.9zm0 .34a8.76 8.76 0 100 17.521A8.76 8.76 0 0012 3.24zm-.139 7.39a.17.17 0 01.17.17v6.435a.17.17 0 01-.34 0V10.8a.17.17 0 01.17-.169zm.027-3.486a.393.393 0 110 .785.393.393 0 010-.785z'
      fill='#404249'
      stroke='#6C6F7B'
    />
  </svg>
);

export default InformationIconSvg;
