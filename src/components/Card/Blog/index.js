import React from 'react';
import Image from 'next/image';
import { Dropdown, Popover, Skeleton } from 'antd';
import { useRouter } from 'next/router';
import dayjs from 'dayjs';
import classNames from 'class-names';
import { useTranslation } from 'next-i18next';

import { STATUS } from '@constants';
import ModalAlertEdit from '@components/Common/ModalAlertEdit';
import useCardBlogLogic from './useCardBlogLogic';

const CardBlog = props => {
  const { isLoading, data } = props;

  const {
    isOpenModalAlertEdit,
    isRemoving,
    dropdownItems,
    handleClickUpdate,
    handleToggleAlertEditWarning,
  } = useCardBlogLogic(props);
  const router = useRouter();
  const { t } = useTranslation(['common', 'blogs']);

  return (
    <>
      <Skeleton
        className='mb-4 last:mb-0'
        loading={isLoading || isRemoving}
        paragraph={{ rows: 2 }}
      >
        <div className='mb-4 flex items-center rounded-lg border border-platinum p-4 last:mb-0'>
          <div className='h-20 w-2/12 overflow-hidden pr-6'>
            <div className='relative h-full max-w-130 overflow-hidden rounded-lg'>
              <Image
                src={data.banner_image_url}
                fill
                className='object-cover'
                alt='Blog'
              />
            </div>
          </div>
          <div className='w-4/12 pr-6'>
            <h3 className='mb-2 text-sm font-bold text-arsenic'>
              {data.title}
            </h3>
            <p className='inline-block rounded-xl bg-lavender-web p-1 text-xs text-azure'>
              {router.locale === 'vi'
                ? data.blog_category.vn_label
                : data.blog_category.en_label}
            </p>
          </div>
          <p className='w-2/12 self-start text-sm font-bold text-nickel'>
            {dayjs(data.created_at).format('DD/MM/YYYY')}
          </p>
          {data.status === STATUS.REJECTED && (
            <Popover
              title={t('blogs:popover')}
              content={
                <p className='max-w-422 break-all text-sm text-nickel'>
                  {data.reason}
                </p>
              }
              trigger='hover'
            >
              <div className='flex w-2/12 items-center justify-center rounded-3xl bg-linen px-4 py-1'>
                <p className='mr-2 text-xs text-vermilion xl:text-base'>
                  {t(`common:status:${data.status}`)}
                </p>
                <Image
                  src='/svg/danger.svg'
                  width={20}
                  height={20}
                  alt='Warning'
                />
              </div>
            </Popover>
          )}
          {data.status !== STATUS.REJECTED && (
            <div
              className={classNames(
                'flex w-2/12 items-center justify-center rounded-3xl px-4 py-1',
                {
                  'bg-cosmic-latte': data.status === STATUS.PENDING,
                  'bg-chinese-white': data.status === STATUS.APPROVED,
                }
              )}
            >
              <p
                className={classNames('text-xs xl:text-base', {
                  'text-royal-orange': data.status === STATUS.PENDING,
                  'text-apple': data.status === STATUS.APPROVED,
                })}
              >
                {t(`common:status:${data.status}`)}
              </p>
            </div>
          )}
          <div className='flex w-2/12 justify-center'>
            <Dropdown
              placement='bottom'
              menu={{ items: dropdownItems }}
              trigger={['click']}
            >
              <button className='group flex w-4/5 items-center justify-center rounded-lg border border-nickel py-2 transition hover:border-ultramarine-blue'>
                <p className='mr-2 text-nickel group-hover:text-ultramarine-blue'>
                  Action
                </p>
                <Image
                  src='/svg/arrow-down-1.svg'
                  width={24}
                  height={24}
                  alt='Arrow Down'
                />
              </button>
            </Dropdown>
          </div>
        </div>
      </Skeleton>
      <ModalAlertEdit
        type='blog'
        isOpen={isOpenModalAlertEdit}
        onCancel={() => handleToggleAlertEditWarning(false)}
        onOk={handleClickUpdate}
      />
    </>
  );
};

export default CardBlog;
