import { STATUS } from '@constants';
import { handleError } from '@helpers/handleError';
import { removeBlog } from '@services/blogs';
import { notification } from 'antd';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

const useCardBlogLogic = props => {
  const {
    data,
    handleOpenModalPreviewBlog,
    handleOpenModalUpdateBlog,
    handleGetBlogs,
  } = props;

  const [isRemoving, setIsRemoving] = useState(false);
  const [isOpenModalAlertEdit, setIsOpenModalAlertEdit] = useState(false);

  const { t } = useTranslation(['common', 'blogs']);

  const handleUpdate = () => {
    if (data.status === STATUS.PENDING) {
      handleOpenModalUpdateBlog(data?.id);
    }
  };
  const handleToggleAlertEditWarning = isOpen =>
    setIsOpenModalAlertEdit(isOpen);
  const handleClickUpdate = () => {
    setIsOpenModalAlertEdit(false);
    handleOpenModalUpdateBlog(data?.id);
  };
  const handleClickRemove = async () => {
    try {
      setIsRemoving(true);
      await removeBlog(data?.id);
      notification.success({ message: t('blogs:responses:remove') });

      handleGetBlogs();
    } catch (error) {
      handleError(error);
    } finally {
      setIsRemoving(false);
    }
  };

  const dropdownItemUpdate = [
    {
      key: 'update',
      label: (
        <p
          className='cursor-pointer
        text-nickel transition duration-300 hover:text-azure'
          onClick={
            data.status === STATUS.APPROVED
              ? () => handleToggleAlertEditWarning(true)
              : handleUpdate
          }
        >
          {t('blogs:button:update')}
        </p>
      ),
    },
  ];

  const dropdownItems = [
    {
      key: 'preview',
      label: (
        <p
          className='cursor-pointer
        text-nickel transition duration-300 hover:text-azure'
          onClick={() => handleOpenModalPreviewBlog(data?.id)}
        >
          {t('blogs:button:preview')}
        </p>
      ),
    },
    ...(data.status !== STATUS.REJECTED ? dropdownItemUpdate : []),
    {
      key: 'remove',
      label: (
        <p
          className='cursor-pointer
          text-nickel transition duration-300 hover:text-azure'
          onClick={handleClickRemove}
        >
          {t('blogs:button:remove')}
        </p>
      ),
    },
  ];

  return {
    isRemoving,
    isOpenModalAlertEdit,
    handleClickUpdate,
    handleToggleAlertEditWarning,
    dropdownItems,
  };
};

export default useCardBlogLogic;
