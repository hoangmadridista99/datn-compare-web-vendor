import React from 'react';
import Image from 'next/image';
import { Dropdown, Popover, Skeleton } from 'antd';
import dayjs from 'dayjs';

import { formatCurrency } from '@helpers/formatCurrency';
import classNames from 'class-names';
import ModalAlertEdit from '@components/Common/ModalAlertEdit';
import { MODEL_ALERT_EDIT_TYPE, STATUS } from '@constants';
import useCardInsuranceLogic from './useCardInsuranceLogic';
import { useTranslation } from 'next-i18next';

const CardInsurance = props => {
  const { data, isLoading } = props;

  const {
    isModalEditWarning,
    isRemoving,
    dropdownItems,
    handleClickUpdate,
    handleToggleModalEditWarning,
  } = useCardInsuranceLogic(props);
  const { t } = useTranslation(['insurances', 'common']);

  return (
    <>
      <Skeleton
        className='mb-4 last:mb-0'
        loading={isLoading || isRemoving}
        paragraph={{ rows: 2 }}
      >
        <div className='mb-4 flex items-center rounded-lg border border-platinum p-4 last:mb-0'>
          <div className='flex w-3/12'>
            <Image
              src={data.company.logo}
              width={48}
              height={48}
              alt={data.name}
              className='object-contain'
            />
            <div className='ml-4 flex flex-col'>
              <p className='mb-1 text-base font-bold text-arsenic'>
                {data.name}
              </p>
              <span className='text-sm text-nickel'>
                {t(
                  `insurances:form:options:${data?.insurance_category?.label}`
                )}
              </span>
            </div>
          </div>
          <div className='flex w-3/12 flex-col'>
            <div className='mb-1 flex items-center'>
              <p className='mr-1 font-bold text-ultramarine-blue'>{`${formatCurrency(
                data.terms?.monthly_fee?.from ?? 0
              )} VNĐ`}</p>
              <span className='text-silver-metallic'>
                {t('insurances:card:month')}
              </span>
            </div>
            <div className='flex items-center'>
              <span className='mr-0.5 text-sm font-bold text-nickel'>
                {data.rating_scores}
              </span>
              <Image src='/svg/star.svg' width={12} height={12} alt='Star' />
              <Image
                src='/svg/dot.svg'
                width={4}
                height={4}
                alt='Dot'
                className='mx-1.5'
              />
              <span className='text-xs text-spanish-gray'>{`${formatCurrency(
                data.total_rating
              )} ${t('insurances:card:evaluate')}`}</span>
            </div>
          </div>
          <p className='w-2/12 self-start text-sm font-bold text-nickel'>
            {dayjs(data.created_at).format('DD/MM/YYYY')}
          </p>
          {data.status === STATUS.REJECTED && (
            <Popover
              title={t('insurances:card:reason')}
              content={
                <p className='max-w-422 break-all text-sm text-nickel'>
                  {data.reason}
                </p>
              }
              trigger='hover'
            >
              <div className='flex w-2/12 items-center justify-center rounded-3xl bg-linen px-4 py-1'>
                <p className='mr-2 text-xs text-vermilion xl:text-base'>
                  {t(`common:status:${data.status}`)}
                </p>
                <Image
                  src='/svg/danger.svg'
                  width={20}
                  height={20}
                  alt='Warning'
                />
              </div>
            </Popover>
          )}
          {data.status !== STATUS.REJECTED && (
            <div
              className={classNames(
                'flex w-2/12 items-center justify-center rounded-3xl px-4 py-1',
                {
                  'bg-cosmic-latte': data.status === STATUS.PENDING,
                  'bg-chinese-white': data.status === STATUS.APPROVED,
                }
              )}
            >
              <p
                className={classNames('text-xs xl:text-base', {
                  'text-royal-orange': data.status === STATUS.PENDING,
                  'text-apple': data.status === STATUS.APPROVED,
                })}
              >
                {t(`common:status:${data.status}`)}
              </p>
            </div>
          )}
          <div className='flex w-2/12 justify-center'>
            <Dropdown
              placement='bottom'
              menu={{ items: dropdownItems }}
              className='dropdown-menu'
              trigger={['click']}
            >
              <button className='group flex w-4/5 items-center justify-center rounded-lg border border-nickel py-2 transition hover:border-ultramarine-blue'>
                <p className='mr-2 text-nickel group-hover:text-ultramarine-blue'>
                  Action
                </p>
                <Image
                  src='/svg/arrow-down-1.svg'
                  width={24}
                  height={24}
                  alt='Arrow Down'
                />
              </button>
            </Dropdown>
          </div>
        </div>
      </Skeleton>
      <ModalAlertEdit
        isOpen={isModalEditWarning}
        onCancel={() => handleToggleModalEditWarning(false)}
        onOk={handleClickUpdate}
        type={MODEL_ALERT_EDIT_TYPE.INSURANCE}
      />
    </>
  );
};

export default CardInsurance;
