import { ROUTE, STATUS } from '@constants';
import { handleError } from '@helpers/handleError';
import { removeInsurance } from '@services/insurances';
import { notification } from 'antd';
import { useState } from 'react';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';

const useCardInsuranceLogic = props => {
  const { handleGetInsurances, data, handleOpenModalPreview } = props;

  const [isRemoving, setIsRemoving] = useState(false);
  const [isModalEditWarning, setIsModalEditWarning] = useState(false);

  const { t } = useTranslation(['insurances', 'common']);
  const router = useRouter();

  const handleUpdateInsuranceWithStatusPending = () => {
    if (data.status === STATUS.PENDING) {
      router.push(`${ROUTE.UPDATE_INSURANCE}/${data?.id}`);
    }
  };

  const handleToggleModalEditWarning = isOpen => setIsModalEditWarning(isOpen);

  const handleClickUpdate = () => {
    setIsModalEditWarning(false);
    router.push(`${ROUTE.UPDATE_INSURANCE}/${data?.id}`);
  };
  const handleClickRemove = async () => {
    try {
      setIsRemoving(true);
      await removeInsurance(data?.id);
      notification.success({ message: t('insurances:success:remove') });
      await handleGetInsurances();
    } catch (error) {
      handleError(error);
    } finally {
      setIsRemoving(false);
    }
  };

  const onClickUpdate =
    data.status === STATUS.APPROVED
      ? () => handleToggleModalEditWarning(true)
      : handleUpdateInsuranceWithStatusPending;

  const dropdownItemUpdate = [
    {
      key: 'update',
      label: (
        <p
          className='cursor-pointer
        text-nickel transition duration-300 hover:text-ultramarine-blue'
          onClick={onClickUpdate}
        >
          {t('insurances:card:edit')}
        </p>
      ),
    },
  ];
  const dropdownItems = [
    {
      key: 'preview',
      label: (
        <p
          className='cursor-pointer
        text-nickel transition duration-300 hover:text-ultramarine-blue'
          onClick={() => handleOpenModalPreview(data)}
        >
          {t('insurances:card:preview')}
        </p>
      ),
    },
    ...(data.status !== STATUS.REJECTED ? dropdownItemUpdate : []),
    {
      key: 'remove',
      label: (
        <p
          className='cursor-pointer
        text-nickel transition duration-300 hover:text-ultramarine-blue'
          onClick={handleClickRemove}
        >
          {t('insurances:card:delete')}
        </p>
      ),
    },
  ];

  return {
    isRemoving,
    isModalEditWarning,
    dropdownItems,
    handleClickUpdate,
    handleToggleModalEditWarning,
  };
};

export default useCardInsuranceLogic;
