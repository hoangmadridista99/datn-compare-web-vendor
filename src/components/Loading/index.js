import { Spin } from 'antd';
import React from 'react';

const Loading = () => (
  <div className='fixed left-0 top-0 z-50 flex h-screen w-screen items-center justify-center bg-black-0.3'>
    <Spin size='large' />
  </div>
);

export default Loading;
