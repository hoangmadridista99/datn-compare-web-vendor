import InfoCircleIconSvg from '@components/Icons/info-circle';
import InformationIconSvg from '@components/Icons/information';
import { Popover } from 'antd';
import React, { memo } from 'react';

const FormField = ({
  children,
  title,
  fieldDetail,
  isHiddenFieldDetails = false,
}) => (
  <div className='mb-6 flex w-full items-start justify-between last:mb-0'>
    <div className='flex w-3/10 items-center'>
      {!isHiddenFieldDetails && fieldDetail && (
        <Popover
          trigger={['hover']}
          placement='right'
          title={title}
          content={
            <p
              className='w-200 text-sm text-nickel'
              style={{ whiteSpace: 'pre-wrap' }}
            >
              {fieldDetail}
            </p>
          }
        >
          <InfoCircleIconSvg />
        </Popover>
      )}
      {!fieldDetail && !isHiddenFieldDetails && <InformationIconSvg />}
      <p className='ml-2 text-base font-bold leading-6 text-arsenic'>{title}</p>
    </div>
    <div className='w-13/20'>{children}</div>
  </div>
);

export default memo(FormField);
