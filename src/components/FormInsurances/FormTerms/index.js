import React, { memo } from 'react';
import { Checkbox, Input, Select, Form, InputNumber } from 'antd';
import { useTranslation } from 'next-i18next';

import FormFieldHaveLevel from '../FormFieldHaveLevel';
import { LIST_CUSTOMERS } from '@constants';

import {
  LIST_YEAR_OR_OLD,
  LIST_PAY,
  LIST_PERSON,
  LIST_PROFESSION,
  INSURANCE_CATEGORY,
} from '@constants';

import { formatCurrency } from '@helpers/formatCurrency';
import { useInputNumber } from '@hooks/useInputNumber';

const { TextArea } = Input;

const FormTerms = ({ insuranceCategory }) => {
  const { t } = useTranslation(['insurances']);
  const { errors, message, onKeyDown } = useInputNumber();

  const hanldeOptions = list =>
    list.map(item => ({
      value: item,
      label: t(`insurances:form:options:${item}`),
    }));
  const handleFormatter = value => {
    if (isNaN(value)) return undefined;

    return value ? formatCurrency(value) : '';
  };
  const handleParser = value => {
    const formatValue = parseInt(value.replace(/\./g, ''));

    if (isNaN(formatValue)) return;

    return parseInt(value.replace(/\./g, ''));
  };

  return (
    <>
      {/* age_eligibility */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:age_eligibility')}
        fieldDetail={t('insurances:fields:age_eligibility')}
        name={['terms', 'age_eligibility']}
      >
        <div className='flex items-center'>
          <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:from')}
          </p>
          <div className='flex w-full items-center'>
            <Form.Item
              className='mx-2 mb-0 w-full'
              name={['terms', 'age_eligibility', 'from']}
              validateStatus={
                errors['terms.age_eligibility'] ? 'error' : undefined
              }
            >
              <InputNumber
                controls={false}
                min={0}
                max={100}
                placeholder={t('insurances:form:placeholder:age')}
                className='input_addon w-full'
                addonAfter={t('insurances:form:prefix:age')}
                onKeyDown={event => onKeyDown(event, 'terms.age_eligibility')}
                size='large'
              />
            </Form.Item>
            <p className='text-base font-bold leading-6 text-arsenic'>
              {t('insurances:form:to')}
            </p>
            <Form.Item
              className='mx-2 mb-0 w-full'
              name={['terms', 'age_eligibility', 'to']}
              validateStatus={
                errors['terms.age_eligibility'] ? 'error' : undefined
              }
            >
              <InputNumber
                controls={false}
                min={0}
                max={100}
                placeholder={t('insurances:form:placeholder:age')}
                className='input_addon w-full'
                addonAfter={t('insurances:form:prefix:age')}
                onKeyDown={event => onKeyDown(event, 'terms.age_eligibility')}
                size='large'
              />
            </Form.Item>
          </div>
        </div>
        {errors['terms.age_eligibility'] && (
          <p className='ml-1/5 text-sm text-venetian-red'>{message}</p>
        )}
        <div className='mt-4 flex'>
          <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['terms', 'age_eligibility', 'description']}
            className='mx-2 w-full'
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* deadline_for_deal */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:deadline_for_deal')}
        fieldDetail={t('insurances:fields:deadline_for_deal')}
        name={['terms', 'deadline_for_deal']}
      >
        <Form.Item name={['terms', 'deadline_for_deal', 'type']}>
          <Select
            placeholder={t('insurances:form:placeholder:select')}
            className='w-full'
            options={hanldeOptions(LIST_YEAR_OR_OLD)}
          />
        </Form.Item>
        <div className='mt-4 flex items-center'>
          <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:from')}
          </p>
          <div className='flex w-full items-center'>
            <Form.Item
              className='mx-2 mb-0 w-full'
              name={['terms', 'deadline_for_deal', 'from']}
              validateStatus={
                errors['terms.deadline_for_deal'] ? 'error' : undefined
              }
            >
              <InputNumber
                controls={false}
                placeholder={t('insurances:form:placeholder:time')}
                className='input_addon w-full'
                addonAfter={t('insurances:form:prefix:year')}
                onKeyDown={event => onKeyDown(event, 'terms.deadline_for_deal')}
                size='large'
              />
            </Form.Item>
            <p className='text-base font-bold leading-6 text-arsenic'>
              {t('insurances:form:to')}
            </p>
            <Form.Item
              className='mx-2 mb-0 w-full'
              name={['terms', 'deadline_for_deal', 'to']}
              validateStatus={
                errors['terms.deadline_for_deal'] ? 'error' : undefined
              }
            >
              <InputNumber
                controls={false}
                placeholder={t('insurances:form:placeholder:time')}
                className='input_addon w-full'
                addonAfter={t('insurances:form:prefix:year')}
                onKeyDown={event => onKeyDown(event, 'terms.deadline_for_deal')}
                size='large'
              />
            </Form.Item>
          </div>
        </div>
        {errors['terms.deadline_for_deal'] && (
          <p className='ml-1/5 text-sm text-venetian-red'>{message}</p>
        )}
        <div className='mt-4 flex w-full items-center'>
          <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:year')}
          </p>
          <Form.Item
            name={['terms', 'deadline_for_deal', 'value']}
            className='mx-2 mb-0 w-full'
            validateStatus={
              errors['terms.deadline_for_deal.value'] ? 'error' : undefined
            }
          >
            <InputNumber
              className='input_addon w-full'
              controls={false}
              placeholder={t('insurances:form:placeholder:year')}
              addonAfter={t('insurances:form:prefix:year')}
              onKeyDown={event =>
                onKeyDown(event, 'terms.deadline_for_deal.value')
              }
              size='large'
            />
          </Form.Item>
        </div>
        {errors['terms.deadline_for_deal.value'] && (
          <p className='ml-1/5 text-sm text-venetian-red'>{message}</p>
        )}
        <div className='mt-4 flex'>
          <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['terms', 'deadline_for_deal', 'description']}
            className='mx-2 w-full'
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* insurance_minimum_fee */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:insurance_minimum_fee')}
        fieldDetail={t('insurances:fields:insurance_minimum_fee')}
        name={['terms', 'insurance_minimum_fee']}
      >
        <div className='flex w-full items-center'>
          <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['terms', 'insurance_minimum_fee', 'value']}
            className='mx-2 mb-0 w-full'
            validateStatus={
              errors['terms.insurance_minimum_fee'] ? 'error' : undefined
            }
          >
            <InputNumber
              className='input_addon w-full'
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              addonAfter='VNĐ'
              onKeyDown={event =>
                onKeyDown(event, 'terms.insurance_minimum_fee')
              }
              size='large'
            />
          </Form.Item>
        </div>
        {errors['terms.insurance_minimum_fee'] && (
          <p className='ml-1/5 text-sm text-venetian-red'>{message}</p>
        )}
        <div className='mt-4 flex'>
          <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['terms', 'insurance_minimum_fee', 'description']}
            className='mx-2 w-full'
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* insured_person */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:insured_person')}
        fieldDetail={t('insurances:fields:insured_person')}
        name={['terms', 'insured_person']}
      >
        <div className='flex w-full items-center'>
          <p className='w-1/5 self-start pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:object')}
          </p>
          <Form.Item
            name={['terms', 'insured_person', 'value']}
            className='mx-2 mb-0 w-full'
          >
            <Checkbox.Group className='flex flex-col'>
              {hanldeOptions(LIST_PERSON).map(item => (
                <Checkbox
                  key={item.value}
                  className='mx-0 mb-6 last:mb-0'
                  value={item.value}
                >
                  {item.label}
                </Checkbox>
              ))}
            </Checkbox.Group>
          </Form.Item>
        </div>
        <div className='mt-4 flex'>
          <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['terms', 'insured_person', 'description']}
            className='mx-2 w-full'
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* profession */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:profession')}
        fieldDetail={t('insurances:fields:profession')}
        name={['terms', 'profession']}
      >
        <Form.Item
          name={['terms', 'profession', 'text']}
          className='mx-2 mb-0 w-full'
        >
          <Checkbox.Group className='flex flex-col'>
            {hanldeOptions(LIST_PROFESSION).map(item => (
              <Checkbox
                key={item.value}
                className='mx-0 mb-6 last:mb-0'
                value={item.value}
              >
                {item.label}
              </Checkbox>
            ))}
          </Checkbox.Group>
        </Form.Item>
      </FormFieldHaveLevel>

      {/* deadline_for_payment */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:deadline_for_payment')}
        fieldDetail={t('insurances:fields:deadline_for_payment')}
        name={['terms', 'deadline_for_payment']}
      >
        <Form.Item
          name={['terms', 'deadline_for_payment', 'value']}
          className='mx-2 mb-0 w-full'
        >
          <Checkbox.Group className='flex flex-col'>
            {hanldeOptions(LIST_PAY).map(item => (
              <Checkbox
                key={item.value}
                className='mx-0 mb-6 last:mb-0'
                value={item.value}
              >
                {item.label}
              </Checkbox>
            ))}
          </Checkbox.Group>
        </Form.Item>
        <Form.Item
          name={['terms', 'deadline_for_payment', 'description']}
          className='mx-2 mt-4 w-full'
        >
          <TextArea
            placeholder={t('insurances:form:placeholder:describe')}
            autoSize={{ minRows: 5, maxRows: 7 }}
          />
        </Form.Item>
      </FormFieldHaveLevel>

      {/* customer_segment */}
      {insuranceCategory === INSURANCE_CATEGORY.HEALTH && (
        <FormFieldHaveLevel
          title={t('insurances:form:terms:customer_segment')}
          fieldDetail={t('insurances:fields:customer_segment')}
          name={['terms', 'customer_segment']}
        >
          <Form.Item
            name={['terms', 'customer_segment', 'text']}
            className='mx-2 mb-0 w-full'
          >
            <Checkbox.Group className='flex flex-col'>
              {hanldeOptions(LIST_CUSTOMERS).map(item => (
                <Checkbox
                  key={item.value}
                  className='mx-0 mb-6 last:mb-0'
                  value={item.value}
                >
                  {item.label}
                </Checkbox>
              ))}
            </Checkbox.Group>
          </Form.Item>
        </FormFieldHaveLevel>
      )}

      {/* age_of_contract_termination */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:age_of_contract_termination')}
        fieldDetail={t('insurances:fields:age_of_contract_termination')}
        name={['terms', 'age_of_contract_termination']}
      >
        <Form.Item name={['terms', 'age_of_contract_termination', 'type']}>
          <Select
            className='w-full'
            options={hanldeOptions(LIST_YEAR_OR_OLD)}
            placeholder={t('insurances:form:placeholder:select')}
          />
        </Form.Item>
        <div className='mt-4 flex items-center'>
          <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:from')}
          </p>
          <div className='flex w-full items-center'>
            <Form.Item
              className='mx-2 mb-0 w-full'
              name={['terms', 'age_of_contract_termination', 'from']}
              validateStatus={
                errors['terms.age_of_contract_termination']
                  ? 'error'
                  : undefined
              }
            >
              <InputNumber
                controls={false}
                min={0}
                max={100}
                placeholder={t('insurances:form:placeholder:year')}
                className='input_addon w-full'
                onKeyDown={event =>
                  onKeyDown(event, 'terms.age_of_contract_termination')
                }
                size='large'
              />
            </Form.Item>
            <p className='text-base font-bold leading-6 text-arsenic'>
              {t('insurances:form:to')}
            </p>
            <Form.Item
              className='mx-2 mb-0 w-full'
              name={['terms', 'age_of_contract_termination', 'to']}
              validateStatus={
                errors['terms.age_of_contract_termination']
                  ? 'error'
                  : undefined
              }
            >
              <InputNumber
                controls={false}
                min={0}
                max={100}
                placeholder={t('insurances:form:placeholder:year')}
                className='input_addon w-full'
                onKeyDown={event =>
                  onKeyDown(event, 'terms.age_of_contract_termination')
                }
                size='large'
              />
            </Form.Item>
          </div>
        </div>
        {errors['terms.age_of_contract_termination'] && (
          <p className='ml-1/5 text-sm text-venetian-red'>{message}</p>
        )}
        <div className='mt-4 flex'>
          <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['terms', 'age_of_contract_termination', 'description']}
            className='mx-2 w-full'
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* termination_conditions */}
      {insuranceCategory === INSURANCE_CATEGORY.LIFE && (
        <FormFieldHaveLevel
          title={t('insurances:form:terms:termination_conditions')}
          fieldDetail={t('insurances:fields:termination_conditions')}
          name={['terms', 'termination_conditions']}
        >
          <Form.Item name={['terms', 'termination_conditions', 'text']}>
            <Input
              placeholder={t('insurances:form:placeholder:condition')}
              size='large'
            />
          </Form.Item>
        </FormFieldHaveLevel>
      )}

      {/* total_sum_insured */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:total_sum_insured')}
        fieldDetail={t('insurances:fields:total_sum_insured')}
        name={['terms', 'total_sum_insured']}
      >
        <div className='flex items-center'>
          <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:from')}
          </p>
          <div className='flex w-full items-center'>
            <Form.Item
              className='mx-2 mb-0 w-full'
              name={['terms', 'total_sum_insured', 'from']}
              validateStatus={
                errors['terms.total_sum_insured'] ? 'error' : undefined
              }
            >
              <InputNumber
                className='input_addon w-full'
                controls={false}
                placeholder={t('insurances:form:placeholder:money')}
                formatter={handleFormatter}
                parser={handleParser}
                addonAfter='VNĐ'
                onKeyDown={event => onKeyDown(event, 'terms.total_sum_insured')}
                size='large'
              />
            </Form.Item>
            <p className='text-base font-bold leading-6 text-arsenic'>
              {t('insurances:form:to')}
            </p>
            <Form.Item
              className='mx-2 mb-0 w-full'
              name={['terms', 'total_sum_insured', 'to']}
              validateStatus={
                errors['terms.total_sum_insured'] ? 'error' : undefined
              }
            >
              <InputNumber
                className='input_addon w-full'
                controls={false}
                placeholder={t('insurances:form:placeholder:money')}
                formatter={handleFormatter}
                parser={handleParser}
                addonAfter='VNĐ'
                onKeyDown={event => onKeyDown(event, 'terms.total_sum_insured')}
                size='large'
              />
            </Form.Item>
          </div>
        </div>
        {errors['terms.total_sum_insured'] && (
          <p className='ml-1/5 text-sm text-venetian-red'>{message}</p>
        )}
        <div className='mt-4 flex'>
          <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['terms', 'total_sum_insured', 'description']}
            className='mx-2 w-full'
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* monthly_fee */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:monthly_fee')}
        fieldDetail={t('insurances:fields:monthly_fee')}
        name={['terms', 'monthly_fee']}
      >
        <div className='flex items-center'>
          <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:from')}
          </p>
          <div className='flex w-full items-center'>
            <Form.Item
              className='mx-2 mb-0 w-full'
              name={['terms', 'monthly_fee', 'from']}
              validateStatus={errors['terms.monthly_fee'] ? 'error' : undefined}
            >
              <InputNumber
                className='input_addon w-full'
                controls={false}
                placeholder={t('insurances:form:placeholder:money')}
                formatter={handleFormatter}
                parser={handleParser}
                addonAfter='VNĐ'
                onKeyDown={event => onKeyDown(event, 'terms.monthly_fee')}
                size='large'
              />
            </Form.Item>
            <p className='text-base font-bold leading-6 text-arsenic'>
              {t('insurances:form:to')}
            </p>
            <Form.Item
              className='mx-2 mb-0 w-full'
              name={['terms', 'monthly_fee', 'to']}
              validateStatus={errors['terms.monthly_fee'] ? 'error' : undefined}
            >
              <InputNumber
                className='input_addon w-full'
                controls={false}
                placeholder={t('insurances:form:placeholder:money')}
                formatter={handleFormatter}
                parser={handleParser}
                addonAfter='VNĐ'
                onKeyDown={event => onKeyDown(event, 'terms.monthly_fee')}
                size='large'
              />
            </Form.Item>
          </div>
        </div>
        {errors['terms.monthly_fee'] && (
          <p className='ml-1/5 text-sm text-venetian-red'>{message}</p>
        )}
        <div className='mt-4 flex'>
          <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['terms', 'monthly_fee', 'description']}
            className='mx-2 w-full'
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>
    </>
  );
};

export default memo(FormTerms);
