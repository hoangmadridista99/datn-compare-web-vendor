import FormFieldHaveLevel from '@components/FormInsurances/FormFieldHaveLevel';
import FormLayout from '@components/FormInsurances/FormLayout';
import React from 'react';
import { Form, Input, InputNumber, Checkbox } from 'antd';
import { useTranslation } from 'next-i18next';
import {
  FIELDS_HEALTH_INSURANCE,
  INSURANCE_CATEGORY,
  LIST_INSURANCE_SCOPE,
} from '@constants';
import { useInputNumber } from '@hooks/useInputNumber';

const { TextArea } = Input;

const CustomerOrientation = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances']);
  const { errors, message, onKeyDown } = useInputNumber();

  const handleOptions = list =>
    list.map(item => ({
      value: item,
      label: t(`insurances:form:options:${item}`),
    }));

  return (
    <div className='border border-t-0 border-platinum bg-white px-4 py-8'>
      <FormLayout
        activeKey='customer_orientation'
        title={t('insurances:form:customer_orientation:title')}
        active={
          activeFields?.customer_orientation ??
          handleActiveField(
            FIELDS_HEALTH_INSURANCE.customer_orientation,
            'customer_orientation'
          )
        }
        category={INSURANCE_CATEGORY.HEALTH}
      >
        {/* insurance_scope */}
        <FormFieldHaveLevel
          title={t('insurances:form:customer_orientation:insurance_scope')}
          fieldDetail={t('insurances:fields:insurance_scope')}
          name={['customer_orientation', 'insurance_scope']}
        >
          <Form.Item
            name={['customer_orientation', 'insurance_scope', 'values']}
            className='mx-2 mb-0 w-full'
          >
            <Checkbox.Group className='flex flex-col'>
              {handleOptions(LIST_INSURANCE_SCOPE).map(item => (
                <Checkbox
                  key={item.value}
                  className='mx-0 mb-6 last:mb-0'
                  value={item.value}
                >
                  {item.label}
                </Checkbox>
              ))}
            </Checkbox.Group>
          </Form.Item>
        </FormFieldHaveLevel>

        {/* waiting_period */}
        <FormFieldHaveLevel
          title={t('insurances:form:customer_orientation:waiting_period')}
          fieldDetail={t('insurances:fields:waiting_period')}
          name={['customer_orientation', 'waiting_period']}
        >
          <div className='flex items-center'>
            <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
              {t('insurances:form:from')}
            </p>
            <div className=' flex w-full items-center'>
              <Form.Item
                className='mx-2 mb-0 w-full'
                name={['customer_orientation', 'waiting_period', 'from']}
                validateStatus={
                  errors['customer_orientation.waiting_period']
                    ? 'error'
                    : undefined
                }
              >
                <InputNumber
                  controls={false}
                  min={0}
                  max={100}
                  placeholder={t('insurances:form:placeholder:time')}
                  className='input_addon w-full'
                  addonAfter={t('insurances:form:prefix:day')}
                  onKeyDown={event =>
                    onKeyDown(event, 'customer_orientation.waiting_period')
                  }
                />
              </Form.Item>
              <p className='text-base font-bold leading-6 text-arsenic'>
                {t('insurances:form:to')}
              </p>
              <Form.Item
                className='mx-2 mb-0 w-full'
                name={['customer_orientation', 'waiting_period', 'to']}
                validateStatus={
                  errors['customer_orientation.waiting_period']
                    ? 'error'
                    : undefined
                }
              >
                <InputNumber
                  controls={false}
                  min={0}
                  max={100}
                  placeholder={t('insurances:form:placeholder:time')}
                  className='input_addon w-full'
                  addonAfter={t('insurances:form:prefix:day')}
                  onKeyDown={event =>
                    onKeyDown(event, 'customer_orientation.waiting_period')
                  }
                />
              </Form.Item>
            </div>
          </div>
          {errors['customer_orientation.waiting_period'] && (
            <p className='ml-1/5 text-sm text-venetian-red'>{message}</p>
          )}
          <div className='mt-4 flex'>
            <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
              {t('insurances:form:description')}
            </p>
            <Form.Item
              name={['customer_orientation', 'waiting_period', 'description']}
              className='mx-2 w-full'
            >
              <TextArea
                placeholder={t('insurances:form:placeholder:detail')}
                autoSize={{ minRows: 5, maxRows: 7 }}
              />
            </Form.Item>
          </div>
        </FormFieldHaveLevel>

        {/* compensation_process */}
        <FormFieldHaveLevel
          title={t('insurances:form:customer_orientation:compensation_process')}
          fieldDetail={t('insurances:fields:compensation_process')}
          name={['customer_orientation', 'compensation_process']}
        >
          <Form.Item
            name={[
              'customer_orientation',
              'compensation_process',
              'description',
            ]}
            className='mx-2 mt-4 w-full'
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
            />
          </Form.Item>
        </FormFieldHaveLevel>

        {/* reception_and_processing_time */}
        <FormFieldHaveLevel
          title={t(
            'insurances:form:customer_orientation:reception_and_processing_time'
          )}
          fieldDetail={t('insurances:fields:reception_and_processing_time')}
          name={['customer_orientation', 'reception_and_processing_time']}
        >
          <Form.Item
            name={[
              'customer_orientation',
              'reception_and_processing_time',
              'text',
            ]}
            className='mx-2 mt-4 w-full'
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
            />
          </Form.Item>
        </FormFieldHaveLevel>
      </FormLayout>
    </div>
  );
};

export default CustomerOrientation;
