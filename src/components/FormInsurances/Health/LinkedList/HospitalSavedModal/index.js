import React, { useState, useEffect, useCallback } from 'react';
import { useTranslation } from 'next-i18next';
import { getHospitals } from '@services/insurances';
import { Modal, Table, Input } from 'antd';
import CloseIconSvg from '@components/Icons/close';
import SearchOutlinedSvg from '@components/Icons/search';
import { handleError } from '@helpers/handleError';

const HospitalSavedModal = ({
  isOpen,
  onCancel,
  onSubmit,
  hospitalsSelected,
}) => {
  const [hospitals, setHospitals] = useState(null);
  const [name, setName] = useState();
  const [selectedRecords, setSelectedRecords] = useState(
    hospitalsSelected ?? []
  );
  const { t } = useTranslation(['insurances']);

  const handleGetHospitals = useCallback(async (name, signal) => {
    const response = await getHospitals(name, signal);

    if (Array.isArray(response)) {
      setHospitals(response);
      return;
    }
    setHospitals([]);
  }, []);

  useEffect(() => {
    const controller = new AbortController();

    handleGetHospitals(undefined, controller.signal).catch(error => {
      console.log(
        '🚀 ~ file: index.js:33 ~ handleGetHospitals ~ error:',
        error
      );
      setHospitals([]);
    });

    return () => controller.abort();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    const controller = new AbortController();
    let id = null;

    handleGetHospitals(name, controller.signal).catch(error =>
      handleError(error)
    );

    return () => {
      controller.abort();
      clearTimeout(id);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [name]);

  const columns = [
    {
      title: t('insurances:form:linkedList:hospital_name'),
      dataIndex: 'name',
    },
    {
      title: t('insurances:form:linkedList:hospital_address'),
      dataIndex: 'address',
    },
    {
      title: t('insurances:form:linkedList:hospital_province'),
      dataIndex: 'province',
    },
    {
      title: t('insurances:form:linkedList:hospital_district'),
      dataIndex: 'district',
    },
  ];

  const rowSelection = {
    selectedRowKeys: selectedRecords.map(item => item.id),
    onChange: (_, selectedRows) => {
      setSelectedRecords(selectedRows);
    },
    getCheckboxProps: record => ({
      disabled: record.name === 'Disabled User',
      name: record.name,
    }),
  };

  const handleChangeName = e => setName(e.target.value);

  const isDisabledButtonSaved =
    !hospitalsSelected && selectedRecords.length < 1;

  return (
    <Modal
      centered
      open={isOpen}
      onCancel={onCancel}
      width={1000}
      closeIcon={<CloseIconSvg />}
      footer={null}
    >
      <div className='px-3 py-4'>
        <div className='mb-5 inline-flex w-full items-center justify-between border-0 border-b border-solid border-cultured py-5'>
          <p className='text-2xl font-medium text-arsenic'>
            {t('insurances:form:linkedList:modal:saved:title')}
          </p>
          <Input
            placeholder={t('insurances:form:linkedList:modal:saved:filter')}
            size='large'
            prefix={<SearchOutlinedSvg />}
            className='w-1/3'
            onChange={handleChangeName}
          />
        </div>
        <Table
          rowKey={'id'}
          size='middle'
          bordered
          dataSource={hospitals}
          columns={columns}
          rowSelection={{
            type: 'checkbox',
            ...rowSelection,
          }}
          scroll={{ y: 350 }}
          pagination={{
            pageSize: 5,
          }}
          loading={!hospitals}
        />
        <div className='mt-4 flex items-center justify-end gap-x-6'>
          <button
            className='rounded-lg border border-nickel px-10 py-3 font-bold text-nickel'
            onClick={onCancel}
          >
            {t('insurances:form:linkedList:modal:button:cancel')}
          </button>
          <button
            className='rounded-lg bg-ultramarine-blue px-10 py-3 font-bold text-cultured aria-disabled:bg-black-0.04 aria-disabled:text-black-0.25'
            disabled={isDisabledButtonSaved}
            aria-disabled={isDisabledButtonSaved}
            onClick={() => onSubmit(selectedRecords ?? [])}
          >
            {t('insurances:form:linkedList:modal:button:ok:saved')}
          </button>
        </div>
      </div>
    </Modal>
  );
};

export default HospitalSavedModal;
