/* eslint-disable indent */
import React, { useState, useEffect } from 'react';
import { Button, Form, Table } from 'antd';
import { useTranslation } from 'next-i18next';
import DeleteIconSvg from '@components/Icons/delete';
import CreateModal from './CreateModal';
import HospitalSavedModal from './HospitalSavedModal';
import EditHospitalIconSvg from '@components/Icons/edit-hospital';

const LinkedList = ({ form, initialValue }) => {
  const [isOpenModalCreate, setIsOpenModalCreate] = useState(false);
  const [isOpenModalSaved, setIsOpenModalSaved] = useState(false);
  const [hospitals, setHospitals] = useState([...(initialValue ?? [])]);
  const { t } = useTranslation(['insurances']);

  useEffect(() => {
    const updateHospitals = hospitals.map(item => {
      if (typeof item.id === 'number' || item?.isPending) {
        const { name, address, district, province } = item;

        return { name, address, district, province };
      }
      return item;
    });
    form.setFieldValue('hospitals', updateHospitals);
  }, [hospitals, form]);

  const handleModalCreate = isOpen => setIsOpenModalCreate(isOpen);
  const handleModalSaved = isOpen => setIsOpenModalSaved(isOpen);

  const handleSubmitInModalSaved = records => {
    setHospitals(preHospitals => {
      const hospitalsPending = preHospitals.filter(item => item?.isPending);
      return [...hospitalsPending, ...records];
    });
    handleModalSaved(false);
  };
  const handleSubmitInModalCreate = record => {
    setHospitals(preHospitals => [
      { ...record, id: new Date().getTime(), isPending: true },
      ...preHospitals,
    ]);
    handleModalCreate(false);
  };
  const handleRemoveHospital = id => {
    const updateHospitals = [...hospitals].filter(item => item.id !== id);
    setHospitals(updateHospitals);
  };

  const columns = [
    {
      title: t('insurances:form:linkedList:hospital_name'),
      dataIndex: 'name',
    },
    {
      title: t('insurances:form:linkedList:hospital_address'),
      dataIndex: 'address',
    },
    {
      title: t('insurances:form:linkedList:hospital_province'),
      dataIndex: 'province',
    },
    {
      title: t('insurances:form:linkedList:hospital_district'),
      dataIndex: 'district',
    },
    {
      title: 'Action',
      render: (_, record) => (
        <div className='inline-flex w-full justify-center gap-2'>
          {typeof record.id === 'number' && (
            <Button type='ghost' icon={<EditHospitalIconSvg />} />
          )}
          <Button
            type='ghost'
            icon={<DeleteIconSvg />}
            onClick={() => handleRemoveHospital(record.id)}
          />
        </div>
      ),
    },
  ];
  const hospitalsSelected = initialValue
    ? hospitals.filter(item => item.id && !item?.isPending)
    : undefined;

  return (
    <>
      <div className='h-full border border-t-0 border-platinum bg-white px-4 py-8'>
        <Form.Item name={['hospitals']} className='hidden' />
        <div className='mb-10 flex items-center justify-between'>
          <p className='text-3.5xl font-bold leading-12 text-arsenic'>
            {t('insurances:form:linkedList:title')}
          </p>
          <div className='inline-flex gap-4'>
            <Button
              onClick={() => handleModalSaved(true)}
              className='h-full px-6 py-2 text-base font-bold text-nickel'
            >
              {t('insurances:form:linkedList:saved_list')}
            </Button>
            <Button
              onClick={() => handleModalCreate(true)}
              className='h-full px-6 py-2 text-base font-bold text-nickel'
            >
              {t('insurances:form:linkedList:created')}
            </Button>
          </div>
        </div>
        <Table
          rowKey={'id'}
          size='middle'
          bordered
          dataSource={hospitals}
          columns={columns}
          scroll={{ y: 350 }}
          pagination={{
            pageSize: 5,
            hideOnSinglePage: true,
          }}
          className='[&_th]:text-center [&_th]:text-nickel'
        />
      </div>
      <CreateModal
        isOpen={isOpenModalCreate}
        onCancel={() => handleModalCreate(false)}
        onSubmit={handleSubmitInModalCreate}
      />
      <HospitalSavedModal
        isOpen={isOpenModalSaved}
        hospitalsSelected={hospitalsSelected}
        onCancel={() => handleModalSaved(false)}
        onSubmit={handleSubmitInModalSaved}
      />
    </>
  );
};

export default LinkedList;
