import { Modal, Form, Input } from 'antd';
import React from 'react';
import { useTranslation } from 'next-i18next';
import CloseIconSvg from '@components/Icons/close';

const CreateModal = ({ isOpen, onCancel, onSubmit }) => {
  const { t } = useTranslation(['insurances']);
  const [form] = Form.useForm();

  const handleClickSubmit = () => form.submit();
  const handleClickCancel = () => {
    form.resetFields();
    onCancel();
  };
  const handleFinish = data => {
    form.resetFields();
    onSubmit(data);
  };

  return (
    <Modal
      centered
      footer={null}
      open={isOpen}
      width={800}
      closeIcon={<CloseIconSvg />}
      onCancel={handleClickCancel}
      destroyOnClose
    >
      <div className='p-3'>
        <div className='mb-8 inline-flex w-full justify-center'>
          <p className='text-arsenic text-2xl font-medium'>
            {t('insurances:form:linkedList:modal:create:title')}
          </p>
        </div>

        <Form
          form={form}
          layout='vertical'
          name='create_hospital'
          onFinish={handleFinish}
        >
          <Form.Item
            name='name'
            label={t('insurances:form:linkedList:modal:create:name')}
            rules={[
              {
                required: true,
                message: t('insurances:form:linkedList:rules:name'),
              },
            ]}
          >
            <Input
              size='large'
              placeholder={t('insurances:form:linkedList:modal:create:name')}
            />
          </Form.Item>

          <Form.Item
            name='address'
            label={t('insurances:form:linkedList:modal:create:address')}
            rules={[
              {
                required: true,
                message: t('insurances:form:linkedList:rules:address'),
              },
            ]}
          >
            <Input
              size='large'
              placeholder={t('insurances:form:linkedList:modal:create:address')}
            />
          </Form.Item>

          <div className='inline-flex w-full items-center justify-between'>
            <Form.Item
              name='province'
              label={t('insurances:form:linkedList:modal:create:province')}
              className='mr-6 w-1/2'
              rules={[
                {
                  required: true,
                  message: t('insurances:form:linkedList:rules:province'),
                },
              ]}
            >
              <Input
                size='large'
                placeholder={t(
                  'insurances:form:linkedList:modal:create:province'
                )}
              />
            </Form.Item>

            <Form.Item
              label={t('insurances:form:linkedList:modal:create:district')}
              name='district'
              className='w-1/2'
              rules={[
                {
                  required: true,
                  message: t('insurances:form:linkedList:rules:district'),
                },
              ]}
            >
              <Input
                size='large'
                placeholder={t(
                  'insurances:form:linkedList:modal:create:district'
                )}
              />
            </Form.Item>
          </div>
        </Form>
        <div className='flex items-center justify-end gap-x-6'>
          <button
            className='rounded-lg border border-nickel px-10 py-3 font-bold text-nickel'
            onClick={handleClickCancel}
          >
            {t('insurances:form:linkedList:modal:button:cancel')}
          </button>
          <button
            className='rounded-lg bg-ultramarine-blue px-10 py-3 font-bold text-cultured'
            onClick={handleClickSubmit}
          >
            {t('insurances:form:linkedList:modal:button:ok:create')}
          </button>
        </div>
      </div>
    </Modal>
  );
};

export default CreateModal;
