import FormLayout from '@components/FormInsurances/FormLayout';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { FIELDS_HEALTH_INSURANCE, INSURANCE_CATEGORY } from '@constants';
import FormItemHaveValueAndDescription from '../FormItemHaveValueAndDescription';

const Outpatient = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances']);

  return (
    <div className='border border-t-0 border-platinum bg-white px-4 py-8'>
      <FormLayout
        activeKey='outpatient'
        title={t('insurances:form:outpatient:title')}
        active={
          activeFields?.outpatient ??
          handleActiveField(FIELDS_HEALTH_INSURANCE.outpatient, 'outpatient')
        }
        category={INSURANCE_CATEGORY.HEALTH}
      >
        {FIELDS_HEALTH_INSURANCE.outpatient.map(field => (
          <FormItemHaveValueAndDescription
            key={field}
            parentField='outpatient'
            childField={field}
          />
        ))}
      </FormLayout>
    </div>
  );
};

export default Outpatient;
