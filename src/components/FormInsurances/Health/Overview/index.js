import React from 'react';
import FormDetails from '@components/FormInsurances/FormDetails';
import { FIELDS_HEALTH_INSURANCE, INSURANCE_CATEGORY } from '@constants';
import FormLayout from '@components/FormInsurances/FormLayout';
import { useTranslation } from 'next-i18next';
import FormTerms from '@components/FormInsurances/FormTerms';

const Overview = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances']);

  return (
    <div className='border border-t-0 border-platinum bg-white px-4 py-8'>
      <FormLayout
        activeKey='details'
        title={t('insurances:form:details:title')}
        active={
          activeFields?.details ??
          handleActiveField(FIELDS_HEALTH_INSURANCE.details)
        }
        category={INSURANCE_CATEGORY.HEALTH}
      >
        <FormDetails />
      </FormLayout>
      <FormLayout
        activeKey='terms'
        title={t('insurances:form:terms:title')}
        active={
          activeFields?.terms ??
          handleActiveField(FIELDS_HEALTH_INSURANCE.terms, 'terms')
        }
        category={INSURANCE_CATEGORY.HEALTH}
      >
        <FormTerms insuranceCategory={INSURANCE_CATEGORY.HEALTH} />
      </FormLayout>
    </div>
  );
};

export default Overview;
