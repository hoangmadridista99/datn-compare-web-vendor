import FormFieldHaveLevel from '@components/FormInsurances/FormFieldHaveLevel';
import FormLayout from '@components/FormInsurances/FormLayout';
import React from 'react';
import { Form, Input, Select, InputNumber, Checkbox } from 'antd';
import { useTranslation } from 'next-i18next';
import { formatCurrency } from '@helpers/formatCurrency';
import {
  FIELDS_HEALTH_INSURANCE,
  INSURANCE_CATEGORY,
  LIST_HOSPITALIZATION,
  LIST_ROOM_TYPES,
} from '@constants';
import FormItemHaveValueAndDescription from '../FormItemHaveValueAndDescription';

const { TextArea } = Input;

const Inpatient = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances']);

  const handleOptions = list =>
    list.map(item => ({
      value: item,
      label: t(`insurances:form:options:${item}`),
    }));

  const handleFormatter = value => {
    if (isNaN(value)) return undefined;

    return value ? formatCurrency(value) : '';
  };

  const handleParser = value => {
    const formatValue = parseInt(value.replace(/\./g, ''));

    if (isNaN(formatValue)) return;

    return parseInt(value.replace(/\./g, ''));
  };

  return (
    <div className='border border-t-0 border-platinum bg-white px-4 py-8'>
      <FormLayout
        activeKey='inpatient'
        title={t('insurances:form:inpatient:title')}
        active={
          activeFields?.inpatient ??
          handleActiveField(FIELDS_HEALTH_INSURANCE.inpatient, 'inpatient')
        }
        category={INSURANCE_CATEGORY.HEALTH}
      >
        {/* room_type */}
        <FormFieldHaveLevel
          title={t('insurances:form:inpatient:room_type')}
          fieldDetail={t('insurances:fields:room_type')}
          name={['inpatient', 'room_type']}
        >
          <div className='flex w-full items-center'>
            <Form.Item
              name={['inpatient', 'room_type', 'values']}
              className='mx-2 mb-0 w-full'
            >
              <Checkbox.Group className='flex flex-col'>
                {handleOptions(LIST_ROOM_TYPES).map(item => (
                  <Checkbox
                    key={item.value}
                    className='mx-0 mb-6 last:mb-0'
                    value={item.value}
                  >
                    {item.label}
                  </Checkbox>
                ))}
              </Checkbox.Group>
            </Form.Item>
          </div>
          <div className='mt-4 flex'>
            <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
              {t('insurances:form:description')}
            </p>
            <Form.Item
              name={['inpatient', 'room_type', 'description']}
              className='mx-2 w-full'
            >
              <TextArea
                placeholder={t('insurances:form:placeholder:detail')}
                autoSize={{ minRows: 5, maxRows: 7 }}
              />
            </Form.Item>
          </div>
        </FormFieldHaveLevel>

        {/* for_cancer */}
        <FormItemHaveValueAndDescription
          parentField='inpatient'
          childField='for_cancer'
        />

        {/* for_illnesses */}
        <FormItemHaveValueAndDescription
          parentField='inpatient'
          childField='for_illnesses'
        />

        {/* for_accidents */}
        <FormItemHaveValueAndDescription
          parentField='inpatient'
          childField='for_accidents'
        />

        {/* for_surgical */}
        <FormItemHaveValueAndDescription
          parentField='inpatient'
          childField='for_surgical'
        />

        {/* for_hospitalization */}
        <FormFieldHaveLevel
          title={t('insurances:form:inpatient:for_hospitalization')}
          fieldDetail={t('insurances:fields:for_hospitalization')}
          name={['inpatient', 'for_hospitalization']}
        >
          <Form.Item
            name={['inpatient', 'for_hospitalization', 'type']}
            className='mx-2 w-full'
          >
            <Select
              placeholder={t('insurances:form:placeholder:select')}
              className='w-full'
              options={handleOptions(LIST_HOSPITALIZATION)}
            />
          </Form.Item>

          <div className='mb-6 flex w-full items-center'>
            <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
              {t('insurances:form:money')}
            </p>
            <Form.Item
              name={['inpatient', 'for_hospitalization', 'value']}
              className='mx-2 mb-0 w-full'
            >
              <InputNumber
                className='w-full'
                controls={false}
                placeholder={t('insurances:form:placeholder:money')}
                formatter={handleFormatter}
                parser={handleParser}
              />
            </Form.Item>
          </div>
        </FormFieldHaveLevel>

        {/* for_intensive_care */}
        <FormItemHaveValueAndDescription
          parentField='inpatient'
          childField='for_intensive_care'
        />

        {/* for_administrative */}
        <FormItemHaveValueAndDescription
          parentField='inpatient'
          childField='for_administrative'
        />

        {/* for_organ_transplant */}
        <FormItemHaveValueAndDescription
          parentField='inpatient'
          childField='for_organ_transplant'
        />
      </FormLayout>
    </div>
  );
};

export default Inpatient;
