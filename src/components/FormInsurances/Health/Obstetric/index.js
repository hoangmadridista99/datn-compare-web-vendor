import FormLayout from '@components/FormInsurances/FormLayout';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { FIELDS_HEALTH_INSURANCE, INSURANCE_CATEGORY } from '@constants';
import FormItemHaveValueAndDescription from '../FormItemHaveValueAndDescription';

const Obstetric = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances']);
  return (
    <FormLayout
      activeKey='obstetric'
      title={t('insurances:form:obstetric:title')}
      active={
        activeFields?.obstetric ??
        handleActiveField(FIELDS_HEALTH_INSURANCE.obstetric, 'obstetric')
      }
      category={INSURANCE_CATEGORY.HEALTH}
    >
      {FIELDS_HEALTH_INSURANCE.obstetric.map(field => (
        <FormItemHaveValueAndDescription
          key={field}
          parentField='obstetric'
          childField={field}
        />
      ))}
    </FormLayout>
  );
};

export default Obstetric;
