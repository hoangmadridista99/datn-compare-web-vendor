import FormField from '@components/FormInsurances/FormField';
import FormLayout from '@components/FormInsurances/FormLayout';
import { Checkbox, Form } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'next-i18next';
import Dental from '../Dental';
import { HEALTH_INSURANCE_ADDITIONAL_BENEFITS } from '@constants';
import Obstetric from '../Obstetric';

const AdditionalBenefits = ({ additionalBenefit, ...props }) => {
  const [benefitsSelected, setBenefitsSelected] = useState([
    ...(additionalBenefit ?? []),
  ]);
  const { t } = useTranslation(['insurances']);

  const handleChangeBenefits = values => setBenefitsSelected(values);

  return (
    <div className='border border-t-0 border-platinum bg-white px-4 py-8'>
      <FormLayout title={t('insurances:form:additional_benefits:title')}>
        <FormField
          title={t('insurances:form:additional_benefits:existed_services')}
        >
          <Form.Item name={['additional_benefit']}>
            <Checkbox.Group className='w-full' onChange={handleChangeBenefits}>
              <Checkbox value={HEALTH_INSURANCE_ADDITIONAL_BENEFITS.DENTAL}>
                {t('insurances:form:dental:title')}
              </Checkbox>

              <Checkbox value={HEALTH_INSURANCE_ADDITIONAL_BENEFITS.OBSTETRIC}>
                {t('insurances:form:obstetric:title')}
              </Checkbox>
            </Checkbox.Group>
          </Form.Item>
        </FormField>
      </FormLayout>
      {benefitsSelected.includes(
        HEALTH_INSURANCE_ADDITIONAL_BENEFITS.DENTAL
      ) && <Dental {...props} />}
      {benefitsSelected.includes(
        HEALTH_INSURANCE_ADDITIONAL_BENEFITS.OBSTETRIC
      ) && <Obstetric {...props} />}
    </div>
  );
};

export default AdditionalBenefits;
