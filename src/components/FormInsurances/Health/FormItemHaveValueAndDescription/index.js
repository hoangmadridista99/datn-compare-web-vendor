import FormFieldHaveLevel from '@components/FormInsurances/FormFieldHaveLevel';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { Form, InputNumber, Input } from 'antd';
import { formatCurrency } from '@helpers/formatCurrency';
import { useInputNumber } from '@hooks/useInputNumber';

const { TextArea } = Input;

const FormItemHaveValueAndDescription = ({ parentField, childField }) => {
  const { t } = useTranslation(['insurances']);
  const { errors, message, onKeyDown } = useInputNumber();

  const handleFormatter = value => {
    if (isNaN(value)) return undefined;

    return value ? formatCurrency(value) : '';
  };

  const handleParser = value => {
    const formatValue = parseInt(value.replace(/\./g, ''));

    if (isNaN(formatValue)) return;

    return parseInt(value.replace(/\./g, ''));
  };
  return (
    <FormFieldHaveLevel
      title={t(`insurances:form:${parentField}:${childField}`)}
      fieldDetail={t(`insurances:fields:${childField}`)}
      name={[parentField, childField]}
    >
      <div className='flex w-full items-center'>
        <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
          {t('insurances:form:money')}
        </p>
        <Form.Item
          name={[parentField, childField, 'value']}
          className='mx-2 mb-0 w-full'
          validateStatus={
            errors[`${parentField}.${childField}`] ? 'error' : undefined
          }
        >
          <InputNumber
            className='input_addon w-full'
            controls={false}
            placeholder={t('insurances:form:placeholder:money')}
            formatter={handleFormatter}
            parser={handleParser}
            addonAfter='VNĐ'
            onKeyDown={event =>
              onKeyDown(event, `${parentField}.${childField}`)
            }
          />
        </Form.Item>
      </div>
      {errors[`${parentField}.${childField}`] && (
        <p className='ml-1/5 text-sm text-venetian-red'>{message}</p>
      )}
      <div className='mt-4 flex'>
        <p className='w-1/5 pr-2 text-right text-base font-bold leading-6 text-arsenic'>
          {t('insurances:form:description')}
        </p>
        <Form.Item
          name={[parentField, childField, 'description']}
          className='mx-2 w-full'
        >
          <TextArea
            placeholder={t('insurances:form:placeholder:detail')}
            autoSize={{ minRows: 5, maxRows: 7 }}
          />
        </Form.Item>
      </div>
    </FormFieldHaveLevel>
  );
};

export default FormItemHaveValueAndDescription;
