import FormLayout from '@components/FormInsurances/FormLayout';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { FIELDS_HEALTH_INSURANCE, INSURANCE_CATEGORY } from '@constants';
import FormItemHaveValueAndDescription from '../FormItemHaveValueAndDescription';

const Dental = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances']);

  return (
    <FormLayout
      activeKey='dental'
      title={t('insurances:form:dental:title')}
      active={
        activeFields?.dental ??
        handleActiveField(FIELDS_HEALTH_INSURANCE.dental, 'dental')
      }
      category={INSURANCE_CATEGORY.HEALTH}
    >
      {FIELDS_HEALTH_INSURANCE.dental.map(field => (
        <FormItemHaveValueAndDescription
          key={field}
          parentField='dental'
          childField={field}
        />
      ))}
    </FormLayout>
  );
};

export default Dental;
