import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Tabs, Form } from 'antd';
import Overview from './Overview';
import Inpatient from './Inpatient';
import Outpatient from './Outpatient';
import CustomerOrientation from './CustomerOrientation';
import AdditionalBenefits from './AdditionalBenefits';
import LinkedList from './LinkedList';
import { useTranslation } from 'next-i18next';
import {
  FIELDS_HEALTH_INSURANCE,
  FIELDS_HEALTH_INSURANCE_DEFAULT,
  FORM_HEALTH_INSURANCE_DEFAULT,
} from '@constants';

const FormHealthInsurance = props => {
  const { form, onFinishForm, isDisabled, initialValues } = props;

  const [fields, setFields] = useState([]);
  const [activeFields, setActiveFields] = useState(null);
  const debounceId = useRef(null);
  const { t } = useTranslation(['insurances']);

  const handleGetActiveFieldsByInitialValues = useCallback(obj => {
    let result = [];
    const keys = Object.keys(obj);

    for (const key of keys) {
      const item = obj[key];

      if (item?.level && item?.level !== 'none') {
        result = [...result, key];
      }
    }

    return result;
  }, []);

  useEffect(() => {
    if (initialValues) {
      const listKeys = Object.keys(FIELDS_HEALTH_INSURANCE_DEFAULT).reduce(
        (result, key) => {
          if (!initialValues[key]) return result;

          return {
            ...result,
            [key]: handleGetActiveFieldsByInitialValues(initialValues[key]),
          };
        },
        {}
      );
      const result = {
        ...listKeys,
        details: FIELDS_HEALTH_INSURANCE.details,
      };
      setActiveFields(result);
    }
  }, [initialValues, handleGetActiveFieldsByInitialValues]);

  const handleActiveField = useCallback(
    (list, child) => {
      if (child)
        return list.reduce((result, field) => {
          const isField = fields.find(
            item =>
              item.name.length >= 3 &&
              item.name[0] === child &&
              item.name[1] === field &&
              item.name[2] === 'level' &&
              item.value !== 'none'
          );
          if (!isField) return result;
          return [...result, field];
        }, []);

      return list.reduce((result, field) => {
        const isField = fields.find(
          item => item.name[0] === field && !item.errors.length && item.value
        );
        if (!isField) return result;
        return [...result, field];
      }, []);
    },
    [fields]
  );
  const handleFieldsChange = (_, allFields) => {
    clearTimeout(debounceId?.current);
    debounceId.current = setTimeout(() => {
      setActiveFields(null);
      setFields(allFields);
    }, 500);
  };

  const itemsTab = [
    {
      label: t('insurances:form:overview'),
      key: 'overview',
      children: (
        <Overview
          activeFields={activeFields}
          handleActiveField={handleActiveField}
        />
      ),
    },
    {
      label: t('insurances:form:inpatient:title'),
      key: 'inpatient',
      children: (
        <Inpatient
          activeFields={activeFields}
          handleActiveField={handleActiveField}
        />
      ),
    },
    {
      label: t('insurances:form:outpatient:title'),
      key: 'outpatient',
      children: (
        <Outpatient
          activeFields={activeFields}
          handleActiveField={handleActiveField}
        />
      ),
    },
    {
      label: t('insurances:form:additional_benefits:title'),
      key: 'additional-benefits',
      children: (
        <AdditionalBenefits
          activeFields={activeFields}
          handleActiveField={handleActiveField}
          additionalBenefit={
            initialValues ? initialValues.additional_benefit : undefined
          }
        />
      ),
    },
    {
      label: t('insurances:form:linkedList:title'),
      key: 'linked-list',
      children: (
        <LinkedList
          form={form}
          initialValue={initialValues ? initialValues?.hospitals : undefined}
        />
      ),
    },
    {
      label: t('insurances:form:customer_orientation:title'),
      key: 'customer-orientation',
      children: (
        <CustomerOrientation
          activeFields={activeFields}
          handleActiveField={handleActiveField}
        />
      ),
    },
  ];

  return (
    <Form
      form={form}
      onFinish={onFinishForm}
      onFieldsChange={handleFieldsChange}
      initialValues={initialValues ?? FORM_HEALTH_INSURANCE_DEFAULT}
      disabled={isDisabled}
    >
      <Tabs
        className='tabs-setting [&>div]:mb-0'
        defaultActiveKey='overview'
        type='card'
        items={itemsTab}
      />
    </Form>
  );
};

export default FormHealthInsurance;
