import FormFieldHaveLevel from '@components/FormInsurances/FormFieldHaveLevel';
import FormLayout from '@components/FormInsurances/FormLayout';
import React from 'react';
import { Form, Input } from 'antd';
import { useTranslation } from 'next-i18next';
import { FIELDS_LIFE_INSURANCE, INSURANCE_CATEGORY } from '@constants';

const { TextArea } = Input;

const CustomerOrientation = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances']);

  return (
    <div className='border border-t-0 border-platinum bg-white px-4 py-8'>
      <FormLayout
        activeKey='customer_orientation'
        title={t('insurances:form:customer_orientation:title')}
        active={
          activeFields?.customer_orientation ??
          handleActiveField(
            FIELDS_LIFE_INSURANCE.customer_orientation,
            'customer_orientation'
          )
        }
        category={INSURANCE_CATEGORY.LIFE}
      >
        {FIELDS_LIFE_INSURANCE.customer_orientation.map(item => (
          <FormFieldHaveLevel
            key={item}
            title={t(`insurances:form:customer_orientation:${item}`)}
            name={['customer_orientation', item]}
            fieldDetail={t(`insurances:fields:${item}`)}
          >
            <Form.Item name={['customer_orientation', item, 'text']}>
              <TextArea
                placeholder={t('insurances:form:placeholder:detail')}
                autoSize={{ minRows: 5, maxRows: 7 }}
              />
            </Form.Item>
          </FormFieldHaveLevel>
        ))}
      </FormLayout>
    </div>
  );
};

export default CustomerOrientation;
