import FormLayout from '@components/FormInsurances/FormLayout';
import { Form, Input } from 'antd';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { FIELDS_LIFE_INSURANCE, INSURANCE_CATEGORY } from '@constants';
import FormFieldHaveLevel from '@components/FormInsurances/FormFieldHaveLevel';

const { TextArea } = Input;

const AdditionalBenefits = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances']);

  return (
    <div className='border border-t-0 border-platinum bg-white px-4 py-8'>
      <FormLayout
        activeKey='additional_benefits'
        title={t('insurances:form:additional_benefits:title')}
        active={
          activeFields?.additional_benefits ??
          handleActiveField(
            FIELDS_LIFE_INSURANCE.additional_benefits,
            'additional_benefits'
          )
        }
        category={INSURANCE_CATEGORY.LIFE}
      >
        {FIELDS_LIFE_INSURANCE.additional_benefits.map(item => (
          <FormFieldHaveLevel
            key={item}
            title={t(`insurances:form:additional_benefits:${item}`)}
            name={['additional_benefits', item]}
            fieldDetail={
              item !== 'flexible_and_diverse'
                ? t(`insurances:fields:${item}`)
                : undefined
            }
          >
            <Form.Item name={['additional_benefits', item, 'text']}>
              <TextArea
                placeholder={t('insurances:form:placeholder:detail')}
                autoSize={{ minRows: 5, maxRows: 7 }}
              />
            </Form.Item>
          </FormFieldHaveLevel>
        ))}
      </FormLayout>
    </div>
  );
};

export default AdditionalBenefits;
