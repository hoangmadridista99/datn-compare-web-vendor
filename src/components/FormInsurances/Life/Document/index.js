/* eslint-disable indent */
import React, { memo } from 'react';
import { Upload, Form } from 'antd';
import { useTranslation } from 'next-i18next';

import { uploadPdf } from '@services/upload';
import { handleError } from '@helpers/handleError';
import FormLayout from '@components/FormInsurances/FormLayout';
import { FIELDS_LIFE_INSURANCE, INSURANCE_CATEGORY } from '@constants';
import FormField from '@components/FormInsurances/FormField';

const Document = ({ activeFields, handleActiveField, form }) => {
  const { t } = useTranslation(['insurances']);

  const actionUpload = async file => {
    try {
      const body = new FormData();
      body.append('file', file);
      const data = await uploadPdf(body);
      return data;
    } catch (error) {
      handleError(error);
    }
  };

  return (
    <div className='border border-t-0 border-platinum bg-white px-4 py-8'>
      <FormLayout
        activeKey='document'
        title={t('insurances:form:document:title')}
        active={
          activeFields?.document ??
          handleActiveField(FIELDS_LIFE_INSURANCE.document)
        }
        category={INSURANCE_CATEGORY.LIFE}
      >
        <FormField
          title={t('insurances:form:document:benefits_illustration_table')}
        >
          <Form.Item name='benefits_illustration_table'>
            <Upload
              defaultFileList={
                form ? form.getFieldValue('benefits_illustration_table') : []
              }
              maxCount={1}
              accept='.pdf'
              action={actionUpload}
              method='GET'
            >
              <button
                className='rounded-lg bg-ultramarine-blue px-6 py-2 font-bold text-cultured'
                type='button'
              >
                Upload file
              </button>
            </Upload>
          </Form.Item>
        </FormField>
        <FormField title={t('insurances:form:document:documentation_url')}>
          <Form.Item name='documentation_url'>
            <Upload
              defaultFileList={
                form ? form.getFieldValue('documentation_url') : []
              }
              maxCount={1}
              accept='.pdf'
              action={actionUpload}
              method='GET'
            >
              <button
                className='rounded-lg bg-ultramarine-blue px-6 py-2 font-bold text-cultured'
                type='button'
              >
                Upload file
              </button>
            </Upload>
          </Form.Item>
        </FormField>
      </FormLayout>
    </div>
  );
};

export default memo(Document);
