import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Tabs, Form } from 'antd';
import { useTranslation } from 'next-i18next';
import { FIELDS_LIFE_INSURANCE, FORM_LIFE_INSURANCE_DEFAULT } from '@constants';
import Overview from './Overview';
import Benefits from './Benefits';
import AdditionalBenefits from './AdditionalBenefits';
import CustomerOrientation from './CustomerOrientation';
import Document from './Document';

const FormLifeInsurance = props => {
  const { form, onFinishForm, isDisabled, objectives, initialValues } = props;
  const [fields, setFields] = useState([]);
  const [activeFields, setActiveFields] = useState(null);
  const debounceId = useRef(null);
  const { t } = useTranslation(['insurances']);

  const handleGetActiveFieldsByInitialValues = useCallback(obj => {
    let result = [];
    const keys = Object.keys(obj);

    for (const key of keys) {
      const item = obj[key];

      if (item?.level && item?.level !== 'none') {
        result = [...result, key];
      }
    }

    return result;
  }, []);

  useEffect(() => {
    if (initialValues) {
      const { documentation_url, benefits_illustration_table } = initialValues;

      const terms = handleGetActiveFieldsByInitialValues(initialValues.terms);
      const additional_benefits = handleGetActiveFieldsByInitialValues(
        initialValues.additional_benefits
      );
      const customer_orientation = handleGetActiveFieldsByInitialValues(
        initialValues.customer_orientation
      );
      let document = [];

      if (documentation_url) {
        document = [...document, 'documentation_url'];
      }
      if (benefits_illustration_table) {
        document = [...document, 'benefits_illustration_table'];
      }

      const updateActiveKeys = {
        details: FIELDS_LIFE_INSURANCE.details,
        benefits: FIELDS_LIFE_INSURANCE.benefits,
        terms,
        additional_benefits,
        customer_orientation,
        document,
      };
      setActiveFields(updateActiveKeys);
    }
  }, [initialValues, handleGetActiveFieldsByInitialValues]);

  const handleActiveField = useCallback(
    (list, child) => {
      if (child)
        return list.reduce((result, field) => {
          const isField = fields.find(
            item =>
              item.name.length >= 3 &&
              item.name[0] === child &&
              item.name[1] === field &&
              item.name[2] === 'level' &&
              item.value !== 'none'
          );
          if (!isField) return result;
          return [...result, field];
        }, []);

      return list.reduce((result, field) => {
        const isFieldActive = fields.some(item => {
          if (FIELDS_LIFE_INSURANCE.document.includes(field)) {
            const isValid =
              item.name[0] === field &&
              item.value &&
              (item?.value?.length > 0 || item.value?.fileList?.length > 0);
            return isValid;
          }

          return item.name[0] === field && !item.errors.length && item.value;
        });

        if (!isFieldActive) return result;
        return [...result, field];
      }, []);
    },
    [fields]
  );
  const handleFieldsChange = (_, allFields) => {
    clearTimeout(debounceId?.current);
    debounceId.current = setTimeout(() => {
      setActiveFields(null);
      setFields(allFields);
    }, 500);
  };

  const itemsTab = [
    {
      label: t('insurances:form:overview'),
      key: 'overview',
      children: (
        <Overview
          activeFields={activeFields}
          handleActiveField={handleActiveField}
          objectives={objectives}
        />
      ),
    },
    {
      label: t('insurances:form:key_benefits'),
      key: 'benefits',
      children: <Benefits />,
    },
    {
      label: t('insurances:form:additional_benefits:title'),
      key: 'additional_benefits',
      children: (
        <AdditionalBenefits
          activeFields={activeFields}
          handleActiveField={handleActiveField}
        />
      ),
    },
    {
      label: t('insurances:form:customer_orientation:title'),
      key: 'customer-orientation',
      children: (
        <CustomerOrientation
          activeFields={activeFields}
          handleActiveField={handleActiveField}
        />
      ),
    },
    {
      label: t('insurances:form:document:title'),
      key: 'document',
      children: (
        <Document
          activeFields={activeFields}
          handleActiveField={handleActiveField}
          form={initialValues ? form : undefined}
        />
      ),
    },
  ];

  return (
    <Form
      form={form}
      onFinish={onFinishForm}
      onFieldsChange={handleFieldsChange}
      initialValues={initialValues ?? FORM_LIFE_INSURANCE_DEFAULT}
      disabled={isDisabled}
    >
      <Tabs
        className='tabs-setting [&>div]:mb-0'
        defaultActiveKey='overview'
        type='card'
        items={itemsTab}
      />
    </Form>
  );
};

export default FormLifeInsurance;
