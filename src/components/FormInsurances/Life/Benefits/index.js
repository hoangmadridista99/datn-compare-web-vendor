import React from 'react';
import { INSURANCE_CATEGORY } from '@constants';
import FormLayout from '@components/FormInsurances/FormLayout';
import { useTranslation } from 'next-i18next';
import { Form, Input } from 'antd';

const { TextArea } = Input;

const Benefits = () => {
  const { t } = useTranslation(['insurances']);

  return (
    <div className='border border-t-0 border-platinum bg-white px-4 py-8'>
      <FormLayout
        title={t('insurances:form:key_benefits')}
        category={INSURANCE_CATEGORY.LIFE}
      >
        <Form.Item
          name='key_benefits'
          rules={[
            {
              required: true,
              message: t('insurances:form:validation:benefit'),
            },
          ]}
          className='m-0'
        >
          <TextArea
            placeholder={t('insurances:form:placeholder:describe')}
            autoSize={{ minRows: 5, maxRows: 7 }}
          />
        </Form.Item>
      </FormLayout>
    </div>
  );
};

export default Benefits;
