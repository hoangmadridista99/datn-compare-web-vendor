import React, { memo } from 'react';
import { Form, Checkbox, Input } from 'antd';
import { useTranslation } from 'next-i18next';

import FormField from '../FormField';

const { TextArea } = Input;

const FormInsuranceDetail = ({ objectives }) => {
  const { t } = useTranslation(['insurances']);

  return (
    <>
      {/* name */}
      <FormField title={t('insurances:form:details:name')} isHiddenFieldDetails>
        <Form.Item
          name='name'
          rules={[
            { required: true, message: t('insurances:form:validation:name') },
          ]}
        >
          <Input placeholder={t('insurances:form:placeholder:name')} />
        </Form.Item>
      </FormField>

      {/* description_insurance */}
      <FormField
        title={t('insurances:form:details:description_insurance')}
        isHiddenFieldDetails
      >
        <Form.Item
          name='description_insurance'
          rules={[
            {
              required: true,
              message: t('insurances:form:validation:description_insurance'),
            },
          ]}
        >
          <TextArea
            placeholder={t('insurances:form:placeholder:describe')}
            autoSize={{ minRows: 5, maxRows: 7 }}
          />
        </Form.Item>
      </FormField>

      {/* objective_of_insurance */}
      {objectives && (
        <FormField
          title={t('insurances:form:details:objective_of_insurance')}
          isHiddenFieldDetails
        >
          <Form.Item
            name='objective_of_insurance'
            rules={[
              {
                required: true,
                message: t('insurances:form:validation:objective_of_insurance'),
              },
            ]}
          >
            <Checkbox.Group className='flex flex-col gap-6'>
              {objectives.map(item => (
                <Checkbox className='m-0' key={item.id} value={item.id}>
                  {t(`insurances:form:objectives:${item.objective_type}`)}
                </Checkbox>
              ))}
            </Checkbox.Group>
          </Form.Item>
        </FormField>
      )}
    </>
  );
};

export default memo(FormInsuranceDetail);
