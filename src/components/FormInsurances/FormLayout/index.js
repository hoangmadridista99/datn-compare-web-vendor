import React, { memo, useMemo } from 'react';
import Image from 'next/image';
import classNames from 'classnames';
import { Collapse, Popover } from 'antd';
import { useTranslation } from 'next-i18next';

import {
  FIELDS_HEALTH_INSURANCE,
  FIELDS_LIFE_INSURANCE,
  INSURANCE_CATEGORY,
} from '@constants';

const FormLayout = ({ children, ...props }) => {
  const { active, title, activeKey, category } = props;

  const ListFields = useMemo(() => {
    switch (category) {
      case INSURANCE_CATEGORY.HEALTH:
        return FIELDS_HEALTH_INSURANCE;
      case INSURANCE_CATEGORY.LIFE:
        return FIELDS_LIFE_INSURANCE;
      default:
        return [];
    }
  }, [category]);

  const { t } = useTranslation(['insurances']);

  const ContentPopover = () =>
    !active || !activeKey ? null : (
      <>
        {ListFields[activeKey].map(item => {
          const isActive = active.indexOf(item) !== -1;

          return (
            <div key={item} className='mb-2.5 flex items-center last:mb-0'>
              <Image
                width={24}
                height={24}
                alt='Check'
                src={`/svg/${isActive ? 'check-success' : 'info-circle'}.svg`}
              />
              <span
                className={classNames('ml-2.5', {
                  'text-salem': isActive,
                  'text-royal-orange': !isActive,
                })}
              >
                {t(
                  `insurances:form${
                    activeKey !== 'benefits' ? `:${activeKey}` : ''
                  }:${item}`
                )}
              </span>
            </div>
          );
        })}
      </>
    );
  const Header = () => (
    <div className='flex items-center'>
      <h3 className='leading-5xl text-3.5xl font-bold text-arsenic'>{title}</h3>
      {active && activeKey && (
        <Popover placement='right' content={<ContentPopover />}>
          <div
            className={`ant-popover-open ml-4 flex items-center justify-center rounded bg-antique-white px-3 py-1 ${
              active.length === ListFields[activeKey]?.length
                ? 'bg-green-100'
                : 'bg-antique-white'
            }`}
          >
            <p
              className={`mb-0 mr-2 ${
                active.length === ListFields[activeKey]?.length
                  ? 'text-green-700'
                  : 'text-royal-orange'
              }`}
            >{`${active?.length}/${ListFields[activeKey]?.length}`}</p>
            <Image
              width={24}
              height={24}
              alt='Popover'
              src={`/svg/${
                active.length === ListFields[activeKey]?.length
                  ? 'check-success'
                  : 'info-circle'
              }.svg`}
            />
          </div>
        </Popover>
      )}
    </div>
  );
  const Icon = ({ isActive }) => (
    <Image
      width={24}
      height={24}
      alt='Arrow Down'
      src='/svg/arrow-down-1.svg'
      className={classNames('transition duration-200', {
        'rotate-90': !isActive,
      })}
    />
  );

  return (
    <Collapse
      defaultActiveKey={`form-${activeKey}`}
      size='large'
      expandIconPosition='end'
      expandIcon={Icon}
      className='form-insurance mb-10 overflow-hidden last:m-0'
    >
      <Collapse.Panel
        className='bg-white'
        key={`form-${activeKey}`}
        header={<Header />}
      >
        {children}
      </Collapse.Panel>
    </Collapse>
  );
};

export default memo(FormLayout);
