import React from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';
import { ROUTERS } from '@constants';

const Sidebar = () => {
  const router = useRouter();
  const { t } = useTranslation(['common']);

  return (
    <aside className='fixed left-0 top-0 h-screen w-64 overflow-hidden overflow-y-auto bg-white py-6 shadow'>
      <Link href='/' className='mx-6 inline-block'>
        <Image src='/img/logo.png' alt='Sosanh24' width={125} height={32} />
      </Link>
      <div className='mt-9 flex h-aside flex-col pb-3'>
        {ROUTERS.map(item => (
          <Link
            key={item.label}
            href={item.path}
            className='relative mb-6 flex items-center px-6 py-3 after:absolute after:right-0 after:top-0 after:hidden after:h-full after:w-1 after:rounded-s-3.5 after:bg-azure last:mb-0 aria-checked:bg-lavender-web aria-checked:after:block'
            aria-checked={router.pathname === item.path}
          >
            {item.icon(router.pathname === item.path)}
            <span
              className='ml-2 text-nickel aria-selected:text-azure'
              aria-selected={router.pathname === item.path}
            >
              {t(`common:sidebar:${item.label}`)}
            </span>
          </Link>
        ))}
      </div>
    </aside>
  );
};

export default Sidebar;
