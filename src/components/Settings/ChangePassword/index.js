import React, { useState } from 'react';
import { Form, Input, Spin, notification } from 'antd';
import { changePassword } from '@services/auth';
import { useTranslation } from 'next-i18next';
import { handleError } from '@helpers/handleError';

const { useForm } = Form;

const ChangePassword = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [form] = useForm();
  const { t } = useTranslation(['settings', 'errors']);

  const handleFinishForm = async values => {
    try {
      const { current_password, new_password } = values;
      if (current_password === new_password)
        return notification.warning({
          message: t('settings:changePassword:rules:same_password'),
        });
      setIsLoading(true);
      await changePassword(values);
      form.resetFields();
      notification.success({ message: t('settings:responses:changePassword') });
    } catch (error) {
      handleError(t, error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className='border border-t-0 border-platinum bg-white p-8'>
      <Form disabled={isLoading} form={form} onFinish={handleFinishForm}>
        <div className='mb-6 last:mb-0'>
          <p className='mb-2 text-base font-bold text-arsenic'>
            {t('settings:changePassword:form:currentPassword')}
          </p>
          <Form.Item
            name='current_password'
            rules={[
              {
                required: true,
                message: t('settings:changePassword:rules:currentPassword'),
              },
              {
                min: 8,
                message: t('settings:changePassword:rules:min'),
              },
              {
                max: 30,
                message: t('settings:changePassword:rules:max'),
              },
            ]}
          >
            <Input.Password visibilityToggle={false} />
          </Form.Item>
        </div>
        <div className='mb-6 last:mb-0'>
          <p className='mb-2 text-base font-bold text-arsenic'>
            {t('settings:changePassword:form:newPassword')}
          </p>
          <Form.Item
            name='new_password'
            rules={[
              {
                required: true,
                message: t('settings:changePassword:rules:newPassword'),
              },
              {
                min: 8,
                message: t('settings:changePassword:rules:min'),
              },
              {
                max: 30,
                message: t('settings:changePassword:rules:max'),
              },
            ]}
          >
            <Input.Password visibilityToggle={false} />
          </Form.Item>
        </div>
        <div className='mb-6 last:mb-0'>
          <p className='mb-2 text-base font-bold text-arsenic'>
            {t('settings:changePassword:form:confirmationPassword')}
          </p>
          <Form.Item
            name='confirmation_password'
            dependencies={['new_password']}
            rules={[
              {
                required: true,
                message: t(
                  'settings:changePassword:rules:confirmationPassword'
                ),
              },
              {
                min: 8,
                message: t('settings:changePassword:rules:min'),
              },
              {
                max: 30,
                message: t('settings:changePassword:rules:max'),
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('new_password') === value) {
                    return Promise.resolve();
                  }
                  if (value && value.length >= 8 && value.length <= 30)
                    return Promise.reject(
                      new Error(t('settings:changePassword:rules:not_match'))
                    );
                  return Promise.reject();
                },
              }),
            ]}
          >
            <Input.Password visibilityToggle={false} />
          </Form.Item>
        </div>
        <button
          type='submit'
          className='rounded-lg bg-ultramarine-blue px-10 py-3 font-bold text-cultured'
          disabled={isLoading}
        >
          {isLoading ? <Spin /> : t('settings:button:saved')}
        </button>
      </Form>
    </div>
  );
};

export default ChangePassword;
