/* eslint-disable indent */
import React, { useState, useRef, useMemo, useEffect } from 'react';
import { Form, Radio, Input, DatePicker, Spin, notification } from 'antd';
import Image from 'next/image';
import CameraIconSvg from '@components/Icons/camera';
import Edit2IconSvg from '@components/Icons/edit2';
import { useSession } from 'next-auth/react';
import dayjs from 'dayjs';
import { handleError } from '@helpers/handleError';
import { updateUser } from '@services/auth';
import { useTranslation } from 'next-i18next';

const { useForm } = Form;

const MAX_IMAGE_SIZE = 15 * 1024 * 1024;

const SettingProfile = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [fileAvatar, setFileAvatar] = useState();

  const [form] = useForm();
  const session = useSession();
  const { t } = useTranslation(['settings', 'errors']);

  const inputFileRef = useRef(null);

  useEffect(() => {
    if (session.status === 'authenticated') {
      setIsLoading(false);
    }
  }, [session]);

  const handleClickUploadImage = () => inputFileRef?.current?.click();
  const handleChangeUploadImage = event => {
    if (event?.target?.files && event.target.files.length > 0) {
      const size = event?.target?.files[0]?.size;
      if (size > MAX_IMAGE_SIZE)
        return notification.warning({
          message: t('settings:profile:form:rules_avatar'),
        });
      setFileAvatar(event.target.files[0]);
    }
  };

  const handleFinishForm = async values => {
    try {
      const isNoFieldUpdate =
        Object.values(values).every(item => !item?.trim()) && !fileAvatar;
      if (isNoFieldUpdate) return;
      setIsLoading(true);
      const dateOfBirth = values?.date_of_birth
        ? values?.date_of_birth.toISOString()
        : undefined;
      const data = {
        ...values,
        date_of_birth: dateOfBirth,
        file: fileAvatar,
      };
      const body = new FormData();
      let updateSession = Object.keys(data).reduce(
        (result, key) => {
          if (!data[key]) return result;
          body.append(key, data[key]);
          if (key === 'file') return result;
          return { ...result, [key]: data[key] };
        },
        { ...session.data.user }
      );
      const response = await updateUser(body);
      if (response?.avatar_profile_url) {
        updateSession = {
          ...updateSession,
          avatar_profile_url: response.avatar_profile_url,
        };
      }
      await session.update(updateSession);
      form.resetFields();
      setFileAvatar(undefined);
      notification.success({ message: t('settings:responses:profile') });
    } catch (error) {
      handleError(t, error);
    } finally {
      setIsLoading(false);
    }
  };

  const avatar = useMemo(
    () =>
      fileAvatar
        ? URL.createObjectURL(fileAvatar)
        : session?.data?.user?.avatar_profile_url
        ? session.data.user?.avatar_profile_url
        : '/svg/avatar-default.svg',
    [fileAvatar, session]
  );

  return (
    <div className='border border-t-0 border-platinum bg-white p-8'>
      {!session.data?.user && (
        <div className='flex w-full justify-center'>
          <Spin size='large' />
        </div>
      )}
      {session.data?.user && (
        <Form form={form} disabled={isLoading} onFinish={handleFinishForm}>
          <div className='mb-6 last:mb-0'>
            <p className='mb-2 text-base font-bold text-arsenic'>
              {t('settings:profile:form:avatar:label')}
            </p>
            <div className='flex items-center'>
              <input
                type='file'
                onChange={handleChangeUploadImage}
                ref={inputFileRef}
                className='hidden'
                disabled={isLoading}
                accept='image/*'
              />
              <div className='flex h-22 items-center'>
                <div
                  onClick={handleClickUploadImage}
                  className='relative mr-4 h-full w-22 cursor-pointer rounded-full border-2 border-arsenic'
                >
                  <Image
                    src={avatar}
                    fill
                    alt='Avatar'
                    className='rounded-full object-cover'
                  />
                  <CameraIconSvg className='absolute bottom-0 right-0 z-10' />
                </div>
                <div className='flex h-full flex-col justify-between'>
                  <div className='text-sm text-nickel'>
                    <p>{t('settings:profile:form:avatar:description1')}</p>
                    <p>{t('settings:profile:form:avatar:description2')}</p>
                  </div>
                  <button
                    type='button'
                    onClick={handleClickUploadImage}
                    className='flex items-center gap-2 self-start rounded border border-nickel px-4 py-1'
                  >
                    <Edit2IconSvg />
                    <span className='font-bold text-nickel'>
                      {t('settings:button:upload')}
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className='mb-6 last:mb-0'>
            <div className='flex items-center gap-2'>
              <div className='w-1/2'>
                <p className='mb-2 text-base font-bold text-arsenic'>
                  {t('settings:profile:form:firstName')}
                </p>
                <Form.Item name='first_name' className='mb-0'>
                  <Input defaultValue={session.data?.user?.first_name} />
                </Form.Item>
              </div>
              <div className='w-1/2'>
                <p className='mb-2 text-base font-bold text-arsenic'>
                  {t('settings:profile:form:lastName')}
                </p>
                <Form.Item name='last_name' className='mb-0'>
                  <Input defaultValue={session.data?.user?.last_name} />
                </Form.Item>
              </div>
            </div>
          </div>
          <div className='mb-6 flex items-center last:mb-0'>
            <p className='mr-16 text-base font-bold text-arsenic'>
              {t('settings:profile:form:gender')}
            </p>
            <Form.Item className='mb-0' name='gender'>
              <Radio.Group defaultValue={session.data?.user?.gender}>
                <Radio value='male'>
                  {t('settings:profile:form:options:male')}
                </Radio>
                <Radio value='female'>
                  {t('settings:profile:form:options:female')}
                </Radio>
              </Radio.Group>
            </Form.Item>
          </div>
          <div className='mb-6 last:mb-0'>
            <p className='mb-2 text-base font-bold text-arsenic'>
              {t('settings:profile:form:email')}
            </p>
            <Input defaultValue={session.data?.user?.email} disabled />
          </div>
          <div className='mb-6 last:mb-0'>
            <div className='flex items-center gap-2'>
              <div className='w-1/2'>
                <p className='mb-2 text-base font-bold text-arsenic'>
                  {t('settings:profile:form:phoneNumber')}
                </p>
                <Input defaultValue={session.data?.user?.phone} disabled />
              </div>
              <div className='w-1/2'>
                <p className='mb-2 text-base font-bold text-arsenic'>
                  {t('settings:profile:form:birthday')}
                </p>
                <Form.Item name='date_of_birth' className='mb-0'>
                  <DatePicker
                    suffixIcon={
                      <Image
                        src='/svg/calendar.svg'
                        width={18}
                        height={18}
                        alt='Calendar'
                      />
                    }
                    allowClear={false}
                    className='w-full text-nickel placeholder:text-nickel'
                    defaultValue={
                      session.data?.user?.date_of_birth
                        ? dayjs(session.data.user.date_of_birth, 'YYYY/MM/DD')
                        : undefined
                    }
                    format='YYYY/MM/DD'
                  />
                </Form.Item>
              </div>
            </div>
          </div>
          <button
            type='submit'
            className='rounded-lg bg-ultramarine-blue px-10 py-3 font-bold text-cultured'
            disabled={isLoading}
          >
            {isLoading ? <Spin /> : t('settings:button:saved')}
          </button>
        </Form>
      )}
    </div>
  );
};

export default SettingProfile;
