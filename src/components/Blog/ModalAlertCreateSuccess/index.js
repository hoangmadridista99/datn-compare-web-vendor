import { Modal, Result } from 'antd';
import Image from 'next/image';
import React from 'react';
import { useTranslation } from 'next-i18next';

const ModalAlertCreateBlogSuccess = ({ isOpen, onClose, onContinueCreate }) => {
  const { t } = useTranslation(['blogs']);

  return (
    <Modal
      open={isOpen}
      onCancel={onClose}
      centered
      closable={false}
      footer={null}
      width='60%'
    >
      <Result
        status='success'
        title={t('blogs:createSuccess:title')}
        subTitle={t('blogs:createSuccess:subTitle')}
        className='result-insurances'
        icon={
          <div className='flex w-full justify-center'>
            <Image src='/svg/check.svg' width={98} height={98} alt='Check' />
          </div>
        }
        extra={
          <div className='flex w-full items-center justify-center gap-4'>
            <button
              className='rounded-lg border border-nickel px-8 py-2 font-bold text-cultured text-nickel'
              type='button'
              onClick={onClose}
              key='form-insurance-button'
            >
              {t('blogs:createSuccess:list')}
            </button>
            <button
              className='rounded-lg border border-ultramarine-blue bg-ultramarine-blue px-8 py-2 font-bold text-cultured'
              type='button'
              onClick={onContinueCreate}
              key='form-insurance-button'
            >
              {t('blogs:createSuccess:continue')}
            </button>
          </div>
        }
      />
    </Modal>
  );
};

export default ModalAlertCreateBlogSuccess;
