import React, { useState, useCallback } from 'react';
import { Modal, Form } from 'antd';
import FormBlog from '@components/FormBlog';
import { createBlog } from '@services/blogs';
import { handleError } from '@helpers/handleError';
import { useTranslation } from 'next-i18next';

const { useForm } = Form;

const ModalCreateBlog = ({
  blogCategories,
  isOpen,
  onClose,
  onCreateSuccess,
}) => {
  const [isLoading, setIsLoading] = useState(false);

  const [form] = useForm();
  const { t } = useTranslation(['blogs', 'errors']);

  const handleClickSubmitForm = () => form.submit();
  const handleFinishForm = useCallback(
    async body => {
      try {
        setIsLoading(true);
        await createBlog(body);
        form.resetFields();
        onCreateSuccess();
      } catch (error) {
        handleError(t, error);
      } finally {
        setIsLoading(false);
      }
    },
    [onCreateSuccess, form, t]
  );

  return (
    <Modal
      centered
      width='70%'
      destroyOnClose
      open={isOpen}
      onCancel={onClose}
      okText={t('blogs:button:createOk')}
      okButtonProps={{ className: 'bg-ultramarine-blue' }}
      onOk={handleClickSubmitForm}
      cancelButtonProps={{ disabled: isLoading }}
      confirmLoading={isLoading}
    >
      <div className='no-scrollbar h-screen-4/5 overflow-hidden overflow-y-scroll'>
        <FormBlog
          form={form}
          blogCategories={blogCategories}
          onFinishForm={handleFinishForm}
        />
      </div>
    </Modal>
  );
};

export default ModalCreateBlog;
