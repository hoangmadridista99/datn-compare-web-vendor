import FormBlog from '@components/FormBlog';
import { Modal, Form, notification } from 'antd';
import React, { useCallback, useEffect, useState } from 'react';
import { getBlogDetails, updateBlog } from '@services/blogs';
import { handleError } from '@helpers/handleError';
import { useTranslation } from 'next-i18next';

const { useForm } = Form;

const ModalUpdateBlog = ({
  blogCategories,
  blogId,
  onClose,
  onUpdateSuccess,
}) => {
  const [blogDetails, setBlogDetails] = useState(null);

  const [form] = useForm();
  const { t } = useTranslation(['blogs', 'errors']);

  const handleGetBlogDetails = useCallback(
    async signal => {
      if (blogId) {
        const response = await getBlogDetails(blogId, signal);
        setBlogDetails(response);
        form.setFieldsValue(response);
      }
    },
    [blogId, form]
  );

  useEffect(() => {
    const controller = new AbortController();

    handleGetBlogDetails(controller.signal).catch(error => {
      handleError(t, error);
      onClose();
    });

    return () => controller.abort();
  }, [handleGetBlogDetails, onClose, t]);

  const handleClickSubmitForm = () => form.submit();

  const handleFinishForm = async data => {
    try {
      if (blogId) {
        await updateBlog(blogId, data);
        notification.success({ message: t('blogs:responses:update') });
        onUpdateSuccess({ id: blogId, ...data });
      }
    } catch (error) {
      handleError(t, error);
    } finally {
      onClose();
    }
  };

  return (
    <Modal
      centered
      width='70%'
      destroyOnClose
      open={!!blogId}
      onCancel={onClose}
      okText={t('blogs:button:updateOk')}
      okButtonProps={{ className: 'bg-ultramarine-blue' }}
      onOk={handleClickSubmitForm}
    >
      <div className='no-scrollbar h-screen-4/5 overflow-hidden overflow-y-scroll'>
        <FormBlog
          form={form}
          blogCategories={blogCategories}
          blogDetails={blogDetails}
          onFinishForm={handleFinishForm}
          isFormUpdate
        />
      </div>
    </Modal>
  );
};

export default ModalUpdateBlog;
