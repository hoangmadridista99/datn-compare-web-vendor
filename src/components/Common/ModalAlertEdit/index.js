import React from 'react';
import { Modal } from 'antd';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';

const ModalAlertEdit = ({ isOpen, onOk, onCancel, type }) => {
  const { t } = useTranslation(['modal']);

  return (
    <Modal width='60%' centered open={isOpen} onCancel={onCancel} footer={null}>
      <div className='flex flex-col items-center p-5'>
        <Image
          src='/svg/warning-1.svg'
          width={98}
          height={98}
          alt='Notify Icon'
        />
        <div className='mt-6'>
          <p className='mb-2 text-center text-base text-nickel'>
            {`${t('modal:edit:title1')} ${t(`modal:type:${type}`)} ${t(
              'modal:edit:title2'
            )}`}
          </p>
          <p className='text-center text-base text-nickel'>
            {`${t('modal:edit:desc')} ${t(`modal:type:${type}`)}?`}
          </p>
        </div>
        <div className='mt-10 flex w-4/5 items-center justify-center'>
          <button
            onClick={onCancel}
            className='mr-2 w-1/2 rounded-lg border border-nickel py-2 text-nickel'
          >
            {t('modal:button:cancel')}
          </button>
          <button
            onClick={onOk}
            className='ml-2 w-1/2 rounded-lg bg-ultramarine-blue py-2 text-cultured'
          >
            {t('modal:button:edit')}
          </button>
        </div>
      </div>
    </Modal>
  );
};

export default ModalAlertEdit;
