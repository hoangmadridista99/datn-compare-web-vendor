import React from 'react';
import Image from 'next/image';
import { Skeleton } from 'antd';

const Rating = ({ isPage }) => {
  return (
    <Skeleton loading={isPage} active className='mb-5 last:mb-0'>
      <div className='mb-5 last:mb-0'>
        <div className='flex items-center'>
          <Image
            src='/svg/avatar-default.svg'
            width={20}
            height={20}
            alt='Avatar'
          />
          <p className='test-arsenic ml-2 text-sm font-bold'>Vendor</p>
        </div>
        <div className='mb-3 flex items-center'>
          <div className='mb-1 flex items-center'>
            <span className='mr-0.5 text-sm font-bold text-nickel'>4</span>
            <Image src='/svg/star.svg' width={20} height={20} alt='Rating' />
          </div>
          <p className='ml-2 text-sm text-spanish-gray'>12/04/2023 - 10:20</p>
        </div>
        <p className='text-sm text-nickel'>
          Sản phẩm bảo hiểm liên kết đầu tư, giúp bảo vệ tài chính vững chắc cho
          hiện tại và mang đến cơ hội tích lũy đầu tư, gia tăng tài sản hiệu
          quả.
        </p>
      </div>
    </Skeleton>
  );
};

export default Rating;
