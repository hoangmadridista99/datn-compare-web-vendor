export const formatCurrency = value => {
  const formatter = new Intl.NumberFormat('vi-VN');

  return formatter.format(value);
};
