import { notification } from 'antd';
import { signOut } from 'next-auth/react';

export const handleError = (t, error) => {
  if (typeof error === 'string')
    return notification.error({ message: t(`errors:${error}`) });

  if (error?.name === 'AxiosError') {
    const { response } = error;

    notification.error({
      message: t(`errors:${response?.data?.error_code || 'SS24001'}`),
    });

    if (response.status === 401 || response.status === 403) return signOut();
  }
};
