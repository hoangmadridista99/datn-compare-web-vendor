module.exports = {
  settings: {
    'import/resolver': {
      typescript: {
        directory: '.',
      },
    },
  },
  env: {
    browser: true,
    es6: true,
    jest: true,
  },
  extends: [
    'prettier',
    'prettier/react',
    'plugin:tailwind/recommended',
    'plugin:prettier/recommended',
    'prettier/@typescript-eslint',
    'plugin:react/recommended',
    'standard',
  ],
  globals: {
    fetch: true,
    Request: true,
    RequestInfo: true,
    RequestInit: true,
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: ['react', '@typescript-eslint', 'prettier'],
  rules: {
    'import/no-unresolved': 'error',
    'react/prop-types': 'off',
    'react/display-name': 'off',
    'comma-dangle': 'off',
    camelcase: 'off',
    'eol-last': ['error', 'always'],
    semi: ['error', 'always'],
    'space-before-function-paren': 'off',
    'multiline-ternary': ['error', 'never'],
    'no-control-regex': 'off',
  },
};