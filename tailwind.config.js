/* eslint-disable no-undef */
const plugin = require('tailwindcss/plugin');

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,ts,jsx,tsx,mdx}', './public/**/*.html'],
  important: true,
  theme: {
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      colors: {
        cultured: '#F8F8F9',
        nickel: '#6C6F7B',
        'ultramarine-blue': '#2B59FF',
        arsenic: '#404249',
        platinum: '#E3E4E7',
        'silver-metallic': '#ABADB5',
        'lavender-web': '#E3F1FE',
        azure: '#008DFF',
        linen: '#FBEAE5',
        vermilion: '#DE3618',
        'spanish-gray': '#8F929D',
        'chinese-silver': '#C7C9CE',
        'royal-orange': '#F49342',
        'antique-white': '#FCEBDB',
        'cosmic-latte': '#FFF8E8',
        'chinese-white': '#E3F1DF',
        'black-0.5': '#00000080',
        'black-0.04': '#0000000a',
        'black-0.25': '#00000040',
        'black-0.3': '#00000033',
        apple: '#50B83C',
        salem: '#108043',
        'pale-silver': '#c8c2c221',
        'egyptian-blue': '#0e30a9',
        'interdimensional-blue': '3700b3',
        transparent: 'transparent',
        'venetian-red': '#ff4d4f',
      },
      fontSize: {
        '3.5xl': '32px',
      },
      spacing: {
        0.25: '1px',
        200: '400px',
        22: '88px',
        15: '60px',
        27: '108px',
        '7/5': '140%',
      },
      lineHeight: {
        12: '48px',
      },
      zIndex: {
        1: 1,
        2: 2,
      },
      content: {
        empty: '',
      },
      maxWidth: {
        130: '130px',
        422: '422px',
      },
      boxShadow: {
        blog: '0px 4px 6px rgba(0,0,0,0.08)',
        form: 'inset 0px 0px 6px rgba(0,0,0,0.08)',
      },
      width: {
        '3/10': '30%',
        '13/20': '65%',
        '7/10': '70%',
      },
      borderRadius: {
        3.5: '3.5px',
      },
      height: {
        'screen-4/5': '80vh',
        'screen-7/10': '70vh',
        9.25: '148px',
        aside: 'calc(100vh - 128px)',
      },
      maxHeight: {
        'screen-7/10': '70vh',
      },
      margin: {
        '1/5': '20%',
      },
    },
  },
  plugins: [
    require('prettier-plugin-tailwindcss'),
    plugin(function ({ matchUtilities }) {
      matchUtilities({
        'bg-gradient': angle => ({
          'background-image': `linear-gradient(${angle}, var(--tw-gradient-stops))`,
        }),
      });
    }),
  ],
};
